package no.uib.jmi053.opencvtestapp.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Joar Midtun on 03.09.2015.
 */
public class FontView extends TextView {
    public FontView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/DODG5.TTF"));
    }
}
