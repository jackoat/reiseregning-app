package no.uib.jmi053.opencvtestapp.ocr;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by JoaT Development on 23/10/2015.
 *
 * Temporary date utility class.
 */
public abstract class DateParserUtil {

    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy_HH:mm:ss");

    private static final String CONNECTION_STRING = "(\\.|/|\\-)";

    private static final List<String> DATE_FORMATS = Arrays.asList(
            "\\d{1,2}" + CONNECTION_STRING + "\\d{1,2}" + CONNECTION_STRING + "\\d{2,4}",
            "\\d{1,2}\\s+(jan|feb|mar|apr|mai|jun|jul|aug|sep|okt|nov|des)\\s+\\d{2,4}",
            "\\d{4}" + CONNECTION_STRING + "\\d{1,2}" + CONNECTION_STRING + "\\d{1,2}");

    /**
     * Finds all dates from a parsed receipt text and returns them as a set.
     * @param parsedText The parsed text to find dates from.
     * @return A set of strings containing all dates found.
     */
    public static Set<String> findAllDates(String parsedText) {
        Set<String > dates = new HashSet<>();

        for (String regex : DATE_FORMATS) {
            Matcher m = Pattern.compile(regex).matcher(parsedText.toLowerCase());
            while(m.find()) {
                dates.add(m.group());
            }
        }
        return dates;
    }
}
