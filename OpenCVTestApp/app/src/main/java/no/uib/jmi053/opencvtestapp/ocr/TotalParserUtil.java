package no.uib.jmi053.opencvtestapp.ocr;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by JoaT Development on 24.10.2015.
 *
 * Utility class for extraction of receipt totals by using regular expressions.
 */
public abstract class TotalParserUtil {

    private static final String TOTAL_DEC = "(\\d+)(,|\\.)\\d{2}";
    private static final String TOTAL_INT = "([a-z]+)(?:\\.|,|/|\\-)?(\\s+)?\\d+";
    private static final String[] TOTAL_PREFIXES_NOR =
            {"total", "totalt", "betale", "nok", "sum", "pris", "bankkort", "bank", "kontant",
                    "kjøp", "minibankkort", "kort", "varekjøp", "kr", "beløp", "varer"};

    /**
     * Finds all totals from a parsed receipt text and returns them as a set.
     * @param parsedText The parsed text to find totals from.
     * @return A set of strings containing all decimals and integers with valid total prefixes.
     */
    public static Set<String> findAllTotals(String parsedText) {
        Set<String> totals = new HashSet<>();

        Matcher m = Pattern.compile(TOTAL_DEC).matcher(parsedText.toLowerCase());
        while (m.find()) {
            totals.add(m.group());
        }

        m = Pattern.compile(TOTAL_INT).matcher(parsedText.toLowerCase());
        while (m.find()) {
            if(hasValidPrefix(m.group())) {
                totals.add(stripNonDigits(m.group()));
            }
        }

        return totals;
    }

    /**
     * Checks if a total prefix matches a determined valid value from the TOTAL_PREFIXES_NOR list.
     * @param maybeTotal A found expression to check for valid total prefix expression.
     * @return True if the prefix has a valid prefix, false otherwise.
     */
    private static boolean hasValidPrefix(String maybeTotal) {
        String prefix = stripNonAlphabetGlyphs(maybeTotal);
        for (String validPrefix : TOTAL_PREFIXES_NOR) {
            if(prefix.equals(validPrefix)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Removes all non digits from a given string.
     * @param input The string to strip non-digits from.
     * @return A string containing only the digits from the given string.
     */
    private static String stripNonDigits(CharSequence input){
        StringBuilder sb = new StringBuilder(input.length());
        for(int i = 0; i < input.length(); i++){
            char c = input.charAt(i);
            if(c > 47 && c < 58){
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * Removes all non alphabet glyphs from a given string.
     * @param input The string to strip non-alphabet glyphs from.
     * @return A string containing only the alphabet glyphs from the given string.
     */
    private static String stripNonAlphabetGlyphs(CharSequence input){
        StringBuilder sb = new StringBuilder(input.length());
        for(int i = 0; i < input.length(); i++){
            char c = input.charAt(i);
            if(c > 96 && c < 123){
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
