package no.uib.jmi053.opencvtestapp.camera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import no.uib.jmi053.opencvtestapp.ocr.DateParserUtil;

/**
 * Created by JoaT Development on 02.10.2015.
 *
 * Contains functionality for finding device camera, and also for handling image resizing and
 * rotation.
 */
public abstract class IOUtils {

    /**
     * Checks if a device has the camera feature.
     * @param context The activity to get device information from.
     * @return True if the device has the camera feature, false otherwise.
     */
    public static boolean deviceHasCamera(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    /**
     * Returns the device camera if found.
     * @return An opened camera if found, null otherwise.
     */
    public static Camera getCamera() {
        try {
            return Camera.open();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Rotates a given bitmap a given amount of degrees.
     * @param b The bitmap to rotate.
     * @param degrees The amount of degrees to rotate.
     * @return The rotated bitmap.
     */
    public static Bitmap rotate(Bitmap b, int degrees) {
        if (degrees != 0 && b != null) {
            Matrix m = new Matrix();

            m.setRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);
            try {
                Bitmap b2 = Bitmap.createBitmap(
                        b, 0, 0, b.getWidth(), b.getHeight(), m, true);
                if (b != b2) {
                    b.recycle();
                    b = b2;
                }
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }
        }
        return b;
    }

    public static String saveToExternalStorage(Bitmap bitmap) {
        File datapath = new File(Environment.getExternalStorageDirectory() + "/tesseract");
        File dir = new File(datapath, "/scannedImages");
        if(!dir.exists()) {
            if(dir.mkdirs()) {
                System.out.println("Dir made.");
            } else {
                System.out.println("Dir not made.");
            }
        }
        SimpleDateFormat sdf = DateParserUtil.DATETIME_FORMAT;
        Calendar cal = Calendar.getInstance();
        String dateString = sdf.format(cal.getTime());
        String name = "receipt_" + dateString + ".jpeg";

        File path = new File(dir, name);
        try {
            FileOutputStream fos = new FileOutputStream(path);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
            System.out.println("Image stored to: " + path.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path.getAbsolutePath() + "/" + name;
    }

    public static int deleteLocalFiles(Context context) {
        int deleted = 0;
        ContextWrapper cw = new ContextWrapper(context.getApplicationContext());
        for (File file : cw.getDir("images", Context.MODE_PRIVATE).listFiles()) {
            if(file.delete()) {
                deleted++;
            }
        }
        return deleted;
    }

    /**
     * Stores a bitmap to internal storage.
     * @param bitmapImage The image to store.
     * @param name The name to be given to the stored image.
     * @return A string representing the path of the stored image.
     */
    public static String saveToInternalStorage(Context context, Bitmap bitmapImage, String name){
        ContextWrapper cw = new ContextWrapper(context.getApplicationContext());
        // Path to /data/data/tefapplication/app_data/images
        File directory = cw.getDir("images", Context.MODE_PRIVATE);
        // Create images directory
        File path = new File(directory, name);

        FileOutputStream fos;
        try {
            fos = new FileOutputStream(path);

            // Use the compress method on the Bitmap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return directory.getAbsolutePath() + "/" + name;
    }

    /**
     * Loads a stored image from internal memory.
     * @param context The context from which to access stream.
     * @param path The path of the image to load.
     * @return The stored bitmap if found at path, null otherwise.
     */
    public static Bitmap loadImageFromStorage(Context context, String path) {
        final String TAG = context.getClass().getName();

        Uri uri = Uri.fromFile(new File(path));
        InputStream in;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = context.getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();

            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d(TAG, "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " +
                    o.outHeight);

            Bitmap b;
            in = context.getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // Scale to max possible inSampleSize that still yields an image
                // Larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // Resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();
                Log.d(TAG, "1st scale operation dimensions - width: " + width + ", height: " +
                        height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d(TAG, "Bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            Log.e(context.getClass().getName(), e.getMessage(), e);
            return null;
        }
    }

    /**
     * Deletes an image from the images-directory with the given path.
     * @param imagePath The path of the image to delete (including name).
     * @return True if the image was deleted, false otherwise;
     */
    public static boolean deleteImageFromStorage(String imagePath) {
        File file = new File(imagePath);
        return file.delete();
    }
}
