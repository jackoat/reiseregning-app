package no.uib.jmi053.opencvtestapp.ocr;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

/**
 * Created by JoaT Development on 13/10/2015.
 *
 * A class containing OCR functionality from Tesseract.
 */
public class TesseractOCR {

    public final String TAG = getClass().getName();

    private TessBaseAPI tess;

    public TesseractOCR(String language) {
        try {
            tess = new TessBaseAPI();
            File datapath = new File(Environment.getExternalStorageDirectory() + "/tesseract");
            File dir = new File(datapath + "/tessdata");
            if(!dir.exists()) {
                if(dir.mkdirs()) {
                    Log.d(TAG, "Dir made.");
                } else {
                    Log.d(TAG, "Dir not made.");
                }
            }
            tess.init(datapath.getAbsolutePath(), language);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Parses a receipt image into text.
     * @param image A binary colorized receipt image to get text from.
     * @return A string containing all the parsed text with trimmed whitespace (max 1 char).
     */
    public String getTextFromImage(Bitmap image) {
        tess.setImage(image);
        return tess.getUTF8Text();
    }

    /**
     * Called on activity destruction.
     */
    public void onDestroy() {
        if(tess != null) {
            tess.end();
        }
    }

    /**
     * Finds and returns all dates from a parsed text of a receipt image.
     * @param parsedText The text to extract dates from.
     * @return An array list of all found dates.
     */
    public List<String> findDates(String parsedText) {
        Set<String> dates = DateParserUtil.findAllDates(parsedText);

        List<String> dateList = new ArrayList<>();
        dateList.addAll(dates);

        return dateList;
    }

    /**
     * Finds all totals from a parsed text of a receipt image (x+(,|.)xx) and returns them in
     * descending order.
     * @param parsedText The text to extract totals from.
     * @return A sorted set in descending order of all found totals.
     */
    public List<String> findTotals(String parsedText) {
        Set<String> totals = TotalParserUtil.findAllTotals(parsedText);

        List<String> totalList = new ArrayList<>();
        totalList.addAll(totals);

        Collections.sort(totalList, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                String s1 = lhs;
                String s2 = rhs;
                if(lhs.contains(",")) {
                    s1 = lhs.replace(",", ".");
                }
                if(rhs.contains(",")) {
                    s2 = rhs.replace(",", ".");
                }
                Double d1 = Double.parseDouble(s1);
                Double d2 = Double.parseDouble(s2);
                return d1.compareTo(d2);
            }
        });
        Collections.reverse(totalList);

        return totalList;
    }

    /**
     * Removes all non-context relevant glyphs.
     * @param parsedText The string to strip non-context relevant glyphs from.
     * @return A string containing only the relevant glyphs from the given string.
     */
    public String removeNoiseFromText(String parsedText) {
        parsedText = parsedText.toLowerCase();
        StringBuilder sb = new StringBuilder(parsedText.length());

        for(int i = 0; i < parsedText.length(); i++){
            char c = parsedText.charAt(i);
            if(isValidChar(c)){
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * Checks if a char value is legal based on relevant context. Valid values are latin glyphs
     * (a-z), norwegian glyphs (æ, ø, å), digits (0-9), whitespaces ( ), commas (,), dashes (-),
     * periods (.), slashes(/) and a new line (\n).
     * @param c The char to be checked for validity.
     * @return True if the value is valid, false otherwise.
     */
    private boolean isValidChar(char c) {
        return c > 96 && c < 123 ||     // is an alphabetical glyph (a-z)
                c > 47 && c < 58 ||     // is a digit (0-9)
                c == 32 ||              // is whitespace ( )
                c > 43 && c < 48 ||     // is either of these in order: (,) (-) (.) (/)
                c == 229 ||             // is an å
                c == 230 ||             // is an æ
                c == 248 ||             // is an ø
                c == '\n';              // is a new line (\n)
    }
}
