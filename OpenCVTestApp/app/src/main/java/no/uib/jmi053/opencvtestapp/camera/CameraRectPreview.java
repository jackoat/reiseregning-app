package no.uib.jmi053.opencvtestapp.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.Display;
import android.view.SurfaceView;
import android.view.WindowManager;

import java.util.List;

/**
 * Created by JoaT Development on 20.10.2015.
 *
 * Creates a custom surface view with a rectangle boundary drawn on it.
 */
public class CameraRectPreview extends SurfaceView {

    private Paint paint;
    private float[] rectangle;
    private boolean showRect;

    public CameraRectPreview(Context context, AttributeSet attrs) {
        super(context, attrs);

        setWillNotDraw(false);

        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);
    }

    public void calcPreviewRectangle() {
        // Finds the width, height and horizontal center of the screen
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        int centerX = width / 2;

        // Position ratios
        double smallGap = 0.10;
        double bigGap = 0.31;
        double marginBottom = height * 0.85;

        // Draws the preview rectangle
        float tlX = (float) (width * smallGap);
        float tlY = (float) (height * bigGap);
        float trX = (float) centerX;
        float trY = (float) (height * smallGap);
        float brX = (float) (width - (width * smallGap));
        float brY = (float) (marginBottom - (height * bigGap));
        float blX = (float) centerX;
        float blY = (float) (marginBottom - (height * smallGap));
        rectangle = new float[]{
                tlX, tlY, trX, trY,
                trX, trY, brX, brY,
                brX, brY, blX, blY,
                blX, blY, tlX, tlY};
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(showRect && rectangle != null) {
            canvas.drawLines(rectangle, paint);
        }
    }

    /**
     * Sets drawing of preview rect to given value.
     * @param showRect True if rect is to be drawn, false otherwise.
     */
    public void setShowRect(boolean showRect) {
        this.showRect = showRect;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(parentWidth, parentHeight);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
