package no.uib.jmi053.opencvtestapp.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import no.uib.jmi053.opencvtestapp.R;
import no.uib.jmi053.opencvtestapp.camera.IOUtils;
import no.uib.jmi053.opencvtestapp.cv.CVUtil;
import no.uib.jmi053.opencvtestapp.ocr.TesseractOCR;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    static final int CAMERA_REQUEST = 1;

    static final String LANGUAGE_TEF = "nor+tef";
    static final String LANGUAGE_NOR = "nor";

    String imagePath;

    // Tesseract fields
    TesseractOCR tess;
    String language;
    String norText;
    String tefText;
    String parsedText;
    String norCleaned;
    String tefCleaned;
    String cleanedText;
    String infoText;

    // Layout fields
    ScrollView resultMenu;
    ImageView imageView;
    TextView textView;

    // OpenCV fields
    Mat edgedImageMat;
    Bitmap edgedImage;
    Bitmap linedImage;
    Bitmap contouredImage;
    Mat warpedImageMat;
    Bitmap warpedImageGray;
    Bitmap warpedImageBinary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        language = LANGUAGE_TEF;
        OpenCVLoader.initDebug();
        int deleted = IOUtils.deleteLocalFiles(this);
        if(deleted == 1) {
            System.out.println(deleted + " image deleted locally.");
        } else {
            System.out.println(deleted + " images deleted locally.");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_restart:
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                return true;
            case R.id.action_change_language:
                if(language.equals(LANGUAGE_NOR)) {
                    language = LANGUAGE_TEF;
                    parsedText = tefText;
                    cleanedText = tefCleaned;
                } else {
                    language = LANGUAGE_NOR;
                    parsedText = norText;
                    cleanedText = norCleaned;
                }
                Toast.makeText(this, "Changed language to: " + language, Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            imagePath = data.getStringExtra("imagePath");
            setContentView(R.layout.activity_image_confirm);
            ImageView imageToConfirm = (ImageView) findViewById(R.id.image_confirm_view);
            imageToConfirm.setImageBitmap(IOUtils.loadImageFromStorage(this, imagePath));
            Button accept = (Button) findViewById(R.id.image_confirm_accept_button);
            Button retake = (Button) findViewById(R.id.image_confirm_retake_button);

            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setResultLayout();
                }
            });
            retake.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("Deleted image: " +
                            IOUtils.deleteImageFromStorage(imagePath));
                    clickCameraButton();
                }
            });
        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_CANCELED) {
            setContentView(R.layout.activity_main);
        }
    }

    @Override
    protected void onDestroy() {
        if(tess != null) {
            tess.onDestroy();
        }

        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera_button:
                clickCameraButton();
                break;
            case R.id.button_edged_image:
                clickEdgedButton();
                break;
            case R.id.button_lined_image:
                clickLinedButton();
                break;
            case R.id.button_contoured_image:
                clickContouredButton();
                break;
            case R.id.button_warped_image:
                clickWarpedButton();
                break;
            case R.id.button_binary_image:
                clickBinaryButton();
                break;
            case R.id.button_parse_text:
                clickParsedButton();
                break;
            case R.id.button_remove_text_noise:
                clickRemoveTextNoise();
                break;
            case R.id.button_find_info:
                clickFindInfoButton();
                break;
        }
    }

    /**
     * The on click listener content for the camera button.
     */
    private void clickCameraButton() {
        Intent cameraIntent = new Intent(this, CameraActivity.class);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    /**
     * The on click listener content for the edged image button.
     */
    private void clickEdgedButton() {
        if(edgedImageMat == null) {
            try {
                System.out.println("Finding edges...");
                edgedImageMat = CVUtil.getEdgedImage(imagePath);
                edgedImage = CVUtil.matToBitmap(
                        edgedImageMat.width(), edgedImageMat.height(), edgedImageMat);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        showImageView(edgedImage);
    }

    /**
     * The on click listener content for the lined image button.
     */
    private void clickLinedButton() {
        if(edgedImageMat != null) {
            try {
                if (linedImage == null) {
                    System.out.println("Finding lines...");
                    Mat linedImageMat = CVUtil.getLinedImage(edgedImageMat);
                    linedImage = CVUtil.matToBitmap(
                            linedImageMat.width(), linedImageMat.height(), linedImageMat);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            showImageView(linedImage);
        } else {
            Toast.makeText(this, "You need to edge image first.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * The on click listener content for the contour image button.
     */
    private void clickContouredButton() {
        if(edgedImageMat != null) {
            try {
                if(contouredImage == null) {
                    System.out.println("Finding contours...");
                    contouredImage = CVUtil.getContouredImage(edgedImageMat);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            showImageView(contouredImage);
        } else {
            Toast.makeText(this, "You need to edge image first.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * The on click listener content for the warp image button.
     */
    private void clickWarpedButton() {
        if(warpedImageMat == null) {
            try {
                System.out.println("Cropping and transforming...");
                warpedImageMat = CVUtil.cropAndTransform(imagePath);
                warpedImageGray = CVUtil.matToBitmap(warpedImageMat);
                warpedImageBinary = CVUtil.matToBinary(warpedImageMat);
                IOUtils.saveToExternalStorage(warpedImageBinary);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        showImageView(warpedImageGray);
    }

    /**
     * The on click listener content for the binary image button.
     */
    private void clickBinaryButton() {
        if(warpedImageBinary != null ) {
            showImageView(warpedImageBinary);
        } else {
            Toast.makeText(this, "You need to warp image first.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * The on click listener content for the parse text from image button.
     */
    private void clickParsedButton() {
        if(warpedImageBinary != null) {
            if(language.equals(LANGUAGE_TEF) && tefText == null) {
                try {
                    System.out.println("Parsing image text...");
                    tess = new TesseractOCR(language);
                    tefText = tess.getTextFromImage(warpedImageBinary);
                    tefCleaned = tess.removeNoiseFromText(tefText);
                    parsedText = tefText;
                    cleanedText = tefCleaned;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(language.equals(LANGUAGE_NOR) && norText == null) {
                try {
                    System.out.println("Parsing image text...");
                    tess = new TesseractOCR(language);
                    norText = tess.getTextFromImage(warpedImageBinary);
                    norCleaned = tess.removeNoiseFromText(norText);
                    parsedText = norText;
                    cleanedText = norCleaned;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            showTextView(parsedText);
        } else {
            Toast.makeText(this, "You need to warp image first.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * The on click listener content for the remove text noise button.
     */
    private void clickRemoveTextNoise() {
        if(cleanedText != null) {
            showTextView(cleanedText);
        } else {
            Toast.makeText(this, "You need to parse text first.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * The on click listener content for the find info button.
     */
    private void clickFindInfoButton() {
        if(parsedText != null) {
            if (infoText == null) {
                try {
                    infoText = "Dates found:\n";
                    for (String date : tess.findDates(cleanedText)) {
                        infoText += (date.toUpperCase() + "\n");
                    }
                    infoText += "\nTotals found:\n";
                    for (String total : tess.findTotals(cleanedText)) {
                        infoText += (total + "\n");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            showTextView(infoText);
        } else {
            Toast.makeText(this, "You need to parse text first.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Updates visibility for displaying images.
     * @param image The image to be shown in the image view.
     */
    private void showImageView(Bitmap image) {
        imageView.setImageBitmap(image);
        imageView.setVisibility(View.VISIBLE);
        resultMenu.setVisibility(View.GONE);
    }

    /**
     * Updates visibility for displaying text strings.
     * @param text The text string to be shown in the text view.
     */
    private void showTextView(String text) {
        textView.setText(text);
        textView.setVisibility(View.VISIBLE);
        resultMenu.setVisibility(View.GONE);
    }

    /**
     * Sets the result layout upon verifying satisfiable image taken.
     */
    private void setResultLayout() {
        setContentView(R.layout.activity_result);

        resultMenu = (ScrollView) findViewById(R.id.result_menu);
        imageView = (ImageView) findViewById(R.id.image_view);
        textView = (TextView) findViewById(R.id.parsed_text);

        // Creates and sets click to finish listeners
        View.OnClickListener clickToFinishListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setClickable(true);
                v.setVisibility(View.GONE);
                resultMenu.setVisibility(View.VISIBLE);
            }
        };

        imageView.setOnClickListener(clickToFinishListener);
        textView.setOnClickListener(clickToFinishListener);
    }
}