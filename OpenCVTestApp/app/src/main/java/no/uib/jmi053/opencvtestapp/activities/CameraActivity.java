package no.uib.jmi053.opencvtestapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import no.uib.jmi053.opencvtestapp.R;
import no.uib.jmi053.opencvtestapp.camera.CameraRectPreview;
import no.uib.jmi053.opencvtestapp.camera.IOUtils;
import no.uib.jmi053.opencvtestapp.camera.PhotoHandler;
import no.uib.jmi053.opencvtestapp.ocr.DateParserUtil;

/**
 * Created by JoaT Development on 04.10.2015.
 *
 * Activity for taking receipt pictures.
 */
public class CameraActivity extends AppCompatActivity implements View.OnClickListener, SurfaceHolder.Callback {

    private final String TAG = getClass().getName();

    // Camera fields
    private Camera camera;
    private CameraRectPreview cameraFrame;
    private boolean rectPreview;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        rectPreview = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!IOUtils.deviceHasCamera(this)) {
            Toast.makeText(this, "No camera found on this device.", Toast.LENGTH_LONG).show();
        } else {
            int cameraId = findActiveCamera();
            if(cameraId < 0) {
                Toast.makeText(this, "No front facing camera found.", Toast.LENGTH_LONG).show();
            } else {
                try {
                    camera = Camera.open(cameraId);

                    if(rectPreview) {
                        cameraFrame = (CameraRectPreview) findViewById(R.id.camera_rect_frame);
                    }
                    SurfaceHolder surfaceHolder = cameraFrame.getHolder();
                    surfaceHolder.addCallback(this);
                    surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

                    cameraFrame.setOnClickListener(this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onPause() {
        if(camera != null) {
            camera.release();
            camera = null;

            cameraFrame.getHolder().removeCallback(this);
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setResult(RESULT_CANCELED, new Intent());
    }

    /**
     * Returns the ID of the active camera.
     * @return An int containing the ID of the active camera.
     */
    private int findActiveCamera() {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                Log.d(TAG, "Camera found");
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            camera.setPreviewDisplay(holder);
            // Sets orientation to portrait
            setCameraDisplayOrientation(findActiveCamera(), camera);
            // Sets auto focus to continuous
            Camera.Parameters cameraParameters = camera.getParameters();
            cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            camera.setParameters(cameraParameters);
            cameraFrame.setShowRect(rectPreview);
            cameraFrame.calcPreviewRectangle();
            // Starts the camera preview
            camera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {}

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera_shutter_button:;
                try {
                    camera.takePicture(null, null, new PhotoHandler(this));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    /**
     * Sets the camera display orientation.
     * @param cameraId The ID of the camera.
     * @param camera The camera instance.
     */
    public void setCameraDisplayOrientation(int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        // set the right preview orientation
        camera.setDisplayOrientation(result);
        // make the camera output a rotated image
        Camera.Parameters cameraParameters = camera.getParameters();
        cameraParameters.setRotation(result);
        camera.setParameters(cameraParameters);
    }
}
