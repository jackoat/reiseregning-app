package no.uib.jmi053.opencvtestapp.cv;

import android.graphics.Bitmap;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by JoaT Development on 04.10.2015.
 *
 * Contains functionality for cropping and transforming images of receipts.
 */
public abstract class CVUtil {

    // The max height of resized image
    private static final int RESIZE_HEIGHT = 500;

    // Blur value constant
    private static final int BLUR_KERNEL_SIZE = 9;

    // Canny value constants
    private static final int CANNY_THRESH_LOW = 50;
    private static final int CANNY_THRESH_HIGH = 110;

    // Hough value constants
    private static final int HOUGH_THRESH = 70;
    private static final int HOUGH_MIN_LENGTH = 20;
    private static final int HOUGH_MAX_GAP = 2;

    /**
     * Crops and transforms the largest contour found in an image.
     * @param path The path of image to crop and transform.
     */
    public static Mat cropAndTransform(String path) {
        Mat original = Imgcodecs.imread(path);
        Size orgSize = original.size();
        double ratio = orgSize.height / RESIZE_HEIGHT;

        Mat edged = getEdgedImage(path);

        List<Point> corners = findLargestContourCorners(edged);
        if (corners.size() == 4) {
            corners = orderPoints(corners);
            scaleCorners(corners, ratio);
        } else {
            corners.clear();
            corners = findHoughLineCorners(edged);
            corners = orderPoints(corners);
            if (corners.size() == 4) {
                scaleCorners(corners, ratio);
            } else {
                System.out.println("No rectangles were found");
                return original;
            }
        }

        return warpTransform(original, corners);
    }

    /**
     * Scales corners by a given ratio.
     * @param corners The found corners to scale.
     * @param ratio   The ratio for which found corners are to be scaled.
     */
    private static void scaleCorners(List<Point> corners, double ratio) {
        for (Point c : corners) {
            c.x *= ratio;
            c.y *= ratio;
            System.out.println(String.format("%.2f", c.x) + "," + String.format("%.2f", c.y));
        }
    }

    /**
     * Gray scales, blurs and detects edges from an image matrix.
     * @param mat The image matrix to detect edges from.
     */
    private static void findEdges(Mat mat) {
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGB2GRAY);
        Imgproc.GaussianBlur(mat, mat, new Size(BLUR_KERNEL_SIZE, BLUR_KERNEL_SIZE), 0);
        Imgproc.Canny(mat, mat, CANNY_THRESH_LOW, CANNY_THRESH_HIGH, 3, true);
    }

    /**
     * Finds the the corners of the largest contour in the edged image matrix.
     * @param mat The edged image to find contour from.
     * @return A list of four points containing the corners of the largest contour.
     */
    private static List<Point> findLargestContourCorners(Mat mat) {
        // Finds contours
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(mat, contours, new Mat(), Imgproc.RETR_EXTERNAL,
                Imgproc.CHAIN_APPROX_SIMPLE);
        System.out.println("Contours found: " + contours.size());

        // Sort contours based on size
        Collections.sort(contours, new Comparator<MatOfPoint>() {
            @Override
            public int compare(MatOfPoint lhs, MatOfPoint rhs) {
                Double d1 = Imgproc.contourArea(lhs);
                Double d2 = Imgproc.contourArea(rhs);
                return d1.compareTo(d2);
            }
        });

        // From ascending to descending order
        Collections.reverse(contours);

        // Calculates perimeters and approximates contours
        List<Point> points = new ArrayList<>();
        MatOfPoint2f con = new MatOfPoint2f(contours.get(0).toArray());
        double peri = Imgproc.arcLength(con, true);
        Imgproc.approxPolyDP(con, con, 0.02 * peri, true);

        double matArea = mat.rows() * mat.cols();
        double reqArea = matArea / 5;
        double contourArea = Imgproc.contourArea(con);
        System.out.println("Mat area: " + matArea);
        System.out.println("Contour area: " + Imgproc.contourArea(con));
        System.out.println("Required area: " + reqArea);
        System.out.println("Contour is large enough: " +
                String.valueOf(contourArea > reqArea));
        System.out.println("Contour points: " + con.toList().size());

        // If the contour has 4 points and has a substantial area it is likely the receipt
        if (con.toList().size() == 4 && contourArea > reqArea) {
            points = con.toList();
        }

        return points;
    }

    /**
     * Finds the lines from an edged image matrix and extracts corners from line intersections.
     * @param edged The edged image matrix to find lines from.
     * @return A list of four points containing the corners of the intersecting lines.
     */
    private static List<Point> findHoughLineCorners(Mat edged) {
        Mat lines = new Mat();
        Imgproc.HoughLinesP(edged, lines, 1, Math.PI / 180,
                HOUGH_THRESH, HOUGH_MIN_LENGTH, HOUGH_MAX_GAP);

        System.out.println("Lines found: " + lines.rows());

        List<Point> corners = new ArrayList<>();
        for (int i = 0; i < lines.rows(); i++) {
            for (int j = 0; j < lines.rows(); j++) {
                Point point = computeIntersect(lines.get(i, 0), lines.get(j, 0), edged);
                if (point.x >= 0 && point.y >= 0) {
                    corners.add(point);
                }
            }
        }
        System.out.println("Corners found: " + corners.size());

        return corners;
    }

    /**
     * Computes the intersection of two lines.
     * @param lineA First line coordinates to compute intersection for.
     * @param lineB Second line coordinates to compute intersection for.
     * @return A point representation of the intersection of the lines.
     */
    private static Point computeIntersect(double[] lineA, double[] lineB, Mat edged) {
        double x1 = lineA[0], y1 = lineA[1], x2 = lineA[2], y2 = lineA[3];
        double x3 = lineB[0], y3 = lineB[1], x4 = lineB[2], y4 = lineB[3];

        float cross = (float) (((x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4)));


        Point invalid = new Point(-1, -1);
        if (Math.abs(cross) < 1e-8) {
            return invalid;
        } else {
            double x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / cross;
            double y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / cross;
            if((x >= 0 && x <= edged.cols()) &&  (y >= 0 && y <= edged.rows())) {
                Point intersection = new Point(x, y);
                double angle = getAngleBetweenLines(new Point(x2, y2), new Point(x4, y4), intersection);
                if (angle > 80 && angle < 100) {
                    System.out.println("Added intersection with angle: " +
                            String.format("%.2f", angle) + " at " + String.format("%.2f", x) +
                            "," + String.format("%.2f", y));
                    return intersection;
                } else {
                    System.out.println("Rejected intersection with angle: " +
                            String.format("%.2f", angle) + " at " + String.format("%.2f", x) +
                            "," + String.format("%.2f", y));
                    return invalid;
                }
            } else {
                System.out.println("Rejected intersection found out of bounds at: " +
                        String.format("%.2f", x) + "," + String.format("%.2f", y));
                return invalid;
            }
        }
    }

    /**
     * Calculates the angle between two lines and their intersection point.
     * @param a One line point from line A.
     * @param b One line point from line B.
     * @param intersection The intersection point of the lines A and B.
     * @return The calculated angle between the intersection and the two lines.
     */
    private static double getAngleBetweenLines(Point a, Point b, Point intersection) {
        double angle1 = Math.atan2(a.y - intersection.y, a.x - intersection.x);
        double angle2 = Math.atan2(b.y - intersection.y, b.x - intersection.x);

        double angle = Math.toDegrees(angle1 - angle2);

        if(angle < 0) {
            angle += 360;
        }
        return angle;
    }

    /**
     * Orders corners such that they are found top left, top right, bottom right, bottom left.
     * @param corners The corners to order.
     * @return An ordered list of corners.
     */
    private static List<Point> orderPoints(List<Point> corners) {
        Comparator<Point> xSorter = new Comparator<Point>() {
            @Override
            public int compare(Point lhs, Point rhs) {
                Double x1 = lhs.x;
                Double x2 = rhs.x;
                return x1.compareTo(x2);
            }
        };

        Collections.sort(corners, xSorter);

        Point tl = corners.get(0);
        Point br = corners.get(corners.size() - 1);

        Comparator<Point> ySorter = new Comparator<Point>() {
            @Override
            public int compare(Point lhs, Point rhs) {
                Double y1 = lhs.y;
                Double y2 = rhs.y;
                return y1.compareTo(y2);
            }
        };

        Collections.sort(corners, ySorter);

        Point tr = corners.get(0);
        Point bl = corners.get(corners.size() - 1);

        return Arrays.asList(tl, tr, br, bl);
    }

    /**
     * Warps the image to hold only the contoured rectangle found.
     * @param warped  The warped image.
     * @param corners The found corners.
     */
    private static Mat warpTransform(Mat warped, List<Point> corners) {
        fourPointTransform(warped, corners);

        Imgproc.cvtColor(warped, warped, Imgproc.COLOR_RGB2GRAY);

        return warped;
    }

    /**
     * Uses a list of four points to transform an image.
     * @param original The original image to apply transform to.
     * @param corners  The four points holding the edges of the corners found in the image.
     */
    private static void fourPointTransform(Mat original, List<Point> corners) {
        Point tl = corners.get(0);
        Point tr = corners.get(1);
        Point br = corners.get(2);
        Point bl = corners.get(3);

        MatOfPoint2f source = new MatOfPoint2f(tl, tr, br, bl);

        // Computes max width
        double widthA = Math.sqrt((Math.pow((br.x - bl.x), 2)) + (Math.pow((br.y - bl.y), 2)));
        double widthB = Math.sqrt((Math.pow((tr.x - tl.x), 2)) + (Math.pow((tr.y - tl.y), 2)));
        double maxWidth;
        if (widthA > widthB) {
            maxWidth = widthA;
        } else {
            maxWidth = widthB;
        }

        // Computes max height
        double heightA = Math.sqrt((Math.pow((tr.x - br.x), 2)) + (Math.pow((tr.y - br.y), 2)));
        double heightB = Math.sqrt((Math.pow((tl.x - bl.x), 2)) + (Math.pow((tl.y - bl.y), 2)));
        double maxHeight;
        if (heightA > heightB) {
            maxHeight = heightA;
        } else {
            maxHeight = heightB;
        }

        // Constructs the destination set
        List<Point> roi = Arrays.asList(
                new Point(0, 0),
                new Point(maxWidth - 1, 0),
                new Point(maxWidth - 1, maxHeight - 1),
                new Point(0, maxHeight - 1));
        MatOfPoint2f dest = new MatOfPoint2f(roi.get(0), roi.get(1), roi.get(2), roi.get(3));

        Mat per = Imgproc.getPerspectiveTransform(source, dest);
        Imgproc.warpPerspective(original, original, per, new Size(maxWidth, maxHeight));
    }

    /**
     * Returns a matrix with all edges detected.
     * @param path The path to find source map from.
     * @return A matrix containing all found edges from path image.
     */
    public static Mat getEdgedImage(String path) {
        Mat original = Imgcodecs.imread(path);
        Size orgSize = original.size();
        double ratio = orgSize.height / RESIZE_HEIGHT;
        int ratioWidth = (int) (orgSize.width / ratio);
        Size modSize = new Size(ratioWidth, RESIZE_HEIGHT);

        Mat resized = new Mat();
        Imgproc.resize(original, resized, modSize);

        findEdges(resized);

        return resized;
    }

    /**
     * Returns a bitmap picturing found lines from an edged matrix.
     * @param edged The edged matrix to find lines from.
     * @return A bitmap with found lines drawn.
     */
    public static Mat getLinedImage(Mat edged) {
        Mat lines = new Mat();
        Imgproc.HoughLinesP(edged, lines, 1, Math.PI / 180,
                HOUGH_THRESH, HOUGH_MIN_LENGTH, HOUGH_MAX_GAP);

        Mat lined = edged.clone();
        for (int x = 0; x < lines.rows(); x++) {
            double[] vec = lines.get(x, 0);
            double x1 = 0,
                    y1 = ((float) vec[1] - vec[3]) / (vec[0] - vec[2]) * -vec[0] + vec[1],
                    x2 = edged.cols(),
                    y2 = ((float) vec[1] - vec[3]) / (vec[0] - vec[2]) * (edged.cols() - vec[2]) + vec[3];
            Point start = new Point(x1, y1);
            Point end = new Point(x2, y2);

            Imgproc.line(lined, start, end, new Scalar(200, 200, 0, 255), 1);
        }

        return lined;
    }

    /**
     * Returns a bitmap picturing the largest found rectangle contour.
     * @param edged An edge detected matrix.
     * @return A bitmap with the largest 4-point contour drawn, or input if none were found.
     */
    public static Bitmap getContouredImage(Mat edged) {
        List<Point> contourPoints = findLargestContourCorners(edged);
        Mat contoured = edged.clone();
        if (contourPoints.size() == 4) {
            MatOfPoint mat = new MatOfPoint(contourPoints.get(0), contourPoints.get(1),
                    contourPoints.get(2), contourPoints.get(3));

            Imgproc.drawContours(contoured, Collections.singletonList(mat),
                    -1, new Scalar(200, 200, 0, 255), 2);
        } else {
            System.out.println("No rectangles were found.");
            return matToBitmap(edged.width(), RESIZE_HEIGHT, edged);
        }

        return matToBitmap(contoured.width(), RESIZE_HEIGHT, contoured);
    }

    /**
     * Returns a bitmap with a given width and color values from a matrix.
     * @param ratioWidth The width of the resized image.
     * @param mat        The matrix holding color values of the image.
     * @return The matrix as a bitmap.
     */
    public static Bitmap matToBitmap(int ratioWidth, int height, Mat mat) {
        Bitmap bitmap = Bitmap.createBitmap(ratioWidth, height, Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mat, bitmap);

        return bitmap;
    }

    /**
     * Returns a bitmap with height and width based on its matrix.
     *
     * @param mat The matrix to convert to bitmap.
     * @return The converted bitmap.
     */
    public static Bitmap matToBitmap(Mat mat) {
        Bitmap bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mat, bitmap);

        return bitmap;
    }

    /**
     * Changes colour values of a warped image matrix to binary and converts to bitmap.
     * @param warped The warped image matrix to convert to binary bitmap.
     * @return The converted binary bitmap.
     */
    public static Bitmap matToBinary(Mat warped) {
        Imgproc.medianBlur(warped, warped, 3);
        Imgproc.threshold(warped, warped, 0, 255, Imgproc.THRESH_BINARY_INV | Imgproc.THRESH_OTSU);
        Mat element = Imgproc.getStructuringElement(Imgproc.CV_SHAPE_ELLIPSE, new Size(3, 3));
        Imgproc.morphologyEx(warped, warped, Imgproc.MORPH_CLOSE, element);

        return matToBitmap(warped);
    }
}