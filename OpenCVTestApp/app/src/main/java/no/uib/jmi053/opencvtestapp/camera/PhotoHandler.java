package no.uib.jmi053.opencvtestapp.camera;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import no.uib.jmi053.opencvtestapp.ocr.DateParserUtil;

/**
 * Created by JoaT Development on 04.10.2015.
 *
 * Handles photos after being taken from camera activity.
 */
public class PhotoHandler implements Camera.PictureCallback {

    // Log constant
    private final String TAG = getClass().getName();

    private Activity activity;

    public PhotoHandler(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Log.i(TAG, "Picture taken.");

        if(data != null) {
            try {
                Log.i(TAG, "Found picture data.");
                Bitmap image = BitmapFactory.decodeByteArray(data, 0, data.length);
                Log.i(TAG, "Decoded byte array.");

                SimpleDateFormat sdf = DateParserUtil.DATETIME_FORMAT;

                Calendar cal = Calendar.getInstance();
                String dateString = sdf.format(cal.getTime());
                String name = "receipt_" + dateString + ".jpg";

                Log.i(TAG, "Storing image to internal storage.");
                // Save the image and store and return the path to ReceiptActivity
                String path = IOUtils.saveToInternalStorage(activity, image, name);
                Log.i(TAG, "Image stored to: " + path);

                Intent cameraResult = new Intent();
                cameraResult.putExtra("imagePath", path);
                activity.setResult(Activity.RESULT_OK, cameraResult);
                activity.finish();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
