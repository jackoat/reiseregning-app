package no.uib.jmi053.tefapplication.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.superclass.TEFActivity;
import no.uib.jmi053.tefapplication.listeners.OnSwipeTouchListener;
import no.uib.jmi053.tefapplication.utilities.CameraUtils;
import no.uib.jmi053.tefapplication.utilities.constants.IntentConstants;

/**
 * Created by JoaT Development 04.09.15.
 *
 * Activity for presenting a taken and formatted receipt image to the user.
 */
public class ReceiptImageActivity extends TEFActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_image);

        String imagePath = getIntent().getStringExtra(IntentConstants.IMAGE_PATH);
        Bitmap imageBitmap = CameraUtils.loadImageFromStorage(this, imagePath);

        ImageView image = (ImageView) findViewById(R.id.receipt_image);
        image.setImageBitmap(imageBitmap);

        image.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeRight() {
                finish();
            }
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });
    }
}
