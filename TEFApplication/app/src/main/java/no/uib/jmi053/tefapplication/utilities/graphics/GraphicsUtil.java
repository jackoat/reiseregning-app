package no.uib.jmi053.tefapplication.utilities.graphics;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TimePicker;

/**
 * Created by JoaT Development 14.09.15.
 *
 * Contains functionality for editing colors and graphics for different views.
 */
public abstract class GraphicsUtil {

    /**
     * Sets the color of the divider in a NumberPicker
     *
     * @param picker the NumberPicker to change
     * @param color the colour to apply
     */
    public static void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields){
            if(pf.getName().equals("mSelectionDivider")){
                pf.setAccessible(true);
                try{
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                }
                catch (IllegalArgumentException | Resources.NotFoundException | IllegalAccessException e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    /**
     * Formats a DatePicker with a given color
     *
     * @param datePicker the DatePicker to format
     * @param color the colour to apply
     */
    public static void format(DatePicker datePicker, int color){
        LinearLayout llFirst = (LinearLayout) datePicker.getChildAt(0);
        LinearLayout llSecond = (LinearLayout) llFirst.getChildAt(0);
        for (int i = 0; i < llSecond.getChildCount(); i ++){
            if(llSecond.getChildAt(i) instanceof NumberPicker) {
                NumberPicker numberPicker = (NumberPicker) llSecond.getChildAt(i);
                GraphicsUtil.setDividerColor(numberPicker, color);
            }
        }
    }

    /**
     * Formats a TimePicker with a given color
     *
     * @param tp the TimePicker to format
     * @param color the color to apply
     */
    public static void format(TimePicker tp, int color) {
        LinearLayout llFirst = (LinearLayout) tp.getChildAt(0);
        for (int i = 0; i < llFirst.getChildCount(); i++) {
            if (llFirst.getChildAt(i) instanceof NumberPicker) {
                NumberPicker numberPicker = (NumberPicker) llFirst.getChildAt(i);
                GraphicsUtil.setDividerColor(numberPicker, color);
            } else if (llFirst.getChildAt(i) instanceof LinearLayout) {
                LinearLayout llSecond = (LinearLayout) llFirst.getChildAt(i);
                for (int j = 0; j < llSecond.getChildCount(); j++) {
                    if (llSecond.getChildAt(j) instanceof NumberPicker) {
                        NumberPicker numberPicker = (NumberPicker) llSecond.getChildAt(j);
                        GraphicsUtil.setDividerColor(numberPicker, color);
                    }
                    if(llSecond.getChildAt(j) instanceof AppCompatTextView){
                        AppCompatTextView textThingy = (AppCompatTextView) llSecond.getChildAt(j);
                        textThingy.setTextColor(color);
                    }
                }
            }
        }
    }

    /**
     * Sets a new drawable on the right side of a button.
     *
     * @param context The context of the application.
     * @param button The button to set the new drawable.
     * @param drawableID The resource ID of the drawable to be set.
     */
    public static void newDrawableRight(Context context, Button button, int drawableID) {
        button.setCompoundDrawablesWithIntrinsicBounds(
                null, null, ContextCompat.getDrawable(context, drawableID), null);
    }
}

