package no.uib.jmi053.tefapplication.activities.inputs;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.fragments.PersonActivity;
import no.uib.jmi053.tefapplication.activities.superclass.InputActivity;
import no.uib.jmi053.tefapplication.adapters.ExpandableListAdapter;
import no.uib.jmi053.tefapplication.entities.CarTravel;
import no.uib.jmi053.tefapplication.utilities.constants.DebugConstants;
import no.uib.jmi053.tefapplication.utilities.constants.IntentConstants;
import no.uib.jmi053.tefapplication.utilities.constants.PopupConstants;
import no.uib.jmi053.tefapplication.utilities.graphics.ExpandableListUtil;
import no.uib.jmi053.tefapplication.utilities.graphics.GraphicsUtil;
import no.uib.jmi053.tefapplication.utilities.parsers.DateParserUtil;
import no.uib.jmi053.tefapplication.widgets.FontButton;
import no.uib.jmi053.tefapplication.widgets.FontEdit;

/**
 * Created by JoaT Development 16.09.15.
 *
 * Activity for adding or editing information for a car trip.
 */
public class CarTravelActivity extends InputActivity implements View.OnClickListener {

    // Input widgets
    DatePicker date;
    FontEdit start;
    FontEdit end;
    FontEdit km;
    ExpandableListView passengerView;

    // Passenger list
    List<String> passengers;

    // Flag
    boolean beingEdited;
    Boolean showToast = true;

    // The ID of the car travel being edited
    int id;

    // The create and save button
    FontButton button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_car_travel);

        // Finds and sets icon to the create button
        button = (FontButton) findViewById(R.id.new_car_travel_button);
        GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_create);

        // Finds all input fields and expandable lists
        date = (DatePicker) findViewById(R.id.car_date_input);

        start = (FontEdit) findViewById(R.id.car_start_input);
        end = (FontEdit) findViewById(R.id.car_end_input);
        km = (FontEdit) findViewById(R.id.car_kilometers_input);
        passengerView = (ExpandableListView) findViewById(R.id.car_passengers_view);

        // Finds the ID of the car travel
        id = getIntent().getIntExtra(IntentConstants.ITEM_ID, -1);

        // Checks if the car travel is being edited
        beingEdited = getIntent().getBooleanExtra(IntentConstants.EDIT_FLAG, false);

        // If being edited then the passenger list is set from found car travel
        if(beingEdited) {
            passengers = db.getCarTravel(id).getPassengers();
        } else {
            passengers = new ArrayList<>();
        }

        // Sets listeners to children of the passenger view
        passengerView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                        int childPosition, long id) {
                ExpandableListAdapter adapter =
                        (ExpandableListAdapter) passengerView.getExpandableListAdapter();

                String passenger = adapter.getChild(groupPosition, childPosition).toString();

                Intent resultIntent = new Intent(CarTravelActivity.this,
                        PersonActivity.class);

                resultIntent.putExtra(IntentConstants.PARENT_NAME,
                        IntentConstants.PARENT_CAR_TRAVEL);
                resultIntent.putExtra(IntentConstants.ITEM_NAME, passenger);
                resultIntent.putExtra(IntentConstants.EDIT_FLAG, true);
                startActivityForResult(resultIntent, IntentConstants.REQUEST_NEW_ITEM);
                return false;
            }
        });

        // Sets updated adapter for passenger view and formats its height
        passengerView.setAdapter(createPassengerAdapter());
        ExpandableListUtil.setListHeight(Collections.singletonList(passengerView));

        updateInformation();

        inputStringsAtStart = getInputStrings();
    }

    @Override
    protected String getItemName() {
        return "car travel";
    }

    @Override
    protected List<FontEdit> createEditFieldList() {
        return Arrays.asList(start, end, km);
    }

    @Override
    protected List<String> getInputStrings() {
        List<String> tempStrings = Arrays.asList(
                DateParserUtil.getDateString(date),
                start.getText().toString(),
                end.getText().toString(),
                km.getText().toString());

        // Create new list to remove fixed size
        List<String> inputStrings = new ArrayList<>();
        inputStrings.addAll(tempStrings);

        for (String pas : passengers) {
            inputStrings.add(pas);
        }
        return inputStrings;
    }

    /**
     * Updates visibility of representatives view based on whether or not it contains data.
     */
    private void updatePasViewVisibility() {
        if(passengers.size() > 0) {
            passengerView.setVisibility(View.VISIBLE);
            passengerView.collapseGroup(0);
            passengerView.setAdapter(createPassengerAdapter());
            ExpandableListUtil.setListHeight(Collections.singletonList(passengerView));
        } else {
            passengerView.setVisibility(View.GONE);
        }
    }

    /**
     * Ensures that information is up to date on editing.
     */
    private void updateInformation() {
        if (beingEdited) {
            if (id != -1) {
                // Finds the car travel with the given ID
                CarTravel carTravel = db.getCarTravel(id);

                // Sets new titles for editing
                TextView header = (TextView) findViewById(R.id.header);
                header.setText(R.string.edit_car_travel_header);
                setTitle(R.string.title_activity_edit_car_travel);

                // Sets new information based on found car travel
                DateParserUtil.setDate(date, carTravel.getDate());
                start.setText(carTravel.getStart());
                end.setText(carTravel.getEnd());
                km.setText(String.valueOf(carTravel.getKilometers()));

                // Changes create button text and icon to save
                button.setText(R.string.button_save);
                GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_save);

                // Makes delete button visible
                FontButton delete = (FontButton) findViewById(R.id.delete_car_travel_button);
                delete.setVisibility(FontButton.VISIBLE);

                // Updates passengers
                passengerView.setAdapter(createPassengerAdapter());
                ExpandableListUtil.setListHeight(Collections.singletonList(passengerView));

                updatePasViewVisibility();
            } else {
                Log.e(TAG, DebugConstants.ERROR_NO_CAR_TRAVEL);
            }
        }
    }

    /**
     * Creates and returns an adapter for the passenger view.
     * @return An expandable list adapter formatted for the passenger view.
     */
    private ExpandableListAdapter createPassengerAdapter() {
        HashMap<String, List<String>> childList = new HashMap<>();

        List<String> headerList = new ArrayList<>();
        headerList.add(getResources().getString(R.string.car_passengers_label));

        childList.put(headerList.get(0), passengers);

        return new ExpandableListAdapter(this, headerList, childList);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == IntentConstants.RESULT_DELETE) {
            passengers.remove(data.getStringExtra(IntentConstants.ITEM_ID));
        }
        if (resultCode == IntentConstants.RESULT_CREATE) {
            passengers.add(data.getStringExtra(IntentConstants.ITEM_NAME));
        }
        if (resultCode == IntentConstants.RESULT_EDIT) {
            passengers.remove(data.getStringExtra(IntentConstants.ITEM_ID));
            passengers.add(data.getStringExtra(IntentConstants.ITEM_NAME));
        }
        if(requestCode == IntentConstants.REQUEST_NEW_ITEM) {
            updatePasViewVisibility();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.new_car_travel_button:
                if(!hasUnfilledFields()) {
                    showToast = false;
                    int fk = getIntent().getIntExtra(IntentConstants.TRAVEL_ID, -1);
                    if (fk != -1) {
                        int carTravelId = id;
                        if(id == -1) {
                            carTravelId = db.getNextCarTravelId(fk);
                        }
                        CarTravel newCarTravel = new CarTravel(carTravelId, fk,
                                DateParserUtil.getDateString(date),
                                start.getText().toString(), end.getText().toString(),
                                Double.parseDouble(km.getText().toString()));
                        newCarTravel.setPassengers(passengers);
                        if (!beingEdited) {
                            db.insertCarTravel(newCarTravel);
                        } else {
                            if (id != -1) {
                                newCarTravel.setId(id);
                                db.updateCarTravel(newCarTravel);
                            } else {
                                Log.e(TAG, DebugConstants.ERROR_NO_CAR_TRAVEL);
                            }
                        }
                    } else {
                        Log.e(TAG, DebugConstants.ERROR_NO_TRAVEL);
                    }
                    finish();
                } else {
                    if (showToast){
                        showUnfilledFieldsToast();
                        showToast = false;
                    }
                }
                break;
            case R.id.delete_car_travel_button:
                if(id != -1) {
                    final Dialog dialog = new Dialog(this);
                    // Creates the positive button listener
                    View.OnClickListener positiveListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.i(PopupConstants.DIALOG_DELETION,
                                    "User confirmed " + getItemName() + " deletion.");
                            v.setClickable(true);
                            db.deleteCarTravel(id);
                            finish();
                            dialog.dismiss();
                        }
                    };
                    showDeleteConfirmDialog(dialog, positiveListener);
                } else {
                    Log.e(TAG, DebugConstants.ERROR_NO_CAR_TRAVEL);
                }
                break;
            case R.id.car_passengers_button:
                Intent resultIntent = new Intent(CarTravelActivity.this,
                        PersonActivity.class);
                resultIntent.putExtra(IntentConstants.PARENT_NAME,
                        IntentConstants.PARENT_CAR_TRAVEL);
                startActivityForResult(resultIntent, IntentConstants.REQUEST_NEW_ITEM);
                break;
        }
    }
}
