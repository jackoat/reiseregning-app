package no.uib.jmi053.tefapplication.utilities.constants;

import android.content.Context;

/**
 * Created by JoaT Development on 04.10.2015.
 *
 * Contains constants for intents.
 */
public abstract class IntentConstants {

    // Request and result codes
    public static final int REQUEST_NEW_ITEM = 0;
    public static final int REQUEST_NEW_EVENT = 1;
    public static final int REQUEST_NEW_COMMENT = 2;
    public static final int RESULT_CREATE = 1;
    public static final int RESULT_EDIT = 2;
    public static final int RESULT_DELETE = 3;

    // Intent name constants
    public static final String TRAVEL_ID = "travel_id";
    public static final String ITEM_ID = "item_id";
    public static final String ITEM_NAME = "item_name";
    public static final String ITEM_NUMBER = "item_number";
    public static final String PARENT_NAME = "parent_name";
    public static final String PARENT_RECEIPT = "parent_receipt";
    public static final String PARENT_CAR_TRAVEL = "parent_car_travel";
    public static final String IMAGE = "image";
    public static final String IMAGE_PATH = "image_path";

    // Intent flag constants
    public static final String EDIT_FLAG = "edit_flag";
    public static final String REIMBURSEMENT_FLAG = "reimbursement_flag";

}
