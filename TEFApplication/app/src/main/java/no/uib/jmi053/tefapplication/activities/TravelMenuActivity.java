package no.uib.jmi053.tefapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.fragments.CommentActivity;
import no.uib.jmi053.tefapplication.activities.fragments.EventActivity;
import no.uib.jmi053.tefapplication.activities.inputs.CarTravelActivity;
import no.uib.jmi053.tefapplication.activities.inputs.ReceiptActivity;
import no.uib.jmi053.tefapplication.activities.inputs.SendActivity;
import no.uib.jmi053.tefapplication.activities.inputs.TravelActivity;
import no.uib.jmi053.tefapplication.activities.superclass.TEFActivity;
import no.uib.jmi053.tefapplication.adapters.ExpandableListAdapter;
import no.uib.jmi053.tefapplication.entities.CarTravel;
import no.uib.jmi053.tefapplication.entities.Receipt;
import no.uib.jmi053.tefapplication.entities.Travel;
import no.uib.jmi053.tefapplication.utilities.constants.IntentConstants;
import no.uib.jmi053.tefapplication.utilities.graphics.ExpandableListUtil;
import no.uib.jmi053.tefapplication.utilities.parsers.DateParserUtil;

/**
 * Created by JoaT Development 04.09.15.
 *
 * Hub activity for registering information about a travel and its expenses.
 */
public class TravelMenuActivity extends TEFActivity implements View.OnClickListener {

    // Text line shift constant
    private final String newline = System.getProperty("line.separator");

    // Receipt category constants
    private static final int HOTEL = 0;
    private static final int TICKET = 1;
    private static final int CAR_RENTAL = 2;
    private static final int MEAL = 3;
    private static final int OTHER = 4;

    // List label names
    private List<String> RECEIPT_HOTEL_HEADERS;
    private List<String> RECEIPT_TICKET_HEADERS;
    private List<String> RECEIPT_RENTAL_CAR_HEADERS;
    private List<String> RECEIPT_MEAL_HEADERS;
    private List<String> RECEIPT_OTHER_HEADERS;
    private List<String> CAR_TRAVEL_HEADERS;
    private List<String> EVENT_HEADERS;
    private List<String> COMMENT_HEADERS;

    // Receipt category lists
    private List<String> hotelTypes;
    private List<String> ticketTypes;
    private List<String> carRentalTypes;
    private List<String> mealTypes;
    private List<String> otherTypes;

    // Expandable lists
    ExpandableListView receiptHotelView;
    ExpandableListView receiptTicketView;
    ExpandableListView receiptCarRentalView;
    ExpandableListView receiptMealView;
    ExpandableListView receiptOtherView;
    ExpandableListView carTravelView;
    ExpandableListView eventView;
    ExpandableListView commentView;

    // Lists of travel contents
    List<Receipt> receipts;
    List<Receipt> receiptsHotel;
    List<Receipt> receiptsTicket;
    List<Receipt> receiptsCarRental;
    List<Receipt> receiptsMeal;
    List<Receipt> receiptsOther;
    List<CarTravel> carTravels;
    List<String> events;
    List<String> comments;

    // Expandable list view list
    List<ExpandableListView> expLists;

    // Active travel
    Travel travel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel_menu);

        // Sets error log constants from resources
        RECEIPT_HOTEL_HEADERS = Collections.singletonList(getString(
                R.string.travel_menu_hotel_list_label));
        RECEIPT_TICKET_HEADERS = Collections.singletonList(getString(
                R.string.travel_menu_ticket_list_label));
        RECEIPT_RENTAL_CAR_HEADERS = Collections.singletonList(getString(
                R.string.travel_menu_rental_car_list_label));
        RECEIPT_MEAL_HEADERS = Collections.singletonList(getString(
                R.string.travel_menu_meal_list_label));
        RECEIPT_OTHER_HEADERS = Collections.singletonList(getString(
                R.string.travel_menu_other_list_label));
        CAR_TRAVEL_HEADERS = Collections.singletonList(getString(
                R.string.travel_menu_car_travel_list_label));
        EVENT_HEADERS = Collections.singletonList(getString(
                R.string.travel_menu_event_list_label));
        COMMENT_HEADERS = Collections.singletonList(getResources().getString(
                R.string.travel_menu_comment_list_label));

        // Finds expandable lists
        receiptHotelView = (ExpandableListView) findViewById(R.id.receipt_hotel_view);
        receiptTicketView = (ExpandableListView) findViewById(R.id.receipt_ticket_view);
        receiptCarRentalView = (ExpandableListView) findViewById(R.id.receipt_car_rental_view);
        receiptMealView = (ExpandableListView) findViewById(R.id.receipt_meal_view);
        receiptOtherView = (ExpandableListView) findViewById(R.id.receipt_other_view);
        carTravelView = (ExpandableListView) findViewById(R.id.car_travel_view);
        eventView = (ExpandableListView) findViewById(R.id.event_view);
        commentView = (ExpandableListView) findViewById(R.id.comment_view);

        // Defines the expense categories for each receipt type
        String[] receiptTypes = getResources().getStringArray(R.array.receipt_type_categories);
        hotelTypes = Collections.singletonList(receiptTypes[0]);
        ticketTypes = Arrays.asList(receiptTypes[1], receiptTypes[2], receiptTypes[3]);
        carRentalTypes = Arrays.asList(receiptTypes[4], receiptTypes[5], receiptTypes[6]);
        mealTypes = Arrays.asList(receiptTypes[7], receiptTypes[8]);
        otherTypes = Arrays.asList(receiptTypes[9], receiptTypes[10]);

        // Sets all list views in a list
        expLists = Arrays.asList(receiptHotelView, receiptTicketView, receiptCarRentalView,
                receiptMealView, receiptOtherView, carTravelView, eventView, commentView);

        // Sets listeners to the children of the receipt views
        setReceiptChildListener(receiptHotelView, HOTEL);
        setReceiptChildListener(receiptTicketView, TICKET);
        setReceiptChildListener(receiptCarRentalView, CAR_RENTAL);
        setReceiptChildListener(receiptMealView, MEAL);
        setReceiptChildListener(receiptOtherView, OTHER);

        // Sets listeners to children of the car travel view
        carTravelView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                        int childPosition, long id) {
                Intent itemIntent = new Intent(TravelMenuActivity.this, CarTravelActivity.class);
                itemIntent.putExtra(IntentConstants.TRAVEL_ID, travel.getId());
                itemIntent.putExtra(IntentConstants.ITEM_ID, carTravels.get(childPosition).getId());
                itemIntent.putExtra(IntentConstants.EDIT_FLAG, true);
                startActivity(itemIntent);
                return false;
            }
        });

        // Sets listeners to children of the event view
        eventView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                        int childPosition, long id) {
                ExpandableListAdapter adapter =
                        (ExpandableListAdapter) eventView.getExpandableListAdapter();

                String event = adapter.getChild(groupPosition, childPosition).toString();

                Intent resultIntent = new Intent(TravelMenuActivity.this,
                        EventActivity.class);

                resultIntent.putExtra(IntentConstants.ITEM_NAME, event);
                resultIntent.putExtra(IntentConstants.EDIT_FLAG, true);
                startActivityForResult(resultIntent, IntentConstants.REQUEST_NEW_EVENT);
                return false;
            }
        });

        // Sets listeners to children of the comment view
        commentView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                        int childPosition, long id) {
                ExpandableListAdapter adapter =
                        (ExpandableListAdapter) commentView.getExpandableListAdapter();

                String comment = adapter.getChild(groupPosition, childPosition).toString();

                Intent resultIntent = new Intent(TravelMenuActivity.this,
                        CommentActivity.class);

                resultIntent.putExtra(IntentConstants.ITEM_NAME, comment);
                resultIntent.putExtra(IntentConstants.EDIT_FLAG, true);
                startActivityForResult(resultIntent, IntentConstants.REQUEST_NEW_COMMENT);
                return false;
            }
        });

        updateInformation();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateInformation();
    }


    /**
     * Sets click listeners to children of the receipt views.
     * @param receiptView The receipt view to set child listeners for.
     * @param category The category of the receipt view.
     */
    private void setReceiptChildListener(ExpandableListView receiptView, final int category) {
        receiptView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                        int childPosition, long id) {
                try {
                    Intent itemIntent = new Intent(TravelMenuActivity.this, ReceiptActivity.class);
                    itemIntent.putExtra(IntentConstants.TRAVEL_ID, travel.getId());
                    List<Receipt> validReceipts = new ArrayList<>();
                    if (category == HOTEL) {
                        validReceipts = receiptsHotel;
                    }
                    if (category == TICKET) {
                        validReceipts = receiptsTicket;
                    }
                    if (category == CAR_RENTAL) {
                        validReceipts = receiptsCarRental;
                    }
                    if (category == MEAL) {
                        validReceipts = receiptsMeal;
                    }
                    if (category == OTHER) {
                        validReceipts = receiptsOther;
                    }
                    itemIntent.putExtra(IntentConstants.ITEM_ID,
                            validReceipts.get(childPosition).getId());
                    itemIntent.putExtra(IntentConstants.EDIT_FLAG, true);
                    startActivity(itemIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });
    }

    /**
     * Ensures that information is up to date both on creation and on resume.
     */
    private void updateInformation() {
        // Finds active travel or terminates activity
        getActiveTravel();

        // Sets correct visibility for all lists
        updateVisibility();

        // Sets the route found in active travel for quick information for user
        EditText route = (EditText) findViewById(R.id.travel_name);
        route.setText(travel.getRoute());
        route.setKeyListener(null); //Disallows changes to route

        // Sets updated adapters for list views and formats their height
        receiptHotelView.setAdapter(createReceiptsAdapter(hotelTypes, RECEIPT_HOTEL_HEADERS));
        receiptTicketView.setAdapter(createReceiptsAdapter(ticketTypes, RECEIPT_TICKET_HEADERS));
        receiptCarRentalView.setAdapter(createReceiptsAdapter(carRentalTypes,
                RECEIPT_RENTAL_CAR_HEADERS));
        receiptMealView.setAdapter(createReceiptsAdapter(mealTypes, RECEIPT_MEAL_HEADERS));
        receiptOtherView.setAdapter(createReceiptsAdapter(otherTypes, RECEIPT_OTHER_HEADERS));
        carTravelView.setAdapter(createCarTravelsAdapter());
        eventView.setAdapter(createEventsAdapter());
        commentView.setAdapter(createCommentsAdapter());
        ExpandableListUtil.setListHeight(expLists);
    }

    /**
     * Sets the active travel is one is found, finishes the activity otherwise.
     */
    private void getActiveTravel() {
        if (db.hasActiveTravel()) {
            travel = db.getActiveTravel();
        } else {
            finish();
        }
    }

    /**
     * Updates the visibility of the list views based on whether or not they contain data.
     */
    private void updateVisibility() {
        List<Receipt> updatedReceipts = db.getAllReceipts(travel.getId());
        Boolean[] found = {false, false, false, false, false};

        for (int i = 0; i < updatedReceipts.size(); i++) {
            int result = getCategory(updatedReceipts.get(i));
            if (result >= 0 && result < 5) {
                found[result] = true;
            }
        }
        if (found[HOTEL]) {
            receiptHotelView.setVisibility(View.VISIBLE);
        } else {
            receiptHotelView.setVisibility(View.GONE);
        }
        if (found[TICKET]) {
            receiptTicketView.setVisibility(View.VISIBLE);
        } else {
            receiptTicketView.setVisibility(View.GONE);
        }
        if (found[CAR_RENTAL]) {
            receiptCarRentalView.setVisibility(View.VISIBLE);
        } else {
            receiptCarRentalView.setVisibility(View.GONE);
        }
        if (found[MEAL]) {
            receiptMealView.setVisibility(View.VISIBLE);
        } else {
            receiptMealView.setVisibility(View.GONE);
        }
        if (found[OTHER]) {
            receiptOtherView.setVisibility(View.VISIBLE);
        } else {
            receiptOtherView.setVisibility(View.GONE);
        }

        if (db.getAllCarTravels(travel.getId()).size() > 0) {
            carTravelView.setVisibility(View.VISIBLE);
        } else {
            carTravelView.setVisibility(View.GONE);
        }
        if (travel.getEvents().size() > 0) {
            eventView.setVisibility(View.VISIBLE);
        } else {
            eventView.setVisibility(View.GONE);
        }
        if (travel.getComments().size() > 0) {
            commentView.setVisibility(View.VISIBLE);
        } else {
            commentView.setVisibility(View.GONE);
        }
    }

    /**
     * Returns the category of a receipt.
     * @return the category constant
     */
    private int getCategory(Receipt receipt) {
        for (int i = 0; i < hotelTypes.size(); i++) {
            if (receipt.getType().equals(hotelTypes.get(i))) {
                return HOTEL;
            }
        }
        for (int i = 0; i < ticketTypes.size(); i++) {
            if (receipt.getType().equals(ticketTypes.get(i))) {
                return TICKET;
            }
        }
        for (int i = 0; i < carRentalTypes.size(); i++) {
            if (receipt.getType().equals(carRentalTypes.get(i))) {
                return CAR_RENTAL;
            }
        }
        for (int i = 0; i < mealTypes.size(); i++) {
            if (receipt.getType().equals(mealTypes.get(i))) {
                return MEAL;
            }
        }
        for (int i = 0; i < otherTypes.size(); i++) {
            if (receipt.getType().equals(otherTypes.get(i))) {
                return OTHER;
            }
        }
        return -1;
    }

    /**
     * Creates and returns an adapter for the receipt view.
     * @return An expandable list adapter formatted for the receipt view.
     */
    private ExpandableListAdapter createReceiptsAdapter(List<String> typeList, List<String> headers) {
        HashMap<String, List<String>> childList = new HashMap<>();

        receipts = db.getAllReceipts(travel.getId());
        List<Receipt> validReceipts = new ArrayList<>();

        for (int i = 0; i < receipts.size(); i++) {
            Boolean match = false;
            for (int j = 0; j < typeList.size() && !match; j++) {
                if (receipts.get(i).getType().equals(typeList.get(j))) {
                    validReceipts.add(receipts.get(i));
                    match = true;
                }
            }
        }

        // Sets sublists of receipts based on input type list
        if(typeList == hotelTypes) {
            receiptsHotel = validReceipts;
        }
        if(typeList == ticketTypes) {
            receiptsTicket = validReceipts;
        }
        if(typeList == carRentalTypes) {
            receiptsCarRental = validReceipts;
        }
        if(typeList == mealTypes) {
            receiptsMeal = validReceipts;
        }
        if(typeList == otherTypes) {
            receiptsOther = validReceipts;
        }

        List<String> receiptStrings = new ArrayList<>();
        for (int i = 0; i < validReceipts.size(); i++) {
            Receipt receipt = validReceipts.get(i);
            receiptStrings.add(DateParserUtil.formatDate(receipt.getDate()) + newline +
                    receipt.getName() + " - " + String.format("%.2f", receipt.getTotal()) + " " +
                    receipt.getCurrency());
        }
        childList.put(headers.get(0), receiptStrings);

        return new ExpandableListAdapter(this, headers, childList);
    }

    /**
     * Creates and returns an adapter for the car travel view.
     * @return An expandable list adapter formatted for the car travel view.
     */
    private ExpandableListAdapter createCarTravelsAdapter() {
        HashMap<String, List<String>> childList = new HashMap<>();

        carTravels = db.getAllCarTravels(travel.getId());

        List<String> carTravelStrings = new ArrayList<>();
        for (int i = 0; i < carTravels.size(); i++) {
            CarTravel carTravel = carTravels.get(i);
            carTravelStrings.add(DateParserUtil.formatDate(carTravel.getDate()) + newline +
                    carTravel.getStart() + " - " + carTravel.getEnd() + " = " +
                    carTravel.getKilometers() + " km");
        }

        childList.put(CAR_TRAVEL_HEADERS.get(0), carTravelStrings);

        return new ExpandableListAdapter(this, CAR_TRAVEL_HEADERS, childList);
    }

    /**
     * Creates and returns an adapter for the events view.
     * @return An expandable list adapter formatted for the event view.
     */
    private ExpandableListAdapter createEventsAdapter() {
        HashMap<String, List<String>> childList = new HashMap<>();

        events = travel.getEvents();

        childList.put(EVENT_HEADERS.get(0), events);

        return new ExpandableListAdapter(this, EVENT_HEADERS, childList);
    }

    /**
     * Creates and returns an adapter for the comment view.
     * @return An expandable list adapter formatted for the comment view.
     */
    private ExpandableListAdapter createCommentsAdapter() {
        HashMap<String, List<String>> childList = new HashMap<>();

        comments = travel.getComments();

        childList.put(COMMENT_HEADERS.get(0), comments);

        return new ExpandableListAdapter(this, COMMENT_HEADERS, childList);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == IntentConstants.REQUEST_NEW_EVENT) {
            if (resultCode == IntentConstants.RESULT_DELETE) {
                events.remove(data.getStringExtra(IntentConstants.ITEM_ID));
            }
            if (resultCode == IntentConstants.RESULT_CREATE) {
                events.add(data.getStringExtra(IntentConstants.ITEM_NAME));
            }
            if (resultCode == IntentConstants.RESULT_EDIT) {
                events.remove(data.getStringExtra(IntentConstants.ITEM_ID));
                events.add(data.getStringExtra(IntentConstants.ITEM_NAME));
            }
            travel.setEvents(events);
            eventView.collapseGroup(0);
            ExpandableListUtil.setListHeight(Collections.singletonList(eventView));

        }
        if (requestCode == IntentConstants.REQUEST_NEW_COMMENT) {
            if (resultCode == IntentConstants.RESULT_DELETE) {
                comments.remove(data.getStringExtra(IntentConstants.ITEM_ID));
            }
            if (resultCode == IntentConstants.RESULT_CREATE) {
                comments.add(data.getStringExtra(IntentConstants.ITEM_NAME));
            }
            if (resultCode == IntentConstants.RESULT_EDIT) {
                comments.remove(data.getStringExtra(IntentConstants.ITEM_ID));
                comments.add(data.getStringExtra(IntentConstants.ITEM_NAME));
            }
            travel.setComments(comments);
            commentView.collapseGroup(0);
            ExpandableListUtil.setListHeight(Collections.singletonList(commentView));
        }
        db.updateTravel(travel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.receipt_button:
                Intent receiptIntent = new Intent(this, ReceiptActivity.class);
                receiptIntent.putExtra(IntentConstants.TRAVEL_ID, travel.getId());
                startActivity(receiptIntent);
                break;
            case R.id.car_travel_button:
                Intent carIntent = new Intent(this, CarTravelActivity.class);
                carIntent.putExtra(IntentConstants.TRAVEL_ID, travel.getId());
                startActivity(carIntent);
                break;
            case R.id.event_button:
                startActivityForResult(new Intent(this, EventActivity.class),
                        IntentConstants.REQUEST_NEW_EVENT);
                break;
            case R.id.comment_button:
                startActivityForResult(new Intent(this, CommentActivity.class),
                        IntentConstants.REQUEST_NEW_COMMENT);
                break;
            case R.id.edit_travel_button:
                Intent editIntent = new Intent(this, TravelActivity.class);
                editIntent.putExtra(IntentConstants.EDIT_FLAG, true);
                editIntent.putExtra(IntentConstants.TRAVEL_ID, travel.getId());
                startActivity(editIntent);
                break;
            case R.id.send_travel_button:
                startActivity(new Intent(this, SendActivity.class));
                break;
        }
    }
}
