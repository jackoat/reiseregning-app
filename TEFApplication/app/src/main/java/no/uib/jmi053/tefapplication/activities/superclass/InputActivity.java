package no.uib.jmi053.tefapplication.activities.superclass;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import java.util.ArrayList;
import java.util.List;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.utilities.constants.PopupConstants;
import no.uib.jmi053.tefapplication.utilities.popups.DialogUtil;
import no.uib.jmi053.tefapplication.utilities.popups.ToastUtil;
import no.uib.jmi053.tefapplication.widgets.FontEdit;

/**
 * Created by JoaT Development on 05.10.2015.
 *
 * A superclass of activities with input fields that implements constraints.
 */
public abstract class InputActivity extends TEFActivity {

    // A list of input fields strings created on startup
    protected List<String> inputStringsAtStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        if(hasBeenEdited()) {
            showInformationLossDialog();
        } else {
            finish();
        }
    }

    /**
     * Returns the item name of the input activity.
     * @return A string representing the name of the item.
     */
    protected abstract String getItemName();

    /**
     * Puts all edit fields (FontEdit) in the protected list 'editFields'.
     */
    protected abstract List<FontEdit> createEditFieldList();

    /**
     * Puts all edit fields (including pickers and spinners) in one of the protected complete lists.
     */
    protected abstract List<String> getInputStrings();

    /**
     * Checks if all input fields have information stored in them.
     * @return True if all fields are filled, false otherwise.
     */
    protected boolean hasUnfilledFields() {
        // A list of input fields that can contain null values
        List<FontEdit> editFields = createEditFieldList();

        boolean filled = false;
        for (FontEdit fe : editFields) {
            if(fe.getText().toString().equals("")) {
                filled = true;
                break;
            }
        }
        return filled;
    }

    /**
     * Checks if there exists a difference between input strings at start of activity and on check
     * for edit.
     * @return True if there is a difference, false otherwise.
     */
    protected boolean hasBeenEdited() {
        boolean edited = false;

        // Finds strings at time of check
        List<String> inputStringsAtCheck = getInputStrings();

        // If the lists have different sizes they are not equal and we know we have editing
        if(inputStringsAtStart.size() != inputStringsAtCheck.size()) {
            return true;
        }

        // Loops until difference found or until all strings are checked
        for (int i = 0; i < inputStringsAtStart.size(); i++) {
            if(!inputStringsAtStart.get(i).equals(inputStringsAtCheck.get(i))) {
                edited = true;
                break;
            }
        }

        return edited;
    }

    /**
     * Shows a toast if there is a constraint violation from lacking input information.
     */
    protected void showUnfilledFieldsToast() {
        ToastUtil.createWarningToast(
                this, getString(R.string.toast_warning_unfilled_fields_text))
                .show();
    }

    /**
     * Shows a confirmation dialog to a user which attempts to finish an unsaved activity.
     */
    protected void showInformationLossDialog() {
        final Dialog dialog = new Dialog(this);

        // Creates the negative button listener
        View.OnClickListener negativeListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(PopupConstants.DIALOG_INFORMATION_LOSS,
                        "User denied loss of information.");
                v.setClickable(true);
                dialog.dismiss();
            }
        };

        // Creates the positive button listener
        View.OnClickListener positiveListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(PopupConstants.DIALOG_INFORMATION_LOSS,
                        "User confirmed loss of information.");
                v.setClickable(true);
                finish();
                dialog.dismiss();
            }
        };

        // Customizes the created dialog
        DialogUtil.createAlertDialog(
                this, dialog, getString(R.string.dialog_information_loss_message),
                getString(R.string.dialog_information_loss_negative_button),
                getString(R.string.dialog_information_loss_positive_button),
                negativeListener, positiveListener)
                .show();
    }

    protected void showDeleteConfirmDialog(final Dialog dialog,
                                           final View.OnClickListener positiveListener) {
        // Creates the negative button listener
        View.OnClickListener negativeListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(PopupConstants.DIALOG_DELETION,
                        "User denied " + getItemName() + " deletion.");
                v.setClickable(true);
                dialog.dismiss();
            }
        };

        // Customizes the created dialog
        String messageString = getString(R.string.dialog_deletion_message_prefix) + getItemName()
                + getString(R.string.dialog_deletion_message_postfix);
        DialogUtil.createAlertDialog(
                this, dialog, messageString,
                getString(R.string.dialog_deletion_negative_button),
                getString(R.string.dialog_deletion_positive_button),
                negativeListener, positiveListener)
                .show();
    }

    /**
     * Removes the onscreen keyboard when touching outside of the FontEdits
     * @param event the touch event to be handled
     * @return return true if the event was handled
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof FontEdit) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            boolean hide = true;

            View view = ((ViewGroup)findViewById(android.R.id.content)).getChildAt(0);
            ArrayList<View> fontEdits = view.getFocusables(View.FOCUS_BACKWARD);     // Get All FontEdits in view

            for(int i=0; i< fontEdits.size(); i++){
                View editText = fontEdits.get(i);
                editText.getLocationOnScreen(scrcoords);
                float x = event.getRawX();
                float y = event.getRawY();
                int viewX = scrcoords[0];
                int viewY = scrcoords[1];

                // If touch is in any of EditText, keep keyboard active, otherwise hide it.
                if (event.getAction() == MotionEvent.ACTION_UP  && ( x > viewX && x < (viewX + editText.getWidth())) && ( y > viewY && y < (viewY + editText.getHeight())) ) {
                    hide = false;
                }
            }

            if (hide) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }
}
