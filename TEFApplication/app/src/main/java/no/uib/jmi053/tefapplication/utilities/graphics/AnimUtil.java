package no.uib.jmi053.tefapplication.utilities.graphics;

import android.app.Activity;

import no.uib.jmi053.tefapplication.R;

/**
 * Created by JoaT Development on 10/09/2015.
 *
 * Contains methods for handling animations.
 */
public abstract class AnimUtil {

    /**
     * Overrides the transitions between activities.
     * @param activity The activity to override.
     */
    public static void overrideForwardTransition(Activity activity) {
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void overrideBackTransition(Activity activity) {
        activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
