package no.uib.jmi053.tefapplication.io;

        import java.util.Date;
        import java.util.Properties;

        import javax.activation.CommandMap;
        import javax.activation.DataHandler;
        import javax.activation.DataSource;
        import javax.activation.FileDataSource;
        import javax.activation.MailcapCommandMap;
        import javax.mail.BodyPart;
        import javax.mail.Multipart;
        import javax.mail.PasswordAuthentication;
        import javax.mail.Session;
        import javax.mail.Transport;
        import javax.mail.internet.InternetAddress;
        import javax.mail.internet.MimeBodyPart;
        import javax.mail.internet.MimeMessage;
        import javax.mail.internet.MimeMultipart;

/**
 * Created by JoaT Development on 02.10.2015.
 *
 * Handles sending an email from a mail-client.
 */
public class MailSender {

    private String user;
    private String pass;
    private String[] to;
    private String from;
    private String port;
    private String host;
    private String subject;
    private String body;
    private boolean auth;
    private boolean debuggable;
    private Multipart multipart;

    /**
     * Creates a new MailSender with default values
     */
    public MailSender() {
        host = "smtp.gmail.com";
        port = "465";
        user = "";
        pass = "";
        from = "";
        subject = "";
        body = "";

        debuggable = false; // debug mode on or off - default off
        auth = true; // smtp authentication - default on

        multipart = new MimeMultipart();

        // There is something wrong with MailCap, javamail can not find a handler for the multipart/mixed part, so this bit needs to be added.
        MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
        CommandMap.setDefaultCommandMap(mc);
    }

    /**
     * Creates a MailSender given a username and password
     * @param user the username
     * @param pass the password
     */
    public MailSender(String user, String pass) {
        this();
        this.user = user;
        this.pass = pass;
    }

    /**
     * Internal Authenticator for username/password authentication
     */
    private class SMTPAuthenticator extends javax.mail.Authenticator
    {
        public PasswordAuthentication getPasswordAuthentication()
        {
            return new PasswordAuthentication(user, pass);
        }
    }

    /**
     * Sends a generated email
     * @return whether the send was successfull
     * @throws Exception
     */
    public boolean send() throws Exception {
        //Sets properties for sending
        Properties props = setProperties();

        //Check if neccessary fields are set
        if(!user.equals("") && !pass.equals("") && to.length > 0 && !from.equals("") && !subject.equals("") && !body.equals("")) {
            //Create a new Session and a message
            SMTPAuthenticator auth = new SMTPAuthenticator();
            Session session = Session.getInstance(props, auth);
            MimeMessage msg = new MimeMessage(session);

            //Set to and from addresses
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] addressTo = new InternetAddress[to.length];
            for (int i = 0; i < to.length; i++) {
                addressTo[i] = new InternetAddress(to[i]);
            }
            msg.setRecipients(MimeMessage.RecipientType.TO, addressTo);

            //Subject
            msg.setSubject(subject);
            msg.setSentDate(new Date());

            // setup message body
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText(body);
            multipart.addBodyPart(messageBodyPart);

            // Put parts in message
            msg.setContent(multipart);

            // Send email
            Transport t = session.getTransport("smtps");
            t.connect(host, Integer.parseInt(port), user, pass);
            t.send(msg);
            t.close();

            return true;
        } else {
            return false;
        }
    }

    /**
     * Adds an attachment to the message
     * @param filePath the file to attach
     * @param filename the name to give the file
     * @throws Exception
     */
    public void addAttachment(String filePath, String filename) throws Exception {
        BodyPart messageBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(filePath);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(filename);

        multipart.addBodyPart(messageBodyPart);
    }

    /**
     * Sets the properties for a Session
     * @return the properties
     */
    private Properties setProperties() {
        Properties props = new Properties();

        props.put("mail.smtp.host", host);

        if(debuggable) {
            props.put("mail.debug", "true");
        }

        if(auth) {
            props.put("mail.smtp.auth", "true");
        }

        props.put("mail.smtp.user", user);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.ssl.trust", host);
        props.setProperty("mail.smtp.quitwait", "false");

        return props;
    }

    // The setters
    public void setBody(String _body) {
        this.body = _body;
    }

    public void setTo(String[] toArr) {
        this.to =toArr;
    }

    public void setFrom(String string) {
        this.from =string;
    }

    public void setSubject(String string) {
        this.subject =string;
    }

}
