package no.uib.jmi053.tefapplication.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import no.uib.jmi053.tefapplication.R;

/**
 * Created by JoaT Development on 09/09/2015.
 *
 * Custom EditText with a custom font from assets.
 */
public class FontEdit extends EditText {

    /**
     * Creates a FontEdit with a predefined font.
     * @param context The activity to get resources from.
     * @param attrs The attribute set of the connected layout.
     */
    public FontEdit(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Typeface.createFromAsset(
                getContext().getAssets(), getResources().getString(R.string.font_roboto_regular)));
    }
}
