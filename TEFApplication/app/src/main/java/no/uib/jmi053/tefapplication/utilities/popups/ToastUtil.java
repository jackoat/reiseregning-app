package no.uib.jmi053.tefapplication.utilities.popups;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.widgets.FontView;

/**
 * Created by JoaT Development on 06.10.2015.
 *
 * Utility class for creating custom toasts.
 */
public abstract class ToastUtil {

    /**
     * Creates a customized warning toast with a given message.
     * @param activity The activity which creates the toast.
     * @param messageString The message to be provided by the toast.
     * @return The customized toast.
     */
    public static Toast createWarningToast(Activity activity, String messageString) {
        // Creates a custom toast
        Toast toast = new Toast(activity.getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);

        // Finds and sets the layout for the toast
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_warning,
                (ViewGroup) activity.findViewById(R.id.toast_warning_layout));
        FontView message = (FontView) layout.findViewById(R.id.toast_warning_message);
        message.setText(messageString);
        toast.setView(layout);

        // Places the toast in the center of the screen
        toast.setGravity(Gravity.CENTER, 0, 0);

        return toast;
    }

    public static Toast createInfoToast(Activity activity, String messageString) {
        // Creates a custom toast
        Toast toast = new Toast(activity.getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);

        // Finds and sets the layout for the toast
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_info,
                (ViewGroup) activity.findViewById(R.id.toast_info_layout));
        FontView message = (FontView) layout.findViewById(R.id.toast_info_message);
        message.setText(messageString);
        toast.setView(layout);

        // Places the toast in the center of the screen
        toast.setGravity(Gravity.CENTER, 0, 0);

        return toast;
    }
}
