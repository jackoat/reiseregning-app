package no.uib.jmi053.tefapplication.utilities;

import android.content.Context;

import no.uib.jmi053.tefapplication.R;

/**
 * Created by JoaT Development on 28/09/2015.
 *
 * Contains functionality for retrieving information about constant positions in string arrays.
 */
public abstract class ArrayPositionUtil {

    public static final int HOTEL_INDEX = 0;
    public static final int MEAL_REPRESENTATION_INDEX = 7;

    /**
     * Returns the index of the given category from the string array from resources.
     * @param context The context to get resources from.
     * @param type The type string which to compare for.
     * @param arrayID The resource ID of the string array.
     * @return The index of the given type in category array.
     */
    public static int getTypeArrayIndex(Context context, String type, int arrayID) {
        String[] types = context.getResources().getStringArray(arrayID);

        int index = 0;

        for(int i = 0; i < types.length; i++) {
            if(type.equals(types[i])) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     * Checks if a receipt type is of hotel.
     * @param context The context to get resources from.
     * @param type The type string which to compare for.
     * @return True if the receipt is of type hotel, false otherwise.
     */
    public static boolean hasHotelPosition(Context context, String type) {
        String[] types = context.getResources().getStringArray(R.array.receipt_type_categories);

        boolean hotel = false;

        if(type.equals(types[HOTEL_INDEX])) {
            hotel = true;
        }

        return hotel;
    }

    /**
     * Checks if a receipt type is of meal representation.
     * @param context The context to get resources from.
     * @param type The type string which to compare for.
     * @return True if the receipt is of type meal representation, false otherwise.
     */
    public static boolean hasMealRepresentationPosition(Context context, String type) {
        String[] types = context.getResources().getStringArray(R.array.receipt_type_categories);

        boolean representation = false;

        if(type.equals(types[MEAL_REPRESENTATION_INDEX])) {
            representation = true;
        }

        return representation;
    }

}
