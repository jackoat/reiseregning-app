package no.uib.jmi053.tefapplication.activities.inputs;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.fragments.AccountActivity;
import no.uib.jmi053.tefapplication.activities.superclass.InputActivity;
import no.uib.jmi053.tefapplication.adapters.ExpandableListAdapter;
import no.uib.jmi053.tefapplication.entities.User;
import no.uib.jmi053.tefapplication.utilities.constants.IntentConstants;
import no.uib.jmi053.tefapplication.utilities.constants.PopupConstants;
import no.uib.jmi053.tefapplication.utilities.graphics.ExpandableListUtil;
import no.uib.jmi053.tefapplication.utilities.graphics.GraphicsUtil;
import no.uib.jmi053.tefapplication.utilities.popups.ToastUtil;
import no.uib.jmi053.tefapplication.widgets.FontButton;
import no.uib.jmi053.tefapplication.widgets.FontEdit;
import no.uib.jmi053.tefapplication.widgets.FontView;

/**
 * Created by JoaT Development 16.09.15.
 *
 * Activity for adding or editing personal information for a user.
 */
public class UserActivity extends InputActivity implements View.OnClickListener {

    // Input widgets
    FontEdit ssn;
    FontEdit name;
    FontEdit address;
    FontEdit postCode;
    FontEdit email;
    FontEdit employerEmail;
    FontEdit teamCode;
    FontEdit company;
    ExpandableListView accountView;

    // Account map
    HashMap<String, Long> accountMap;

    // Flag
    boolean beingEdited;
    Boolean showToast;

    // Create and save button
    FontButton button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info);

        // Finds and sets icon to the create button
        button = (FontButton) findViewById(R.id.personal_save_button);
        GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_create);

        // Finds all input fields and expandable lists
        ssn = (FontEdit) findViewById(R.id.personal_ssn_input);
        name = (FontEdit) findViewById(R.id.personal_name_input);
        address = (FontEdit) findViewById(R.id.personal_address_input);
        postCode = (FontEdit) findViewById(R.id.personal_post_code_input);
        email = (FontEdit) findViewById(R.id.personal_email_input);
        employerEmail = (FontEdit) findViewById(R.id.personal_employer_email_input);
        teamCode = (FontEdit) findViewById(R.id.personal_team_code_input);
        company = (FontEdit) findViewById(R.id.personal_company_input);
        accountView = (ExpandableListView) findViewById(R.id.personal_account_list);

        // Checks if the user is being edited
        beingEdited = hasUser();

        // Sets the toast flag to allow toasts to be shown
        showToast = true;

        // If being edited then the account map is set from existing user
        if(beingEdited) {
            accountMap = db.getAllUsers().get(0).getAccounts();
        } else {
            accountMap = new HashMap<>();
        }

        // Sets listeners to children of the account view
        accountView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                        int childPosition, long id) {
                ExpandableListAdapter adapter =
                        (ExpandableListAdapter) accountView.getExpandableListAdapter();

                String accountName = adapter.getChild(groupPosition, childPosition).toString();

                Intent resultIntent = new Intent(UserActivity.this,
                        AccountActivity.class);

                resultIntent.putExtra(IntentConstants.ITEM_NAME, accountName);
                resultIntent.putExtra(IntentConstants.ITEM_NUMBER, accountMap.get(accountName));
                resultIntent.putExtra(IntentConstants.EDIT_FLAG, true);
                startActivityForResult(resultIntent, IntentConstants.REQUEST_NEW_ITEM);
                return false;
            }
        });

        // Updates information for list view and format height
        accountView.setAdapter(createAccountsAdapter());
        ExpandableListUtil.setListHeight(Collections.singletonList(accountView));

        updateInformation();

        inputStringsAtStart = getInputStrings();
    }

    @Override
    protected String getItemName() {
        return "user";
    }

    @Override
    protected List<FontEdit> createEditFieldList() {
        return Arrays.asList(ssn, name, address, postCode, email, employerEmail, company,
                teamCode);
    }

    @Override
    protected List<String> getInputStrings() {
        List<String> tempStrings = Arrays.asList(
                ssn.getText().toString(),
                name.getText().toString(),
                address.getText().toString(),
                postCode.getText().toString(),
                email.getText().toString(),
                employerEmail.getText().toString(),
                teamCode.getText().toString(),
                company.getText().toString());

        // Create new list to remove fixed size
        List<String> inputStrings = new ArrayList<>();
        inputStrings.addAll(tempStrings);

        for (String accName : new ArrayList<>(accountMap.keySet())) {
            inputStrings.add(accName);
            inputStrings.add(String.valueOf(accountMap.get(accName)));
        }
        return inputStrings;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    /**
     * Ensures that information is up to date on editing.
     */
    private void updateInformation() {
        if(beingEdited) {
            // Finds the existing user
            User user = db.getAllUsers().get(0);

            // Disallows changes to SSN
            ssn.setKeyListener(null);

            // Splits address into street address (split[0]) and post code (split[1]).
            String[] split = user.getAddress().split(",");

            // Sets new titles for editing
            FontView header = (FontView) findViewById(R.id.header);
            header.setText(R.string.edit_personal_info_header);
            setTitle(R.string.title_activity_edit_personal_info);

            // Sets new information based on found user
            ssn.setText(String.format("%011d", user.getSsn()));
            name.setText(user.getName());
            address.setText(split[0]);
            postCode.setText(split[1].substring(1, split[1].length())); // Removes space from start
            email.setText(user.getEmail());
            employerEmail.setText(user.getEmployerEmail());
            company.setText(user.getCompany());
            teamCode.setText(String.valueOf(user.getDepartment()));

            // Changes create button text and icon to save
            button.setText(R.string.button_save);
            GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_save);

            // Makes delete button visible
            FontButton delete = (FontButton) findViewById(R.id.personal_delete_button);
            delete.setVisibility(FontButton.VISIBLE);

            updateAccVisibility();
        }
    }

    /**
     * Updates the visibility of the account view based on whether or not it contains data.
     */
    private void updateAccVisibility() {
        if(accountMap.size() > 0) {
            accountView.setVisibility(ExpandableListView.VISIBLE);
            accountView.setAdapter(createAccountsAdapter());
            accountView.collapseGroup(0);
            ExpandableListUtil.setListHeight(Collections.singletonList(accountView));
        } else {
            accountView.setVisibility(ExpandableListView.GONE);
        }
    }

    /**
     * Checks if there is an existing user.
     * @return True if there is a stored user, false otherwise.
     */
    private boolean hasUser() {
        return db.getAllUsers().size() != 0;
    }

    /**
     * Creates and returns an adapter for the account view.
     * @return An expandable list adapter formatted for the account view.
     */
    private ExpandableListAdapter createAccountsAdapter() {
        HashMap<String, List<String>> childList = new HashMap<>();

        List<String> headerList = new ArrayList<>();
        headerList.add(getResources().getString(R.string.personal_accounts_list));

        childList.put(headerList.get(0), new ArrayList<>(accountMap.keySet()));

        return new ExpandableListAdapter(this, headerList, childList);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == IntentConstants.RESULT_DELETE) {
            accountMap.remove(data.getStringExtra(IntentConstants.ITEM_ID));
            accountView.collapseGroup(0);
            ExpandableListUtil.setListHeight(Collections.singletonList(accountView));
        }
        if (resultCode == IntentConstants.RESULT_CREATE) {
            long newAccountNumber = data.getLongExtra(IntentConstants.ITEM_NUMBER, -1);

            if (newAccountNumber != -1) {
                accountMap.put(data.getStringExtra(IntentConstants.ITEM_NAME), newAccountNumber);
            } else {
                Log.d(TAG, "New account number not found.");
            }
        }
        if (resultCode == IntentConstants.RESULT_EDIT) {
            accountMap.remove(data.getStringExtra(IntentConstants.ITEM_ID));

            long newAccountNumber = data.getLongExtra(IntentConstants.ITEM_NUMBER, -1);

            if (newAccountNumber != -1) {
                accountMap.put(data.getStringExtra(IntentConstants.ITEM_NAME), newAccountNumber);
            } else {
                Log.d(TAG, "New account number not found.");
            }
        }
        if(requestCode == IntentConstants.REQUEST_NEW_ITEM) {
            updateAccVisibility();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.personal_save_button:
                if (!hasUnfilledFields()) {
                    showToast = true;
                    if (ssn.getText().toString().length() == 11) {
                        if(accountMap.size() > 0 ) {
                            User newUser = new User(Long.parseLong(ssn.getText().toString()),
                                    name.getText().toString(), address.getText().toString() +
                                    ", " + postCode.getText().toString(),
                                    email.getText().toString(),
                                    employerEmail.getText().toString(),
                                    company.getText().toString(),
                                    Integer.parseInt(teamCode.getText().toString()));
                            newUser.setAccounts(accountMap);
                            if (!beingEdited) {
                                db.insertUser(newUser);
                            } else {
                                db.updateUser(newUser);
                            }
                            finish();
                        } else {
                            if (showToast){
                                ToastUtil.createWarningToast(this, getString(
                                        R.string.toast_warning_no_account_found))
                                        .show();
                                showToast = false;
                            }
                        }
                    } else {
                        if (showToast){
                            ToastUtil.createWarningToast(this, getString(
                                    R.string.toast_warning_invalid_ssn_length))
                                    .show();
                            showToast = false;
                        }
                    }
                } else {
                    if (showToast){
                        showUnfilledFieldsToast();
                        showToast = false;
                    }
                }
                break;
            case R.id.personal_delete_button:
                final Dialog dialog = new Dialog(this);
                // Creates the positive button listener
                View.OnClickListener positiveListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(PopupConstants.DIALOG_DELETION,
                                "User confirmed " + getItemName() + " deletion.");
                        v.setClickable(true);
                        db.deleteUser(Long.parseLong(ssn.getText().toString()));
                        Intent restartIntent = getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage(getBaseContext().getPackageName());
                        restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(restartIntent);
                    }
                };
                showDeleteConfirmDialog(dialog, positiveListener);
                break;
            case R.id.personal_account_add_button:
                startActivityForResult(new Intent(this, AccountActivity.class),
                        IntentConstants.REQUEST_NEW_ITEM);
                break;
        }
    }

    /**
     * Creates test values for input fields which are undeclared. Method for testing.
     */
    private void createTestInput() {
        if (ssn.getText().toString().equals("")) {
            ssn.setText("01020312345");
        }
        if (name.getText().toString().equals("")) {
            name.setText("Joe Middle");
        }
        if (address.getText().toString().equals("")) {
            address.setText("Svalbardsgaten 1");
        }
        if (postCode.getText().toString().equals("")) {
            postCode.setText("9171");
        }
        if (email.getText().toString().equals("")) {
            email.setText("joeymidboy@gmail.com");
        }
        if (employerEmail.getText().toString().equals("")){
            employerEmail.setText("joar.mi@sklbb.no");
        }
        if (company.getText().toString().equals("")) {
            company.setText("Bergen Energi AS");
        }
        if (teamCode.getText().toString().equals("")) {
            teamCode.setText("1234");
        }
        accountMap.put("Primary", 45870017804L);
    }
}
