package no.uib.jmi053.tefapplication.activities;

import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;

import java.io.IOException;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.superclass.TEFActivity;
import no.uib.jmi053.tefapplication.io.PhotoHandler;
import no.uib.jmi053.tefapplication.utilities.CameraUtils;

/**
 * Created by JoaT Development on 04.10.2015.
 *
 * Activity for taking receipt pictures.
 */
public class CameraActivity extends TEFActivity implements View.OnClickListener, SurfaceHolder.Callback {

    // Camera fields
    private Camera camera;
    private SurfaceView cameraFrame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        if(!OpenCVLoader.initDebug()) {
            Log.d(TAG, "The fuck is going on?");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!CameraUtils.deviceHasCamera(this)) {
            Toast.makeText(this, "No camera found on this device.", Toast.LENGTH_LONG).show();
        } else {
            int cameraId = findActiveCamera();
            if(cameraId < 0) {
                Toast.makeText(this, "No front facing camera found.", Toast.LENGTH_LONG).show();
            } else {
                camera = Camera.open(cameraId);

                cameraFrame = (SurfaceView) findViewById(R.id.camera_frame);
                SurfaceHolder surfaceHolder = cameraFrame.getHolder();
                surfaceHolder.addCallback(this);
                surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

                cameraFrame.setOnClickListener(this);
            }
        }
    }

    @Override
    protected void onPause() {
        if(camera != null) {
            camera.release();
            camera = null;

            cameraFrame.getHolder().removeCallback(this);
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setResult(RESULT_CANCELED, new Intent());
    }

    /**
     * Returns the ID of the active camera.
     * @return An int containing the ID of the active camera.
     */
    private int findActiveCamera() {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                Log.d(TAG, "Camera found");
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            camera.setPreviewDisplay(holder);
            // Sets orientation to portrait
            camera.setDisplayOrientation(90);
            // Sets auto focus to continuous
            Camera.Parameters cameraParameters = camera.getParameters();
            cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            camera.setParameters(cameraParameters);
            // Starts the camera preview
            camera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {}

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera_shutter_button:;
                camera.takePicture(null, null, new PhotoHandler(this, getIntent()));
                break;
        }
    }
}
