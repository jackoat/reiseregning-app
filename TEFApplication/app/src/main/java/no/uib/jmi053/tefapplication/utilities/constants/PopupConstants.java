package no.uib.jmi053.tefapplication.utilities.constants;

/**
 * Created by JoaT Development on 11.10.2015.
 *
 * Contains constants for popups.
 */
public abstract class PopupConstants {

    public static final String DIALOG_INFORMATION_LOSS = "DialogInformationLoss";
    public static final String DIALOG_DELETION = "DialogDeletion";
}
