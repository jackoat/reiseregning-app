package no.uib.jmi053.tefapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import no.uib.jmi053.tefapplication.R;

/**
 * Created by JoaT Development 06.09.15.
 *
 * Startup activity presenting logo and application name.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(MainActivity.this, ParentActivity.class));
                finish();
            }
        }, 2000L);
    }
}
