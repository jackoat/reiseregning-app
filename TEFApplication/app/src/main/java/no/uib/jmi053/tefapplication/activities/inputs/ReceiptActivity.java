package no.uib.jmi053.tefapplication.activities.inputs;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.CameraActivity;
import no.uib.jmi053.tefapplication.activities.ReceiptImageActivity;
import no.uib.jmi053.tefapplication.activities.fragments.PersonActivity;
import no.uib.jmi053.tefapplication.activities.superclass.InputActivity;
import no.uib.jmi053.tefapplication.adapters.CursorAdapter;
import no.uib.jmi053.tefapplication.adapters.ExpandableListAdapter;
import no.uib.jmi053.tefapplication.entities.Receipt;
import no.uib.jmi053.tefapplication.entities.Stay;
import no.uib.jmi053.tefapplication.entities.Travel;
import no.uib.jmi053.tefapplication.io.DatabaseHandler;
import no.uib.jmi053.tefapplication.listeners.OnSwipeTouchListener;
import no.uib.jmi053.tefapplication.utilities.ArrayPositionUtil;
import no.uib.jmi053.tefapplication.utilities.CameraUtils;
import no.uib.jmi053.tefapplication.utilities.constants.DebugConstants;
import no.uib.jmi053.tefapplication.utilities.constants.IntentConstants;
import no.uib.jmi053.tefapplication.utilities.constants.PopupConstants;
import no.uib.jmi053.tefapplication.utilities.graphics.ExpandableListUtil;
import no.uib.jmi053.tefapplication.utilities.graphics.GraphicsUtil;
import no.uib.jmi053.tefapplication.utilities.parsers.DateParserUtil;
import no.uib.jmi053.tefapplication.utilities.popups.DialogUtil;
import no.uib.jmi053.tefapplication.utilities.popups.ToastUtil;
import no.uib.jmi053.tefapplication.widgets.FontButton;
import no.uib.jmi053.tefapplication.widgets.FontEdit;
import no.uib.jmi053.tefapplication.widgets.FontView;

/**
 * Created by JoaT Development 16.09.15.
 *
 * Activity for adding or editing information for a receipt.
 */
public class ReceiptActivity extends InputActivity implements View.OnClickListener {

    // Tesseract constants
    public static final String DATA_PATH = Environment
            .getExternalStorageDirectory().toString() + "/TEFApplication/sdcard";
    public static final String lang = "eng";
    // String recognizedText;

    // Camera image widgets and constants
    static final int CAMERA_REQUEST = 1888;
    ImageView receiptImage;
    ScrollView touchView;

    // Input widgets
    Spinner type;
    FontEdit name;
    DatePicker date;
    FontEdit total;
    FontEdit currency;
    FontEdit stayName;
    FontEdit stayAddress;
    DatePicker stayDate;
    FontEdit stayDuration;
    Spinner stayType;
    FontEdit representationPurpose;
    ExpandableListView representativesView;

    // Representatives list
    List<String> representatives;

    // Types list
    List<String> types;

    // Flags
    boolean beingEdited;
    boolean reimbursement;
    Boolean showToast;

    // ID of receipt
    int id;

    // ID of the connected travel
    int fk;

    // The create and save button
    FontButton button;

    // Image path
    String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_receipt);

        // Finds and sets icon to the create button
        button = (FontButton) findViewById(R.id.new_receipt_button);
        GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_create);

        // Finds camera image preview widgets
        receiptImage = (ImageView) findViewById(R.id.receipt_image);
        touchView = (ScrollView) findViewById(R.id.receipt_scroll);

        // Sets a swipe listener to the scroll view
        touchView.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                Intent intent = new Intent(ReceiptActivity.this, ReceiptImageActivity.class);
                intent.putExtra(IntentConstants.IMAGE_PATH, path);
                startActivity(intent);
            }

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        touchView.requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        touchView.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return gestureDetector.onTouchEvent(event);
            }
        });

        // Finds all input widgets and expandable lists
        type = (Spinner) findViewById(R.id.receipt_type_input);
        name = (FontEdit) findViewById(R.id.receipt_name_input);
        date = (DatePicker) findViewById(R.id.receipt_date_input);
        total = (FontEdit) findViewById(R.id.receipt_total_input);
        currency = (FontEdit) findViewById(R.id.receipt_currency_input);

        // Type dependent input widgets and list
        stayName = (FontEdit) findViewById(R.id.receipt_stay_name_input);
        stayAddress = (FontEdit) findViewById(R.id.receipt_stay_address_input);
        stayDate = (DatePicker) findViewById(R.id.receipt_stay_date_input);
        stayDuration = (FontEdit) findViewById(R.id.receipt_stay_duration_input);
        stayType = (Spinner) findViewById(R.id.receipt_stay_type_input);
        stayType.setAdapter(new CursorAdapter(this,
                Arrays.asList(getResources().getStringArray(R.array.receipt_stay_type_list))));
        representativesView = (ExpandableListView) findViewById(R.id.receipt_representatives_view);
        representationPurpose = (FontEdit) findViewById(R.id.receipt_representation_purpose_input);

        // Finds the ID of the receipt
        id = getIntent().getIntExtra(IntentConstants.ITEM_ID, -1);

        // Finds the ID of the connected travel
        fk = getIntent().getIntExtra(IntentConstants.TRAVEL_ID, -1);

        //Stores all the receipt types
        String[] typesFromStrings = getResources().getStringArray(R.array.receipt_type_categories);
        types = Arrays.asList(typesFromStrings);

        // Checks if the receipt is being edited or is a single reimbursement
        beingEdited = getIntent().getBooleanExtra(IntentConstants.EDIT_FLAG, false);
        reimbursement = getIntent().getBooleanExtra(IntentConstants.REIMBURSEMENT_FLAG, false);

        // Resets the toast flag to allow toasts to be shown
        showToast = true;

        // Reimbursement-specific
        if (reimbursement) {
            // Changes create button text and icon to send
            button.setText(R.string.button_send);
            GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_send);
        }

        // If being edited then the representatives list is set from found receipt
        if (beingEdited) {
            representatives = db.getReceipt(id).getRepresentatives();
            path = db.getReceipt(id).getImagePath();
        } else {
            representatives = new ArrayList<>();
        }

        // Sets the adapter to the type Spinner
        CursorAdapter typeAdapter = new CursorAdapter(this, types);
        type.setAdapter(typeAdapter);

        // Adjusts activity according to type chosen
        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView,
                                       int position, long id) {
                showToast = true;
                //If chosen Meals (Representation)
                if (position == ArrayPositionUtil.MEAL_REPRESENTATION_INDEX) {
                    //Display the list of guests
                    showRepresentation();
                    hideStayFields();
                } else if (position == ArrayPositionUtil.HOTEL_INDEX) {
                    showStayFields();
                    hideRepresentation();
                } else {
                    hideStayFields();
                    hideRepresentation();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}

        });

        // Sets listeners to children of the representatives view
        representativesView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                        int childPosition, long id) {
                ExpandableListAdapter adapter =
                        (ExpandableListAdapter) representativesView.getExpandableListAdapter();

                String representative = adapter.getChild(groupPosition, childPosition).toString();

                Intent resultIntent = new Intent(ReceiptActivity.this,
                        PersonActivity.class);

                resultIntent.putExtra(IntentConstants.PARENT_NAME, IntentConstants.PARENT_RECEIPT);
                resultIntent.putExtra(IntentConstants.ITEM_NAME, representative);
                resultIntent.putExtra(IntentConstants.EDIT_FLAG, true);
                startActivityForResult(resultIntent, IntentConstants.REQUEST_NEW_ITEM);
                return false;
            }
        });

        if (!beingEdited) {
            // Starts the camera and send IDs for naming the image file
            Intent cameraIntent = new Intent(this, CameraActivity.class);
            cameraIntent.putExtra(IntentConstants.TRAVEL_ID, fk);
            cameraIntent.putExtra(IntentConstants.ITEM_ID,
                    db.getNextReceiptId(fk));
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }

        // Sets updated adapter for representative view and formats its height
        representativesView.setAdapter(createRepresentativesAdapter());
        ExpandableListUtil.setListHeight(Collections.singletonList(representativesView));

        updateInformation();

        inputStringsAtStart = getInputStrings();
    }

    @Override
    public void onBackPressed() {
        if (id == -1 && !reimbursement) {
            // Customized dialog for handling receipts
            final Dialog dialog = new Dialog(this);

            // Creates the negative button listener
            View.OnClickListener negativeListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(PopupConstants.DIALOG_INFORMATION_LOSS,
                            "User denied loss of information.");
                    v.setClickable(true);
                    dialog.dismiss();
                }
            };

            // Creates the positive button listener
            View.OnClickListener positiveListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(PopupConstants.DIALOG_INFORMATION_LOSS,
                            "User confirmed loss of information.");
                    v.setClickable(true);
                    boolean deleted = CameraUtils.deleteImageFromStorage(path);
                    if (!deleted) {
                        Log.e(TAG, "Deletion of receipt image failed.");
                    } else {
                        Log.i(TAG, "Successfully deleted receipt image.");
                    }
                    finish();
                    dialog.dismiss();
                }
            };

            // Customizes the created dialog
            DialogUtil.createAlertDialog(
                    this, dialog, getString(R.string.dialog_information_loss_message),
                    getString(R.string.dialog_information_loss_negative_button),
                    getString(R.string.dialog_information_loss_positive_button),
                    negativeListener, positiveListener)
                    .show();
        } else if (reimbursement) {
            // Customized dialog for handling reimbursements
            final Dialog dialog = new Dialog(this);

            // Creates the negative button listener
            View.OnClickListener negativeListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(PopupConstants.DIALOG_INFORMATION_LOSS,
                            "User denied loss of information.");
                    v.setClickable(true);
                    dialog.dismiss();
                }
            };

            // Creates the positive button listener
            View.OnClickListener positiveListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(PopupConstants.DIALOG_INFORMATION_LOSS,
                            "User confirmed loss of information.");
                    v.setClickable(true);
                    db.deleteTravel(DatabaseHandler.REIMBURSEMENT_ID);
                    boolean deleted = CameraUtils.deleteImageFromStorage(path);
                    if (!deleted) {
                        Log.e(TAG, "Deletion of receipt image failed.");
                    } else {
                        Log.i(TAG, "Successfully deleted receipt image.");
                    }
                    finish();
                    dialog.dismiss();
                }
            };

            // Customizes the created dialog
            DialogUtil.createAlertDialog(
                    this, dialog, getString(R.string.dialog_information_loss_message),
                    getString(R.string.dialog_information_loss_negative_button),
                    getString(R.string.dialog_information_loss_positive_button),
                    negativeListener, positiveListener)
                    .show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected String getItemName() {
        return "receipt";
    }

    @Override
    protected List<FontEdit> createEditFieldList() {
        List<FontEdit> eFields = Arrays.asList(name, total, currency);

        // Create new list to remove fixed size
        List<FontEdit> editFields = new ArrayList<>();
        editFields.addAll(eFields);

        if (ArrayPositionUtil.hasHotelPosition(this, type.getSelectedItem().toString())) {
            editFields.add(stayName);
            editFields.add(stayAddress);
            editFields.add(stayDuration);
        }
        if (ArrayPositionUtil.hasMealRepresentationPosition(this,
                type.getSelectedItem().toString())) {
            editFields.add(representationPurpose);
        }
        return editFields;
    }

    @Override
    protected List<String> getInputStrings() {
        List<String> tempStrings = Arrays.asList(
                type.getSelectedItem().toString(),
                name.getText().toString(),
                DateParserUtil.getDateString(date),
                total.getText().toString(),
                currency.getText().toString());

        // Create new list to remove fixed size
        List<String> inputStrings = new ArrayList<>();
        inputStrings.addAll(tempStrings);

        if(ArrayPositionUtil.hasHotelPosition(this, type.getSelectedItem().toString())) {
            inputStrings.add(stayName.getText().toString());
            inputStrings.add(stayAddress.getText().toString());
            inputStrings.add(DateParserUtil.getDateString(stayDate));
            inputStrings.add(stayDuration.getText().toString());
            inputStrings.add(stayType.getSelectedItem().toString());
        }
        if(ArrayPositionUtil.hasMealRepresentationPosition(this,
                type.getSelectedItem().toString())) {
            inputStrings.add(representationPurpose.getText().toString());
            for (String rep : representatives) {
                inputStrings.add(rep);
            }
        }
        return inputStrings;
    }

    /**
     * Ensures that information is up to date on editing.
     */
    private void updateInformation() {
        if (beingEdited) {
            if (id != -1) {
                // Finds the receipt with the given ID
                Receipt receipt = db.getReceipt(id);

                // Sets new titles for editing
                FontView header = (FontView) findViewById(R.id.header);
                header.setText(R.string.edit_receipt_header);
                setTitle(R.string.title_activity_edit_receipt);

                // Sets new information based on found receipt
                type.setSelection(ArrayPositionUtil.getTypeArrayIndex(
                        this, receipt.getType(), R.array.receipt_type_categories), true);
                name.setText(receipt.getName());
                DateParserUtil.setDate(date, receipt.getDate());
                total.setText(String.format("%.2f", receipt.getTotal()));
                currency.setText(receipt.getCurrency());

                // Changes create button text and icon to save
                button.setText(R.string.button_save);
                GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_save);

                // Makes delete button visible
                FontButton delete = (FontButton) findViewById(R.id.delete_receipt_button);
                delete.setVisibility(FontButton.VISIBLE);

                // Updates stay information fields if the receipt is of hotel type.
                if(ArrayPositionUtil.hasHotelPosition(this, receipt.getType())) {
                    Stay stay = receipt.getStay();

                    // Sets new information based on found receipt
                    stayName.setText(stay.getName());
                    stayAddress.setText(stay.getAddress());
                    DateParserUtil.setDate(stayDate, stay.getDate());
                    stayDuration.setText(String.valueOf(stay.getDuration()));
                    stayType.setSelection(ArrayPositionUtil.getTypeArrayIndex(this, stay.getType(),
                            R.array.receipt_stay_type_list), true);
                }

                // Updates representatives if the receipt is of meal representation type.
                if(ArrayPositionUtil.hasMealRepresentationPosition(
                        this, type.getSelectedItem().toString())) {
                    representationPurpose.setText(receipt.getRepresentationPurpose());
                }

            } else {
                Log.e(TAG, DebugConstants.ERROR_NO_RECEIPT);
            }
        }
    }

    /**
     * Updates visibility of representatives view based on whether or not it contains data.
     */
    private void updateRepViewVisibility() {
        if(representatives.size() > 0) {
            representativesView.setVisibility(View.VISIBLE);
            representativesView.collapseGroup(0);
            representativesView.setAdapter(createRepresentativesAdapter());
            ExpandableListUtil.setListHeight(Collections.singletonList(representativesView));
        } else {
            representativesView.setVisibility(View.GONE);
        }
    }

    /**
     * Creates and returns an adapter for the representatives view.
     * @return An expandable list adapter formatted for the representatives view.
     */
    private ExpandableListAdapter createRepresentativesAdapter() {
        HashMap<String, List<String>> childList = new HashMap<>();

        List<String> headerList = new ArrayList<>();
        headerList.add(getResources().getString(R.string.receipt_representatives_label));

        childList.put(headerList.get(0), representatives);

        return new ExpandableListAdapter(this, headerList, childList);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Representation result
        if (resultCode == IntentConstants.RESULT_DELETE) {
            representatives.remove(data.getStringExtra(IntentConstants.ITEM_ID));
        }
        if(resultCode == IntentConstants.RESULT_CREATE) {
            representatives.add(data.getStringExtra(IntentConstants.ITEM_NAME));
        }
        if(resultCode == IntentConstants.RESULT_EDIT) {
            representatives.remove(data.getStringExtra(IntentConstants.ITEM_ID));
            representatives.add(data.getStringExtra(IntentConstants.ITEM_NAME));
        }
        if(requestCode == IntentConstants.REQUEST_NEW_ITEM) {
            updateRepViewVisibility();
        }

        // Camera result
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            path = data.getStringExtra(IntentConstants.IMAGE_PATH);

            // Tells the user to find receipt image by swiping (and places toast at top of screen)
            Toast info =
                    ToastUtil.createInfoToast(this, getString(R.string.toast_info_camera_swipe));
            info.setGravity(Gravity.TOP, 0, 140);
            info.show();
            }

            /*
             //Generates ocr text
             recognizedText = getTextFromImage();
             Log.i(TAG, recognizedText);
             */


        // Finishes this activity if no image was taken
        if(requestCode == CAMERA_REQUEST && resultCode == RESULT_CANCELED) {
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.new_receipt_button:
                try {
                    if (!hasUnfilledFields()) {
                        showToast = false;
                        if (fk != -1) {
                            int receiptId = id;
                            if (receiptId == -1) {
                                receiptId = db.getNextReceiptId(fk);
                            }
                            // Enforces "." separation
                            String tot = String.valueOf(total.getText().toString());
                            if (tot.contains(",")) {
                                String[] split = tot.split(",");
                                tot = split[0] + "." + split[1];
                            }

                            // Create a dummy travel if reimbursement
                            if(reimbursement) {
                                // Enforces unique primary ID by deleting any existing dummy travel
                                db.deleteTravel(DatabaseHandler.REIMBURSEMENT_ID);

                                // Creates the reimbursement dummy travel in the database
                                Travel reimbursementTravel = new Travel(DatabaseHandler.REIMBURSEMENT_ID,
                                        db.getAllUsers().get(0).getSsn(),
                                        name.getText().toString(),
                                        getResources().getString(R.string.parent_reimbursement_purpose),
                                        "", "");
                                reimbursementTravel.setId(DatabaseHandler.REIMBURSEMENT_ID);
                                db.insertTravel(reimbursementTravel);
                            }

                            Receipt newReceipt = new Receipt(receiptId, fk,
                                    type.getSelectedItem().toString(),
                                    name.getText().toString(),
                                    DateParserUtil.getDateString(date),
                                    Double.parseDouble(tot),
                                    currency.getText().toString(),
                                    path);

                            if (ArrayPositionUtil.hasHotelPosition(this, newReceipt.getType())) {
                                Stay newStay = new Stay(receiptId,
                                        stayName.getText().toString(),
                                        stayAddress.getText().toString(),
                                        DateParserUtil.getDateString(stayDate),
                                        Integer.parseInt(stayDuration.getText().toString()),
                                        stayType.getSelectedItem().toString());
                                newReceipt.setStay(newStay);
                            }
                            if (ArrayPositionUtil.
                                    hasMealRepresentationPosition(this, newReceipt.getType())) {
                                newReceipt.setRepresentationPurpose(
                                        representationPurpose.getText().toString());
                                newReceipt.setRepresentatives(representatives);
                            }

                            if (!beingEdited) {
                                db.insertReceipt(newReceipt);
                            } else {
                                if (id != -1) {
                                    newReceipt.setId(id);
                                    db.updateReceipt(newReceipt);
                                } else {
                                    Log.e(TAG, DebugConstants.ERROR_NO_RECEIPT);
                                }
                            }
                        } else {
                            Log.e(TAG, DebugConstants.ERROR_NO_TRAVEL);
                        }

                        // With reimbursement flag the receipt is sent directly to the send activity
                        if (reimbursement) {
                            Intent sendIntent = new Intent(this, SendActivity.class);
                            sendIntent.putExtra(IntentConstants.REIMBURSEMENT_FLAG, true);
                            startActivity(sendIntent);
                        }

                        finish();
                    } else {
                        if (showToast){
                            showUnfilledFieldsToast();
                            showToast = false;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.delete_receipt_button:
                if(id != -1) {
                    final Dialog dialog = new Dialog(this);
                    // Creates the positive button listener
                    View.OnClickListener positiveListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.i(PopupConstants.DIALOG_DELETION,
                                    "User confirmed " + getItemName() + " deletion.");
                            v.setClickable(true);
                            db.deleteReceipt(id);
                            finish();
                            dialog.dismiss();
                        }
                    };
                    showDeleteConfirmDialog(dialog, positiveListener);
                } else {
                    Log.e(TAG, DebugConstants.ERROR_NO_RECEIPT);
                }
                break;
            case R.id.receipt_representatives_button:
                Intent resultIntent = new Intent(ReceiptActivity.this,
                        PersonActivity.class);
                resultIntent.putExtra(IntentConstants.PARENT_NAME, IntentConstants.PARENT_RECEIPT);
                startActivityForResult(resultIntent, IntentConstants.REQUEST_NEW_ITEM);
        }
    }

    /**
     * Sets all widgets related to representation meals to be visible.
     */
    private void showRepresentation() {
        // Representation
        FontView representationPurposeLabel = (FontView)
                findViewById(R.id.receipt_representation_purpose_label);
        representationPurposeLabel.setVisibility(View.VISIBLE);
        representationPurpose.setVisibility(View.VISIBLE);

        FontView label = (FontView) findViewById(R.id.receipt_representatives_label);
        label.setVisibility(FontView.VISIBLE);
        FontButton button = (FontButton) findViewById(R.id.receipt_representatives_button);
        button.setVisibility(FontButton.VISIBLE);
        updateRepViewVisibility();
    }

    /**
     * Sets all widgets related to hotel stays to be visible.
     */
    private void showStayFields() {
        FontView label = (FontView) findViewById(R.id.receipt_stay_name_label);
        label.setVisibility(View.VISIBLE);
        stayName.setVisibility(View.VISIBLE);
        label = (FontView) findViewById(R.id.receipt_stay_address_label);
        label.setVisibility(View.VISIBLE);
        stayAddress.setVisibility(View.VISIBLE);
        label = (FontView) findViewById(R.id.receipt_stay_date_label);
        label.setVisibility(View.VISIBLE);
        stayDate.setVisibility(View.VISIBLE);
        label = (FontView) findViewById(R.id.receipt_stay_duration_label);
        label.setVisibility(View.VISIBLE);
        stayDuration.setVisibility(View.VISIBLE);
        label = (FontView) findViewById(R.id.receipt_stay_type_label);
        label.setVisibility(View.VISIBLE);
        stayType.setVisibility(View.VISIBLE);
    }

    /**
     * Sets all widgets related to representation meals to be GONE.
     */
    private void hideRepresentation() {
        representativesView.setVisibility(ExpandableListView.GONE);
        FontView label = (FontView) findViewById(R.id.receipt_representatives_label);
        label.setVisibility(FontView.GONE);
        FontButton button = (FontButton) findViewById(R.id.receipt_representatives_button);
        button.setVisibility(FontButton.GONE);
        FontView representationPurposeLabel = (FontView)
                findViewById(R.id.receipt_representation_purpose_label);
        representationPurposeLabel.setVisibility(View.GONE);
        representationPurpose.setVisibility(View.GONE);
    }

    /**
     * Sets all widgets related to hotel stays to be GONE.
     */
    private void hideStayFields() {
        FontView label = (FontView) findViewById(R.id.receipt_stay_name_label);
        label.setVisibility(View.GONE);
        stayName.setVisibility(View.GONE);
        label = (FontView) findViewById(R.id.receipt_stay_address_label);
        label.setVisibility(View.GONE);
        stayAddress.setVisibility(View.GONE);
        label = (FontView) findViewById(R.id.receipt_stay_date_label);
        label.setVisibility(View.GONE);
        stayDate.setVisibility(View.GONE);
        label = (FontView) findViewById(R.id.receipt_stay_duration_label);
        label.setVisibility(View.GONE);
        stayDuration.setVisibility(View.GONE);
        label = (FontView) findViewById(R.id.receipt_stay_type_label);
        label.setVisibility(View.GONE);
        stayType.setVisibility(View.GONE);
    }

    /**
     * Gets the full text read by tesseract
     *
     * @return the text string
     */
    private String getTextFromImage(){
        //Preparing tessdata directory
        String[] paths = new String[] { DATA_PATH, DATA_PATH + "tessdata/" };
        for (String path : paths) {
            File dir = new File(path);
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    Log.v(TAG, "ERROR: Creation of directory " + path + " on sdcard failed");
                    return "Image not found";
                } else {
                    Log.v(TAG, "Created directory " + path + " on sdcard");
                }
            }
        }

        // lang.traineddata file with the app (in assets folder)
        // You can get them at:
        // http://code.google.com/p/tesseract-ocr/downloads/list
        // This area needs work and optimization
        if (!(new File(DATA_PATH + "tessdata/" + lang + ".traineddata")).exists()) {
            try {

                AssetManager assetManager = getAssets();
                InputStream in = assetManager.open("tessdata/" + lang + ".traineddata");
                OutputStream out = new FileOutputStream(DATA_PATH
                        + "tessdata/" + lang + ".traineddata");

                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();

                Log.v(TAG, "Copied " + lang + " traineddata");
            } catch (IOException e) {
                Log.e(TAG, "Was unable to copy " + lang + " traineddata " + e.toString());
            }
        }

        Bitmap bitmap = CameraUtils.loadImageFromStorage(this, path);

        try {
            ExifInterface exif = new ExifInterface(path);
            int exifOrientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            Log.v(TAG, "Orient: " + exifOrientation);

            int rotate = 0;

            switch (exifOrientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
            }

            Log.v(TAG, "Rotation: " + rotate);

            if (rotate != 0) {

                // Getting width & height of the given image.
                int w = bitmap.getWidth();
                int h = bitmap.getHeight();

                // Setting pre rotate
                Matrix mtx = new Matrix();
                mtx.preRotate(rotate);

                // Rotating Bitmap
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, false);
            }

        } catch (IOException e) {
            Log.e(TAG, "Couldn't correct orientation: " + e.toString());
        }

        TessBaseAPI baseApi = new TessBaseAPI();
        baseApi.setDebug(true);
        baseApi.init(DATA_PATH, lang);
        baseApi.setImage(bitmap);

        return baseApi.getUTF8Text();
    }
}
