package no.uib.jmi053.tefapplication.utilities.popups;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.widgets.FontButton;
import no.uib.jmi053.tefapplication.widgets.FontView;

/**
 * Created by JoaT Development on 06.10.2015.
 *
 * Utility class for creating cusotm alert dialogs.
 */
public abstract class DialogUtil {

    /**
     * Modifies a standard dialog into a custom alert dialog with given message in button listeners.
     * @param activity The activity that created the dialog.
     * @param dialog The standard dialog to modify.
     * @param messageString The message string to be provided by the dialog.
     * @param negativeText The text to put on the negative button.
     * @param positiveText The text to put on the positive button.
     * @param negativeListener The listener to be set to the negative button.
     * @param positiveListener The listener to be set to the positive button.
     * @return The customized alert dialog.
     */
    public static Dialog createAlertDialog(final Activity activity, final Dialog dialog,
                                           String messageString,
                                           String negativeText,
                                           String positiveText,
                                           View.OnClickListener negativeListener,
                                           View.OnClickListener positiveListener) {

        // Removes the background and title to allow for rounded edges
        Window win = dialog.getWindow();
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        win.requestFeature(Window.FEATURE_NO_TITLE);

        // Finds and sets the layout for the dialog
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.dialog_alert,
                (ViewGroup) activity.findViewById(R.id.dialog_alert_layout));
        dialog.setContentView(layout);

        // Finds and set the dialog alert message
        FontView message = (FontView) layout.findViewById(R.id.dialog_alert_message);
        message.setText(messageString);

        // Finds and sets on click listener to the negative button
        FontButton negativeButton = (FontButton)
                layout.findViewById(R.id.dialog_alert_negative_button);
        negativeButton.setOnClickListener(negativeListener);
        negativeButton.setText(negativeText);

        // Finds and sets on click listener to the positive button
        FontButton positiveButton = (FontButton)
                layout.findViewById(R.id.dialog_alert_positive_button);
        positiveButton.setOnClickListener(positiveListener);
        positiveButton.setText(positiveText);

        return dialog;
    }
}
