package no.uib.jmi053.tefapplication.utilities.parsers;

import android.widget.DatePicker;

/**
 * Created by JoaT Development on 04/09/2015.
 *
 * Contains functionality for parsing dates from DatePickers.
 */
public abstract class DateParserUtil {

    /**
     * Parses the day of month from a string of format "dd.mm.yyyy".
     * @param date The date string to parse from.
     * @return An integer representing the parsed day of month.
     */
    private static int parseDayOfMonth(String date) {
        String[] split = date.split("\\.");
        return Integer.parseInt(split[0]);
    }

    /**
     * Parses the month of year from a string of format "dd.mm.yyyy".
     * @param date The date string to parse from.
     * @return An integer representing the parsed month of year.
     */
    private static int parseMonth(String date) {
        String[] split = date.split("\\.");
        return Integer.parseInt(split[1]);
    }

    /**
     * Parses the year from a string of format "dd.mm.yyyy".
     * @param date The date string to parse from.
     * @return An integer representing the parsed year.
     */
    private static int parseYear(String date) {
        String[] split = date.split("\\.");
        return Integer.parseInt(split[2]);
    }

    /**
     * Creates a formatted date string from a date picker.
     * @param date The date picker to get date from.
     * @return A string representing the given date pickers date of format "dd.mm.yyyy".
     */
    public static String getDateString(DatePicker date) {
        String dateString = String.format("%02d", date.getDayOfMonth());
        dateString += "." + String.format("%02d", (date.getMonth()));
        dateString += "." + date.getYear();
        return dateString;
    }

    /**
     * Sets a given date to a date picker.
     * @param date The date picker to set time for.
     * @param dateString The date string to parse information from.
     */
    public static void setDate(DatePicker date, String dateString) {
        date.updateDate(DateParserUtil.parseYear(dateString), DateParserUtil.parseMonth(dateString),
                DateParserUtil.parseDayOfMonth(dateString));
    }

    /**
     * Formats a date after being retrieved from database.
     * @param dateString The string to be formatted.
     * @return The formatted string of format dd.mm.yyyy.
     */
    public static String formatDate(String dateString) {
        if(dateString != null && !dateString.equals("")) {
            int day = DateParserUtil.parseDayOfMonth(dateString);
            int month = DateParserUtil.parseMonth(dateString);
            int year = DateParserUtil.parseYear(dateString);
            month++;
            return String.format("%02d", day) + "." + String.format("%02d", month) + "." + year;
        } else {
            return "";
        }
    }
}
