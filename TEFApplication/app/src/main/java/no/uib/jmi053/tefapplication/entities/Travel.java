package no.uib.jmi053.tefapplication.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JoaT Development on 27/08/2015.
 *
 * Holds information about a travel.
 */
public class Travel {

    private int id;
    private long fk;
    private String route;
    private String purpose;
    private String startDate;
    private String startTime;
    private String customerName;
    private int customerID;
    private double prepaidAmount;
    private List<String> events;
    private List<String> comments;
    
    /**
     * Creates a new Travel from an ID, connected to a user by a foreign key.
     * @param id The ID generated at database insertion.
     * @param fk The ID of the connected user.
     * @param route The route description of the travel.
     * @param purpose The purpose description of the travel.
     * @param startDate The start date of the travel.
     * @param startTime The start time of the travel.
     */
    public Travel(int id, long fk, String route, String purpose, String startDate,
                  String startTime) {
        this.id = id;
        this.fk = fk;
        this.route = route;
        this.purpose = purpose;
        this.startDate = startDate;
        this.startTime = startTime;
        customerName = "null";
        customerID = 0;
        prepaidAmount = 0;
        events = new ArrayList<>();
        comments = new ArrayList<>();
    }

    /**
     * Returns the travel ID.
     * @return An integer representing the ID automatically generated at input.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets a new ID for a travel. Called when a travel is given an ID by the external database.
     * @param id The new ID to be set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the travel foreign key.
     * @return An integer representing the ID of the connected user.
     */
    public long getFk() {
        return fk;
    }

    /**
     * Sets a new foreign key for a travel.
     * @param fk The new foreign key to be set.
     */
    public void setFk(long fk) {
        this.fk = fk;
    }

    /**
     * Returns the travel route description.
     * @return A string representation of the travel route.
     */
    public String getRoute() {
        return route;
    }

    /**
     * Sets a new route description for a travel.
     * @param route The new route description to be set.
     */
    public void setRoute(String route) {
        this.route = route;
    }

    /**
     * Returns the travel purpose description.
     * @return A string representation of the purpose of a travel.
     */
    public String getPurpose() {
        return purpose;
    }

    /**
     * Sets a new purpose description for a travel.
     * @param purpose The new purpose description to be set.
     */
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    /**
     * Returns the start date of a travel.
     * @return A string representation of the travel start date of format "dd.mm.yyyy".
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets a new start date for a travel.
     * @param date The new date to be set of format "dd.mm.yyyy".
     */
    public void setStartDate(String date) {
        this.startDate = date;
    }

    /**
     * Returns the start time of a travel.
     * @return A string representation of the travel start time of format "hh:mm".
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * Sets a new start time for a travel.
     * @param time The new time to be set of format "hh:mm".
     */
    public void setStartTime(String time) {
        this.startTime = time;
    }

    /**
     * Returns the customer ID for a travel.
     * @return An integer representation of the customer ID of a travel.
     */
    public int getCustomerId() {
        return customerID;
    }

    /**
     * Sets a new customer ID for a travel.
     * @param customerID The new ID to be set.
     */
    public void setCustomerId(int customerID) {
        this.customerID = customerID;
    }

    /**
     * Checks if the travel is to be paid by a customer.
     * @return True if customer name does not equal empty string, false otherwise.
     */
    public boolean isPaidByCustomer() {
        return !customerName.equals("null");
    }

    /**
     * Returns the customer name for a travel.
     * @return A string representation of the customer name of a travel.
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Sets a new customer name for a travel.
     * @param customerName The new name to be set.
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * Checks if travel has some prepaid amount.
     * @return True if the prepaid amount does not equal 0, false otherwise.
     */
    public boolean hasPrepaidAmount() {
        return prepaidAmount != 0;
    }

    /**
     * Returns the prepaid amount for a travel.
     * @return A double representing the prepaid amount of a travel.
     */
    public double getPrepaidAmount() {
        return prepaidAmount;
    }

    /**
     * Sets the prepaid amount for a travel.
     * @param prepaidAmount The new prepaid amount to be set.
     */
    public void setPrepaidAmount(double prepaidAmount) {
        this.prepaidAmount = prepaidAmount;
    }

    /**
     * Returns the eventss for a travel.
     * @return An array list containing all events about a travel.
     */
    public List<String> getEvents() {
        return events;
    }

    /**
     * Sets a new event list for a travel.
     * @param events The new events list to be set.
     */
    public void setEvents(List<String> events) {
        this.events = events;
    }

    /**
     * Returns the comments for a travel.
     * @return An array list containing all comments about a travel.
     */
    public List<String> getComments() {
        return comments;
    }

    /**
     * Sets a new comments list for a travel.
     * @param comments The new comments list to be set.
     */
    public void setComments(List<String> comments) {
        this.comments = comments;
    }

}