package no.uib.jmi053.tefapplication.entities;

/**
 * Created by JoaT Development on 31.08.2015.
 *
 * Holds information about a stay.
 */
public class Stay {

    private int fk;
    private String name;
    private String address;
    private String date;
    private int duration;
    private String type;

    /**
     * Creates a new Stay with the ID of the connected receipt.
     * @param name The name of the place of stay.
     * @param address The address of the place of stay.
     * @param date The starting date of the stay.
     * @param duration The duration of the stay.
     * @param type The quality of place of stay.
     */
    public Stay(int fk, String name, String address, String date, int duration, String type) {
        this.fk = fk;
        this.name = name;
        this.address = address;
        this.date = date;
        this.duration = duration;
        this.type = type;
    }

    /**
     * Returns the stay foreign key.
     * @return An integer representing the ID of the connected travel.
     */
    public int getFk() {
        return fk;
    }

    /**
     * Sets a new foreign key for a stay. Called when a travel is given an ID by the external
     * database.
     * @param fk The new foreign key to be set.
     */
    public void setFk(int fk) {
        this.fk = fk;
    }

    /**
     * Returns the place of stay name.
     * @return A string representing the name of the place of stay.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets a new name for a place of stay.
     * @param name The new name to be set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the address of the place of stay.
     * @return A string representing the address of the place of stay.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets a new address for a place of stay.
     * @param address The new address to be set.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Returns the starting date of the stay.
     * @return A string representing the stay stat date of format "dd.mm.yyyy".
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets a new start date for a stay.
     * @param date The new date to be set of format "dd.mm.yyyy".
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Returns the duration of stay.
     * @return An integer representing the number of nights stayed at place of stay.
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Sets a new duration for a stay.
     * @param duration The new duration to be set.
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Returns the type of the stay.
     * @return A string representing the quality of the place of stay.
     */
    public String getType() {
        return type;
    }

    /**
     * Sets a new type for a stay.
     * @param type The new type to be set.
     */
    public void setType(String type) {
        this.type = type;
    }

}
