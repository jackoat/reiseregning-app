package no.uib.jmi053.tefapplication.utilities.graphics;

import android.graphics.Bitmap;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by JoaT Development on 04.10.2015.
 *
 * Contains functionality for cropping and transforming images of receipts.
 */
public abstract class CVUtil {

    /**
     * Crops and transforms the largest contour found in an image.
     * @param bitmap The image to crop and transform.
     */
    public static void cropAndTransform(Bitmap bitmap) {
        double ratio = bitmap.getHeight() / 500.0;
        int ratioWidth = (int) (bitmap.getWidth() * ratio);
        Bitmap resized = Bitmap.createScaledBitmap(bitmap, ratioWidth, 500, false);

        Mat mat = new Mat();
        Mat original = new Mat();
        Utils.bitmapToMat(resized, mat);
        Utils.bitmapToMat(bitmap, original);

        findEdges(mat);
        List<Point> contours = findContours(mat);
        for(Point c : contours) {
            c.x *= ratio;
            c.y *= ratio;
        }
        warpTransform(original, contours);
        Utils.matToBitmap(original, bitmap);
    }

    /**
     * Finds edges in an image.
     * @param mat The image to find edges in.
     */
    private static void findEdges(Mat mat) {
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(mat, mat, new Size(5, 5), 0);
        Imgproc.Canny(mat, mat, 75, 200);
    }

    /**
     * Finds contours in the edged image.
     * @param mat The edged image to find contours from.
     * @return A list of four points containing the corners of the contours.
     */
    private static List<Point> findContours(Mat mat) {
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(mat, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        List<Point> points = new ArrayList<>();

        for(MatOfPoint c : contours) {
            MatOfPoint2f con = new MatOfPoint2f(c.toArray());
            double peri = Imgproc.arcLength(con, true);
            Imgproc.approxPolyDP(con, con, 0.02 * peri, true);

            // If the contour has 4 points it is likely the receipt edge corners
            if(con.toList().size() == 4) {
                points = con.toList();
                break;
            }
        }

        return points;
    }

    /**
     * Warps the image to hold only the contoured rectangle found.
     * @param original The original image.
     * @param contours The found contours.
     */
    private static void warpTransform(Mat original, List<Point> contours) {
        fourPointTransform(original, contours);
        Imgproc.cvtColor(original, original, Imgproc.COLOR_BGR2GRAY);
        Imgproc.adaptiveThreshold(original, original, 250, Imgproc.ADAPTIVE_THRESH_MEAN_C,
                Imgproc.THRESH_BINARY_INV, 5, 4);
    }

    /**
     * Uses a list of four points to transform an image.
     * @param original The original image to apply transform to.
     * @param contours The four points holding the edges of the contours found in the image.
     */
    private static void fourPointTransform(Mat original, List<Point> contours) {
        List<Point> orderedPoints = orderPoints(contours);

        Point tl = orderedPoints.get(0);
        Point tr = orderedPoints.get(1);
        Point br = orderedPoints.get(2);
        Point bl = orderedPoints.get(3);

        // Computes max width
        double widthA = Math.sqrt((Math.pow((br.x - bl.x), 2)) + (Math.pow((br.y - bl.y), 2)));
        double widthB = Math.sqrt((Math.pow((tr.x - tl.x), 2)) + (Math.pow((tr.y - tl.y), 2)));
        double maxWidth;
        if(widthA > widthB) {
            maxWidth = widthA;
        } else {
            maxWidth = widthB;
        }

        // Computes max height
        double heightA = Math.sqrt((Math.pow((tr.x - br.x), 2)) + (Math.pow((tr.y - br.y), 2)));
        double heightB = Math.sqrt((Math.pow((tl.x - bl.x), 2)) + (Math.pow((tl.y - bl.y), 2)));
        double maxHeight;
        if(heightA > heightB) {
            maxHeight = heightA;
        } else {
            maxHeight = heightB;
        }

        // Constructs the destination set
        List<Point> roi = Arrays.asList(
                new Point(0,0),
                new Point(maxWidth -1, 0),
                new Point(maxWidth - 1, maxHeight - 1),
                new Point(0, maxHeight -1));
        Mat dest = Converters.vector_Point2f_to_Mat(roi);

        Mat per = Imgproc.getPerspectiveTransform(original, dest);
        Imgproc.warpPerspective(original, original, per, new Size(maxWidth, maxHeight));
    }

    /**
     * Orders points of a contour such that order is top left, top right, bottom right, bottom left.
     * @param contours The contour points to order.
     * @return An ordered list of contour points.
     */
    private static List<Point> orderPoints(List<Point> contours) {
        // Sums all points and also stores a point references in a map
        List<Double> sums = new ArrayList<>();
        Map<Double, Point> sizePointMap = new HashMap<>();
        for(int i = 0; i < contours.size(); i++) {
            sums.add(contours.get(i).x + contours.get(i).y);
            sizePointMap.put(sums.get(i), contours.get(i));
        }
        Collections.sort(sums);

        // The smallest and biggest sums are set to be top left and bottom right
        Point tl = sizePointMap.get(sums.get(0));
        Point tr;
        Point br = sizePointMap.get(sums.get(3));
        Point bl;

        // The middle sums are instead subtracted and smallest is top right and biggest bottom left
        Point pointX = sizePointMap.get(sums.get(1));
        Point pointY = sizePointMap.get(sums.get(2));

        double diffOne = pointX.x - pointX.y;
        double diffTwo = pointY.x - pointY.y;

        if(diffOne < diffTwo) {
            tr = sizePointMap.get(sums.get(1));
            bl = sizePointMap.get(sums.get(2));
        } else {
            tr = sizePointMap.get(sums.get(2));
            bl = sizePointMap.get(sums.get(1));
        }

        return Arrays.asList(tl, tr, br, bl);
    }
}
