package no.uib.jmi053.tefapplication.activities.superclass;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.settings.SettingsActivity;
import no.uib.jmi053.tefapplication.io.DatabaseHandler;
import no.uib.jmi053.tefapplication.utilities.constants.DebugConstants;
import no.uib.jmi053.tefapplication.utilities.graphics.AnimUtil;

/**
 * Created by JoaT Development on 10/09/2015.
 *
 * The superclass of all activities of substance in TEFApplication.
 */
public abstract class TEFActivity extends AppCompatActivity {

    protected String TAG;

    protected DatabaseHandler db;

    private int onStartCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = DebugConstants.getErrorTag(getClass());
        db = DatabaseHandler.getInstance(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // New activities enters from left to right.
        onStartCount = 1;
        if (savedInstanceState == null) {
            AnimUtil.overrideForwardTransition(this);
        } else {
            onStartCount = 2;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_settings:
                onSettingsPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Sends a user to the settings activity when settings is chosen in options menu.
     */
    public void onSettingsPressed(){
        startActivity(new Intent(this, SettingsActivity.class));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Ensures animation is reversed when going backwards.
        if (onStartCount > 1) {
            AnimUtil.overrideBackTransition(this);
        }
        if (onStartCount == 1) {
            onStartCount++;
        }
    }
}
