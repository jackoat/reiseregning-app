package no.uib.jmi053.tefapplication.io;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import no.uib.jmi053.tefapplication.entities.CarTravel;
import no.uib.jmi053.tefapplication.entities.Receipt;
import no.uib.jmi053.tefapplication.entities.Stay;
import no.uib.jmi053.tefapplication.entities.Travel;
import no.uib.jmi053.tefapplication.entities.User;
import no.uib.jmi053.tefapplication.utilities.ArrayPositionUtil;
import no.uib.jmi053.tefapplication.utilities.CameraUtils;
import no.uib.jmi053.tefapplication.utilities.MapUtil;

/**
 * Created by JoaT Development on 27/08/2015.
 *
 * Handles and runs all database queries.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // Error log constant
    private final String TAG = getClass().getName() + " in table ";

    // Database constants
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "travel_manager";
    public static final String TABLE_USERS = "user";
    public static final String TABLE_USER_ACCOUNTS = "user_account";
    public static final String TABLE_TRAVELS = "travel";
    public static final String TABLE_TRAVEL_EVENTS = "travel_event";
    public static final String TABLE_TRAVEL_COMMENTS = "travel_comment";
    public static final String TABLE_RECEIPTS = "receipt";
    public static final String TABLE_MEAL_REPRESENTATIONS = "meal_representation";
    public static final String TABLE_CAR_TRAVELS = "car_travel";
    public static final String TABLE_CAR_PASSENGERS = "car_passenger";
    public static final String TABLE_STAYS = "stay";
    private static final String[] TABLES = {TABLE_USERS, TABLE_USER_ACCOUNTS, TABLE_TRAVELS,
            TABLE_TRAVEL_EVENTS, TABLE_RECEIPTS, TABLE_MEAL_REPRESENTATIONS, TABLE_CAR_TRAVELS,
            TABLE_CAR_PASSENGERS, TABLE_STAYS};

    // User attribute constants
    private static final String KEY_USER_SSN = "user_ssn";
    private static final String KEY_USER_NAME = "user_name";
    private static final String KEY_USER_ADDRESS = "user_address";
    private static final String KEY_USER_EMAIL = "user_email";
    private static final String KEY_USER_EMPLOYER_EMAIL = "user_employer_email";
    private static final String KEY_USER_COMPANY = "user_company";
    private static final String KEY_USER_DEPARTMENT = "user_department";

    // User account attribute constants
    private static final String KEY_USER_ACCOUNT_USER_ID = "user_account_user_id";
    private static final String KEY_USER_ACCOUNT_NAME = "user_account_name";
    private static final String KEY_USER_ACCOUNT_NR = "user_account_nr";

    // Travel attribute constants
    private static final String KEY_TRAVEL_ID = "travel_id";
    private static final String KEY_TRAVEL_USER_ID = "travel_user_id";
    private static final String KEY_TRAVEL_ROUTE = "travel_route";
    private static final String KEY_TRAVEL_PURPOSE = "travel_purpose";
    private static final String KEY_TRAVEL_START_DATE = "travel_start_date";
    private static final String KEY_TRAVEL_START_TIME = "travel_start_time";
    private static final String KEY_TRAVEL_CUSTOMER_NAME = "travel_customer_name";
    private static final String KEY_TRAVEL_CUSTOMER_ID = "travel_customer_id";
    private static final String KEY_TRAVEL_PREPAID_AMOUNT = "travel_prepaid_amount";

    // Travel event attribute constants
    private static final String KEY_TRAVEL_EVENT_TRAVEL_ID = "travel_event_travel_id";
    private static final String KEY_TRAVEL_EVENT_NAME = "travel_event_name";

    // Travel event attribute constants
    private static final String KEY_TRAVEL_COMMENT_TRAVEL_ID = "travel_comment_travel_id";
    private static final String KEY_TRAVEL_COMMENT = "travel_event_comment";

    // Receipt attribute constants
    private static final String KEY_RECEIPT_ID = "receipt_id";
    private static final String KEY_RECEIPT_TRAVEL_ID = "receipt_travel_id";
    private static final String KEY_RECEIPT_TYPE = "receipt_type";
    private static final String KEY_RECEIPT_NAME = "receipt_name";
    private static final String KEY_RECEIPT_DATE = "receipt_date";
    private static final String KEY_RECEIPT_TOTAL = "receipt_total";
    private static final String KEY_RECEIPT_CURRENCY = "receipt_currency";
    private static final String KEY_RECEIPT_REPRESENTATION_PURPOSE =
            "receipt_representation_purpose";
    private static final String KEY_RECEIPT_IMAGE = "receipt_image";
    public static final int REIMBURSEMENT_ID = 0;

    // Meal representation attribute constants
    private static final String KEY_REPRESENTATION_RECEIPT_ID = "representation_receipt_id";
    private static final String KEY_REPRESENTATION_NAME = "representation_name";

    // Car travel attribute constants
    private static final String KEY_CAR_ID = "car_id";
    private static final String KEY_CAR_TRAVEL_ID = "car_travel_id";
    private static final String KEY_CAR_DATE = "car_date";
    private static final String KEY_CAR_START = "car_start";
    private static final String KEY_CAR_END = "car_end";
    private static final String KEY_CAR_KM = "car_km";

    // Car passengers attribute constants
    private static final String KEY_CAR_PASSENGER_TRAVEL_ID = "car_passenger_travel_id";
    private static final String KEY_CAR_PASSENGER_NAME = "car_passenger_name";

    // Stay attribute constants
    private static final String KEY_STAY_RECEIPT_ID = "stay_receipt_id";
    private static final String KEY_STAY_NAME = "stay_name";
    private static final String KEY_STAY_ADDRESS = "stay_address";
    private static final String KEY_STAY_DATE = "stay_date";
    private static final String KEY_STAY_DURATION = "stay_duration";
    private static final String KEY_STAY_TYPE = "stay_type";

    // General SQL-statements
    private static final String CREATE_TABLE = "CREATE TABLE ";
    private static final String SPACE = ", ";
    private static final String PRIMARY_KEY = " INTEGER PRIMARY KEY" + SPACE;
    private static final String FOREIGN_KEY = "FOREIGN KEY(";
    private static final String REFERENCES = ") REFERENCES ";
    private static final String SELECT_ALL = "SELECT * FROM ";
    private static final String WHERE = " WHERE ";
    private static final String DELETE_CASCADE = "ON DELETE CASCADE";
    private static final String PRAGMA = "PRAGMA foreign_key=1";

    // Foreign key constants
    private static final String FK_TRAVEL = "travel(travel_id) ";
    private static final String FK_USER = "user(user_ssn) ";
    private static final String FK_RECEIPT = "receipt(receipt_id) ";
    private static final String FK_CAR_TRAVEL = "car_travel(car_id) ";

    // SQL Data types
    private static final String TYPE_TEXT = " TEXT" + SPACE;
    private static final String TYPE_DOUBLE = " DOUBLE" + SPACE;
    private static final String TYPE_INTEGER = " INTEGER" + SPACE;

    // Static instance variables to enforce singleton pattern
    private static Context context;
    private static DatabaseHandler instance;
    /**
     * Creates a database access object which handles all queries and insertions into database.
     */
    private DatabaseHandler() {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Creates a new database handler if none exists.
     * @param context The activity that first initializes the database.
     * @return The created database handler with given
     */
    public static DatabaseHandler getInstance(Context context) {
        if(instance == null) {
            DatabaseHandler.context = context;
            instance = new DatabaseHandler();
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_USERS = CREATE_TABLE + TABLE_USERS + " (" + KEY_USER_SSN + PRIMARY_KEY
                + KEY_USER_NAME + TYPE_TEXT + KEY_USER_ADDRESS + TYPE_TEXT + KEY_USER_EMAIL +
                TYPE_TEXT + KEY_USER_EMPLOYER_EMAIL + TYPE_TEXT +
                KEY_USER_COMPANY + TYPE_TEXT + KEY_USER_DEPARTMENT + " INTEGER)";
        String CREATE_TABLE_USER_ACCOUNTS = CREATE_TABLE + TABLE_USER_ACCOUNTS + " (" +
                KEY_USER_ACCOUNT_USER_ID + TYPE_INTEGER + KEY_USER_ACCOUNT_NAME + TYPE_TEXT +
                KEY_USER_ACCOUNT_NR + PRIMARY_KEY + FOREIGN_KEY + KEY_USER_ACCOUNT_USER_ID +
                REFERENCES + FK_USER + DELETE_CASCADE + ")";
        String CREATE_TABLE_TRAVEL_EVENTS = CREATE_TABLE + TABLE_TRAVEL_EVENTS + " (" +
                KEY_TRAVEL_EVENT_TRAVEL_ID + TYPE_INTEGER + KEY_TRAVEL_EVENT_NAME + TYPE_TEXT +
                FOREIGN_KEY + KEY_TRAVEL_EVENT_TRAVEL_ID + REFERENCES + FK_TRAVEL + DELETE_CASCADE
                + ")";
        String CREATE_TABLE_TRAVEL_COMMENTS = CREATE_TABLE + TABLE_TRAVEL_COMMENTS + " (" +
                KEY_TRAVEL_COMMENT_TRAVEL_ID + TYPE_INTEGER + KEY_TRAVEL_COMMENT + TYPE_TEXT +
                FOREIGN_KEY + KEY_TRAVEL_COMMENT_TRAVEL_ID + REFERENCES + FK_TRAVEL + DELETE_CASCADE
                + ")";
        String CREATE_TABLE_TRAVELS = CREATE_TABLE + TABLE_TRAVELS + " (" + KEY_TRAVEL_ID +
                PRIMARY_KEY + KEY_TRAVEL_USER_ID + TYPE_INTEGER + KEY_TRAVEL_ROUTE + TYPE_TEXT +
                KEY_TRAVEL_PURPOSE + TYPE_TEXT + KEY_TRAVEL_START_DATE + TYPE_TEXT +
                KEY_TRAVEL_START_TIME + TYPE_TEXT + KEY_TRAVEL_CUSTOMER_NAME + TYPE_TEXT +
                KEY_TRAVEL_CUSTOMER_ID + TYPE_INTEGER + KEY_TRAVEL_PREPAID_AMOUNT + TYPE_INTEGER +
                FOREIGN_KEY + KEY_TRAVEL_USER_ID + REFERENCES + FK_USER + DELETE_CASCADE + ")";
        String CREATE_TABLE_RECEIPTS = CREATE_TABLE + TABLE_RECEIPTS + " (" + KEY_RECEIPT_ID +
                PRIMARY_KEY + KEY_RECEIPT_TRAVEL_ID + TYPE_INTEGER + KEY_RECEIPT_TYPE + TYPE_TEXT +
                KEY_RECEIPT_NAME + TYPE_TEXT + KEY_RECEIPT_DATE + TYPE_TEXT + KEY_RECEIPT_TOTAL +
                TYPE_DOUBLE + KEY_RECEIPT_CURRENCY + TYPE_TEXT + KEY_RECEIPT_REPRESENTATION_PURPOSE
                + TYPE_TEXT + KEY_RECEIPT_IMAGE + TYPE_TEXT + FOREIGN_KEY + KEY_RECEIPT_TRAVEL_ID +
                REFERENCES + FK_TRAVEL + DELETE_CASCADE + ")";
        String CREATE_TABLE_MEAL_REPRESENTATIONS = CREATE_TABLE + TABLE_MEAL_REPRESENTATIONS + "(" +
                KEY_REPRESENTATION_RECEIPT_ID + TYPE_INTEGER + KEY_REPRESENTATION_NAME +
                TYPE_TEXT + FOREIGN_KEY + KEY_REPRESENTATION_RECEIPT_ID + REFERENCES + FK_RECEIPT +
                DELETE_CASCADE + ")";
        String CREATE_TABLE_CAR_TRAVELS = CREATE_TABLE + TABLE_CAR_TRAVELS + " (" + KEY_CAR_ID +
                PRIMARY_KEY + KEY_CAR_TRAVEL_ID + TYPE_INTEGER + KEY_CAR_DATE + TYPE_TEXT +
                KEY_CAR_START + TYPE_TEXT + KEY_CAR_END + TYPE_TEXT + KEY_CAR_KM + TYPE_INTEGER +
                FOREIGN_KEY + KEY_CAR_TRAVEL_ID + REFERENCES + FK_TRAVEL + DELETE_CASCADE + ")";
        String CREATE_TABLE_CAR_PASSENGERS = CREATE_TABLE + TABLE_CAR_PASSENGERS + "(" +
                KEY_CAR_PASSENGER_TRAVEL_ID + TYPE_INTEGER + KEY_CAR_PASSENGER_NAME + TYPE_TEXT +
                FOREIGN_KEY + KEY_CAR_PASSENGER_TRAVEL_ID + REFERENCES +
                FK_CAR_TRAVEL + DELETE_CASCADE + ")";
        String CREATE_TABLE_STAYS = CREATE_TABLE + TABLE_STAYS + " (" + KEY_STAY_RECEIPT_ID +
                TYPE_INTEGER + KEY_STAY_NAME + TYPE_TEXT + KEY_STAY_ADDRESS + TYPE_TEXT +
                KEY_STAY_DATE + TYPE_TEXT + KEY_STAY_DURATION + TYPE_INTEGER + KEY_STAY_TYPE +
                TYPE_TEXT + FOREIGN_KEY + KEY_STAY_RECEIPT_ID + REFERENCES + FK_RECEIPT +
                DELETE_CASCADE + ")";

        String[] CREATE_TABLES = {CREATE_TABLE_USERS, CREATE_TABLE_USER_ACCOUNTS,
                CREATE_TABLE_TRAVELS, CREATE_TABLE_TRAVEL_EVENTS, CREATE_TABLE_TRAVEL_COMMENTS,
                CREATE_TABLE_RECEIPTS, CREATE_TABLE_MEAL_REPRESENTATIONS, CREATE_TABLE_CAR_TRAVELS,
                CREATE_TABLE_CAR_PASSENGERS, CREATE_TABLE_STAYS};

        db.setForeignKeyConstraintsEnabled(true);

        for (String createTable : CREATE_TABLES) {
            db.execSQL(createTable);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (String table : TABLES) {
            db.execSQL("DROP TABLE IF EXISTS " + table);
        }

        onCreate(db);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
        db.execSQL(PRAGMA);
    }

    @SuppressWarnings("unused")
    /**
     * Returns the size of a given table.
     * @param table The table to count.
     * @return An integer representing the size of the given table.
     */
    public int getTableSize(String table) {
        SQLiteDatabase db = getReadableDatabase();

        String countQuery = SELECT_ALL + table;

        Cursor cursor = db.rawQuery(countQuery, null);

        int count = 0;
        if (cursor != null && !cursor.isClosed()) {
            count = cursor.getCount();
            cursor.close();
        }
        return count;
    }

    /**
     * Inserts a user into the user table with a given ID.
     * @param user The user to be inserted.
     * @return False if error occurs, true otherwise.
     */
    public boolean insertUser(User user) {
        SQLiteDatabase db = getWritableDatabase();

        long result = db.insert(TABLE_USERS, null, getUserContent(user));

        boolean inserted = insertUserAccounts(db, user);
        if (!inserted) {
            Log.e(TAG + TABLE_USER_ACCOUNTS, "Error while inserting user accounts.");
        }

        db.close();
        return result != -1;
    }

    /**
     * Updates a user in the user table.
     * @param user The user to update.
     * @return False if no rows were effected, true otherwise.
     */
    public boolean updateUser(User user) {
        SQLiteDatabase db = getWritableDatabase();

        int effected = db.update(TABLE_USERS, getUserContent(user), KEY_USER_SSN + " = ?",
                new String[]{String.valueOf(user.getSsn())});

        boolean insert = insertUserAccounts(db, user);
        if (!insert) {
            Log.e(TAG + TABLE_USER_ACCOUNTS, "Error while updating user accounts.");
        }

        db.close();
        return effected != 0;
    }

    /**
     * Returns all user content attributes for handling.
     * @param user The user to get content from.
     * @return A mapping of content values from key to attribute.
     */
    private ContentValues getUserContent(User user) {
        ContentValues values = new ContentValues();
        values.put(KEY_USER_SSN, user.getSsn());
        values.put(KEY_USER_NAME, user.getName());
        values.put(KEY_USER_ADDRESS, user.getAddress());;
        values.put(KEY_USER_EMAIL, user.getEmail());
        values.put(KEY_USER_EMPLOYER_EMAIL, user.getEmployerEmail());
        values.put(KEY_USER_COMPANY, user.getCompany());
        values.put(KEY_USER_DEPARTMENT, user.getDepartment());

        return values;
    }

    /**
     * Returns a user by a given SSN from the user table.
     * @param ssn The SSN of the travel to return.
     * @return The user with the given SSN from the travel table.
     */
    public User getUser(long ssn) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_USERS, new String[]{KEY_USER_SSN, KEY_USER_NAME,
                        KEY_USER_ADDRESS, KEY_USER_EMAIL, KEY_USER_EMPLOYER_EMAIL,
                        KEY_USER_COMPANY, KEY_USER_DEPARTMENT}, KEY_USER_SSN + " = ?",
                new String[]{String.valueOf(ssn)}, null, null, null, null);

        User user = null;
        if (cursor != null && cursor.moveToFirst()) {
            user = new User(Long.parseLong(cursor.getString(0)), cursor.getString(1),
                    cursor.getString(2), cursor.getString(3), cursor.getString(4),
                    cursor.getString(5), Integer.parseInt(cursor.getString(6)));

            user.setAccounts(getUserAccounts(db, user));

            cursor.close();
        }

        return user;
    }

    /**
     * Returns all users from the user table.
     * @return An array list containing all users from the user table.
     */
    public List<User> getAllUsers() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery(SELECT_ALL + TABLE_USERS, null);

        List<User> users = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                User user = new User(Long.parseLong(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4), cursor.getString(5),
                        Integer.parseInt(cursor.getString(6)));
                user.setAccounts(getUserAccounts(db, user));
                users.add(user);
            } while (cursor.moveToNext());
            cursor.close();
        }

        return users;
    }

    /**
     * Deletes a given user by SSN from the user table.
     * @param ssn The SSN of the user to delete.
     * @return False if no rows were effected, true otherwise.
     */
    public boolean deleteUser(long ssn) {
        SQLiteDatabase db = getWritableDatabase();

        // Deletes all receipt images upon deletion
        if(!getAllTravels(ssn).isEmpty()) {
            for (Travel travel : getAllTravels(ssn)) {
                if (!getAllReceipts(travel.getId()).isEmpty()) {
                    for (Receipt r : getAllReceipts(travel.getId())) {
                        boolean deleted = CameraUtils.deleteImageFromStorage(r.getImagePath());
                        if (!deleted) {
                            Log.e(TABLE_TRAVELS, "Deletion of receipt image failed.");
                        } else {
                            Log.i(TABLE_TRAVELS, "Successfully deleted receipt image.");
                        }
                    }
                }
            }
        }

        int effected = db.delete(TABLE_USERS, KEY_USER_SSN + " = ?",
                new String[]{String.valueOf(ssn)});
        db.close();

        return effected != 0;
    }

    /**
     * Inserts a users accounts into the user accounts table.
     * @param db The initialized database in the insertUser-method.
     * @param user The user for which to insert accounts.
     * @return False if error occurs, true otherwise.
     */
    private boolean insertUserAccounts(SQLiteDatabase db, User user) {
        List<String> accountNames = new ArrayList<>(user.getAccounts().keySet());

        long error = 0;

        if(!accountNames.isEmpty()) {
            boolean deleted = deleteUserAccounts(db, user);
            if (!deleted) {
                Log.d(TAG + TABLE_USER_ACCOUNTS, "No user accounts were deleted.");
            }

            for (String key : accountNames) {
                long err = db.insert(TABLE_USER_ACCOUNTS, null, getUserAccountContent(user, key));

                // Ensures an error is reported despite following successful attempts
                if (error != -1) {
                    error = err;
                }
            }
        }

        return error != -1;
    }

    /**
     * Deletes a users accounts prior to insertion after update.
     * @param db     The initialized database in the insertUserAccounts-method.
     * @param user The user for which to delete accounts.
     * @return False if no rows were effected, true otherwise.
     */
    private boolean deleteUserAccounts(SQLiteDatabase db, User user) {
        int effected = -1;

        for (String key : new ArrayList<>(getUserAccounts(db, user).keySet())) {
            int eff = db.delete(TABLE_USER_ACCOUNTS, KEY_USER_ACCOUNT_NAME + " = ?",
                    new String[]{String.valueOf(key)});

            if (effected != 0) {
                effected = eff;
            }
        }

        return effected != 0;
    }

    /**
     * Returns all user account content attributes for handling.
     * @param user The user to get account content from.
     * @param key  The key of current account.
     * @return A mapping of content values from key to attribute.
     */
    private ContentValues getUserAccountContent(User user, String key) {
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ACCOUNT_USER_ID, user.getSsn());
        values.put(KEY_USER_ACCOUNT_NAME, MapUtil.getKeyByValue(user.getAccounts(),
                user.getAccounts().get(key)));
        values.put(KEY_USER_ACCOUNT_NR, user.getAccounts().get(key));

        return values;
    }

    /**
     * Returns a users accounts from the user accounts table by a given user.
     * @param db   The database already initialized in the getUser-methods.
     * @param user The user for which to get accounts from.
     * @return A hash map representing both the name and the number of an account.
     */
    private HashMap<String, Long> getUserAccounts(SQLiteDatabase db, User user) {
        List<String> accountNames = getTableList(db, user.getSsn(), TABLE_USER_ACCOUNTS,
                KEY_USER_ACCOUNT_USER_ID, KEY_USER_ACCOUNT_NAME);
        List<String> temp = getTableList(db, user.getSsn(), TABLE_USER_ACCOUNTS,
                KEY_USER_ACCOUNT_USER_ID, KEY_USER_ACCOUNT_NR);


        List<Long> accountNumbers = new ArrayList<>();
        for (String number : temp) {
            long parsed = Long.parseLong(number);
            accountNumbers.add(parsed);
        }

        HashMap<String, Long> mapping = new HashMap<>();
        for (int i = 0; i < accountNames.size(); i++) {
            mapping.put(accountNames.get(i), accountNumbers.get(i));
        }

        return mapping;
    }

    /**
     * Inserts a travel into the travel table with a generated ID.
     *
     * @param travel The travel to be inserted.
     * @return False if an error occurs, true otherwise.
     */
    public boolean insertTravel(Travel travel) {
        SQLiteDatabase db = getWritableDatabase();

        long result = db.insert(TABLE_TRAVELS, null, getTravelContent(travel));

        boolean insertedEvents = insertTravelEvents(db, travel);
        if(!insertedEvents) {
            Log.e(TAG + TABLE_TRAVEL_EVENTS, "Error while inserting travel events.");
        }
        boolean insertedComments = insertTravelComments(db, travel);
        if(!insertedComments) {
            Log.e(TAG + TABLE_TRAVEL_COMMENTS, "Error while inserting travel comments.");
        }

        db.close();
        return result != -1;
    }

    /**
     * Updates a travel in the travel table.
     * @param travel The travel to update.
     * @return False if no rows were effected, true otherwise.
     */
    public boolean updateTravel(Travel travel) {
        SQLiteDatabase db = getWritableDatabase();

        int effected = db.update(TABLE_TRAVELS, getTravelContent(travel), KEY_TRAVEL_ID + " = ?",
                new String[]{String.valueOf(travel.getId())});

        boolean insertedEvents = insertTravelEvents(db, travel);
        if(!insertedEvents) {
            Log.e(TAG + TABLE_TRAVEL_EVENTS, "Error while updating travel events.");
        }
        boolean insertedComments = insertTravelComments(db, travel);
        if(!insertedComments) {
            Log.e(TAG + TABLE_TRAVEL_COMMENTS, "Error while updating travel comments.");
        }

        db.close();
        return effected != 0;
    }

    /**
     * Returns all travel content attributes for handling.
     * @param travel The travel to get content from.
     * @return A mapping of content values from key to attribute.
     */
    private ContentValues getTravelContent(Travel travel) {
        ContentValues values = new ContentValues();
        values.put(KEY_TRAVEL_ID, travel.getId());
        values.put(KEY_TRAVEL_USER_ID, travel.getFk());
        values.put(KEY_TRAVEL_ROUTE, travel.getRoute());
        values.put(KEY_TRAVEL_PURPOSE, travel.getPurpose());
        values.put(KEY_TRAVEL_START_DATE, travel.getStartDate());
        values.put(KEY_TRAVEL_START_TIME, travel.getStartTime());
        values.put(KEY_TRAVEL_CUSTOMER_NAME, travel.getCustomerName());
        values.put(KEY_TRAVEL_CUSTOMER_ID, travel.getCustomerId());
        values.put(KEY_TRAVEL_PREPAID_AMOUNT, travel.getPrepaidAmount());

        return values;
    }

    /**
     * Returns a travel by a given ID from the travel table.
     * @param id The ID of the travel to return.
     * @return The travel with the given ID from the travel table.
     */
    public Travel getTravel(int id) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_TRAVELS, new String[]{KEY_TRAVEL_ID, KEY_TRAVEL_USER_ID,
                        KEY_TRAVEL_ROUTE, KEY_TRAVEL_PURPOSE, KEY_TRAVEL_START_DATE,
                        KEY_TRAVEL_START_TIME, KEY_TRAVEL_CUSTOMER_NAME, KEY_TRAVEL_CUSTOMER_ID,
                        KEY_TRAVEL_PREPAID_AMOUNT}, KEY_TRAVEL_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        Travel travel = null;
        if (cursor != null && cursor.moveToFirst()) {
            travel = new Travel(Integer.parseInt(cursor.getString(0)),
                    Long.parseLong(cursor.getString(1)), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4), cursor.getString(5));
            travel.setCustomerName(cursor.getString(6));
            travel.setCustomerId(Integer.parseInt(cursor.getString(7)));
            travel.setPrepaidAmount(Integer.parseInt(cursor.getString(8)));
            travel.setEvents(getTableList(db, travel.getId(), TABLE_TRAVEL_EVENTS,
                    KEY_TRAVEL_EVENT_TRAVEL_ID, KEY_TRAVEL_EVENT_NAME));
            travel.setComments(getTableList(db, travel.getId(), TABLE_TRAVEL_COMMENTS,
                    KEY_TRAVEL_COMMENT_TRAVEL_ID, KEY_TRAVEL_COMMENT));

            cursor.close();
        }

        return travel;
    }

    /**
     * Returns all travels from the travel table.
     * @param ssn The ID of the connected user to get travels from.
     * @return An array list containing all travels from the travel table.
     */
    public List<Travel> getAllTravels(long ssn) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery(SELECT_ALL + TABLE_TRAVELS + WHERE + KEY_TRAVEL_USER_ID +
                " = ?", new String[]{String.valueOf(ssn)});

        List<Travel> travels = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                Travel travel = new Travel(Integer.parseInt(cursor.getString(0)),
                        Long.parseLong(cursor.getString(1)),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4),
                        cursor.getString(5));
                travel.setCustomerName(cursor.getString(6));
                travel.setCustomerId(Integer.parseInt(cursor.getString(7)));
                travel.setPrepaidAmount(Double.parseDouble(cursor.getString(8)));
                travel.setEvents(getTableList(db, travel.getId(), TABLE_TRAVEL_EVENTS,
                        KEY_TRAVEL_EVENT_TRAVEL_ID, KEY_TRAVEL_EVENT_NAME));
                travel.setComments(getTableList(db, travel.getId(), TABLE_TRAVEL_COMMENTS,
                        KEY_TRAVEL_COMMENT_TRAVEL_ID, KEY_TRAVEL_COMMENT));
                travels.add(travel);
            } while (cursor.moveToNext());
            cursor.close();
        }

        return travels;
    }

    /**
     * Deletes a given table by ID from the travel table.
     * @param id The ID of travel to be deleted.
     * @return False if no rows were effected, true otherwise.
     */
    public boolean deleteTravel(int id) {
        SQLiteDatabase db = getWritableDatabase();

        // Deletes all receipt images upon deletion
        if(!getAllReceipts(id).isEmpty()) {
            for(Receipt r : getAllReceipts(id)) {
                boolean deleted = CameraUtils.deleteImageFromStorage(r.getImagePath());
                if(!deleted) {
                    Log.e(TABLE_TRAVELS, "Deletion of receipt image failed.");
                } else {
                    Log.i(TABLE_TRAVELS, "Successfully deleted receipt image.");
                }
            }
        }

        int effected = db.delete(TABLE_TRAVELS, KEY_TRAVEL_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();

        return effected != 0;
    }

    /**
     * Inserts all travel events into the travel event table.
     * @param db     The initialized database in the insertTravel-method.
     * @param travel The travel for which to insert events for.
     * @return False if an error occurs, true otherwise.
     */
    public boolean insertTravelEvents(SQLiteDatabase db, Travel travel) {
        List<String> events = travel.getEvents();

        long error = 0;

        boolean deleted = deleteTravelEvents(db, travel);
        if(!deleted) {
            Log.d(TAG + TABLE_TRAVEL_EVENTS, "No travel events were deleted.");
        }

        if(!events.isEmpty()) {
            for (int i = 0; i < events.size(); i++) {
                long err = db.insert(TABLE_TRAVEL_EVENTS, null, getTravelEventsContent(travel, i));

                // Ensures an error is reported despite following successful attempts
                if (error != -1) {
                    error = err;
                }
            }
        }

        return error != -1;
    }

    /**
     * Deletes all travel events to insertion after update.
     * @param db     The initialized database in the insertTravelEvent-method.
     * @param travel The travel for which to update events for.
     * @return False if no rows were effected, true otherwise.
     */
    private boolean deleteTravelEvents(SQLiteDatabase db, Travel travel) {
        int effected = db.delete(TABLE_TRAVEL_EVENTS, KEY_TRAVEL_EVENT_TRAVEL_ID + " = ?",
                new String[]{String.valueOf(travel.getId())});

        return effected != 0;
    }

    /**
     * Returns all travel events content attributes for handling.
     * @param travel The travel to get event content from.
     * @param i The index of the current travel events.
     * @return A mapping of content values from key to attribute.
     */
    private ContentValues getTravelEventsContent(Travel travel, int i) {
        ContentValues values = new ContentValues();
        values.put(KEY_TRAVEL_EVENT_TRAVEL_ID, travel.getId());
        values.put(KEY_TRAVEL_EVENT_NAME, travel.getEvents().get(i));

        return values;
    }

    /**
     * Inserts a travel comment into the travel comment table.
     * @param db     The initialized database in the insertTravel-method.
     * @param travel The travel for which to insert comment for.
     * @return False if an error occurs, true otherwise.
     */
    public boolean insertTravelComments(SQLiteDatabase db, Travel travel) {
        List<String> comments = travel.getComments();

        long error = 0;

        boolean deleted = deleteTravelComments(db, travel);
        if(!deleted) {
            Log.d(TAG + TABLE_TRAVEL_COMMENTS, "No travel comments were deleted.");
        }

        if(!comments.isEmpty()) {
            for (int i = 0; i < comments.size(); i++) {
                long err = db.insert(
                        TABLE_TRAVEL_COMMENTS, null, getTravelCommentsContent(travel, i));

                if (error != -1) {
                    error = err;
                }
            }
        }

        return error != -1;
    }

    /**
     * Deletes all user accounts prior to insertion after update.
     * @param db     The initialized database in the insertTravelComment-method.
     * @param travel The travel for which to delete comments.
     * @return False if no rows were effected, true otherwise.
     */
    private boolean deleteTravelComments(SQLiteDatabase db, Travel travel) {
        int effected = db.delete(TABLE_TRAVEL_COMMENTS, KEY_TRAVEL_COMMENT_TRAVEL_ID + " = ?",
                new String[]{String.valueOf(travel.getId())});

        return effected != 0;
    }

    /**
     * Returns all travel comments content attributes for handling.
     * @param travel The travel to get comments content from.
     * @param i The index of the current travel comment.
     * @return A mapping of content values from key to attribute.
     */
    private ContentValues getTravelCommentsContent(Travel travel, int i) {
        ContentValues values = new ContentValues();
        values.put(KEY_TRAVEL_COMMENT_TRAVEL_ID,travel.getId());
        values.put(KEY_TRAVEL_COMMENT, travel.getComments().get(i));

        return values;
    }

    /**
     * Inserts a receipt into the receipt table with a generated ID.
     * @param receipt The receipt to be inserted.
     * @return False if an error occurs, true otherwise.
     */
    public boolean insertReceipt(Receipt receipt) {
        SQLiteDatabase db = getWritableDatabase();

        long error = db.insert(TABLE_RECEIPTS, null, getReceiptContent(receipt));

        if(ArrayPositionUtil.hasMealRepresentationPosition(context, receipt.getType())) {
            boolean inserted = insertRepresentation(db, receipt);
            if (!inserted) {
                Log.e(TAG + TABLE_MEAL_REPRESENTATIONS, "Error while inserting representation.");
            }
        }
        if(ArrayPositionUtil.hasHotelPosition(context, receipt.getType())) {
            boolean inserted = insertStay(db, receipt.getStay());
            if(!inserted) {
                Log.e(TAG + TABLE_STAYS, "Error while inserting stay.");
            }
        }

        db.close();
        return error != -1;
    }

    /**
     * Updates a receipt in the receipt table.
     * @param receipt The receipt to be updated.
     * @return False if no rows were effected, true otherwise.
     */
    public boolean updateReceipt(Receipt receipt) {
        SQLiteDatabase db = getWritableDatabase();

        int effected = db.update(TABLE_RECEIPTS, getReceiptContent(receipt),
                KEY_RECEIPT_ID + " = ?", new String[]{String.valueOf(receipt.getId())});

        if(ArrayPositionUtil.hasMealRepresentationPosition(context, receipt.getType())) {
            boolean inserted = insertRepresentation(db, receipt);
            if (!inserted) {
                Log.e(TAG + TABLE_MEAL_REPRESENTATIONS, "Error while updating representation.");
            }
        }
        if(ArrayPositionUtil.hasHotelPosition(context, receipt.getType())) {
            boolean updated = updateStay(db, receipt.getStay());
            if(!updated) {
                Log.d(TAG + TABLE_STAYS, "No stays were updated.");
            }
        }

        db.close();
        return effected != 0;
    }

    /**
     * Returns all receipt content attributes for handling.
     * @param receipt The receipt to get content from.
     * @return A mapping of content values from key to attribute.
     */
    private ContentValues getReceiptContent(Receipt receipt) {
        ContentValues values = new ContentValues();
        values.put(KEY_RECEIPT_ID, receipt.getId());
        values.put(KEY_RECEIPT_TRAVEL_ID, receipt.getFk());
        values.put(KEY_RECEIPT_TYPE, receipt.getType());
        values.put(KEY_RECEIPT_NAME, receipt.getName());
        values.put(KEY_RECEIPT_DATE, receipt.getDate());
        values.put(KEY_RECEIPT_TOTAL, receipt.getTotal());
        values.put(KEY_RECEIPT_CURRENCY, receipt.getCurrency());
        values.put(KEY_RECEIPT_REPRESENTATION_PURPOSE, receipt.getRepresentationPurpose());
        values.put(KEY_RECEIPT_IMAGE, receipt.getImagePath());

        return values;
    }

    /**
     * Returns a receipt from a given ID from the receipt table.
     * @param id The ID of the receipt to return.
     * @return The receipt with the given ID from the receipt table.
     */
    public Receipt getReceipt(int id) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_RECEIPTS, new String[]{KEY_RECEIPT_ID, KEY_RECEIPT_TRAVEL_ID,
                        KEY_RECEIPT_TYPE, KEY_RECEIPT_NAME, KEY_RECEIPT_DATE, KEY_RECEIPT_TOTAL,
                        KEY_RECEIPT_CURRENCY, KEY_RECEIPT_REPRESENTATION_PURPOSE,
                        KEY_RECEIPT_IMAGE}, KEY_RECEIPT_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        Receipt receipt = null;
        if (cursor != null && cursor.moveToFirst()) {
            receipt = new Receipt(Integer.parseInt(cursor.getString(0)),
                    Integer.parseInt(cursor.getString(1)), cursor.getString(2), cursor.getString(3),
                    cursor.getString(4), Double.parseDouble(cursor.getString(5)),
                    cursor.getString(6), cursor.getString(8));

            if(ArrayPositionUtil.hasMealRepresentationPosition(context, receipt.getType())) {
                receipt.setRepresentationPurpose(cursor.getString(7));
                receipt.setRepresentatives(getTableList(db, receipt.getId(),
                        TABLE_MEAL_REPRESENTATIONS, KEY_REPRESENTATION_RECEIPT_ID,
                        KEY_REPRESENTATION_NAME));
            }

            if(ArrayPositionUtil.hasHotelPosition(context, receipt.getType())) {
                receipt.setStay(getStay(db, receipt.getId()));
            }
            cursor.close();
        }

        return receipt;
    }

    /**
     * Returns all receipts from the receipt table with the given ID as foreign key.
     * @param fk The ID which is provided as foreign key reference.
     * @return An array list containing all receipts with given foreign key from the receipt table.
     */
    public List<Receipt> getAllReceipts(int fk) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery(SELECT_ALL + TABLE_RECEIPTS + WHERE + KEY_RECEIPT_TRAVEL_ID +
                " = ?", new String[]{String.valueOf(fk)});

        List<Receipt> receipts = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()){
            do {
                Receipt receipt = new Receipt(Integer.parseInt(cursor.getString(0)),
                        Integer.parseInt(cursor.getString(1)), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4),
                        Double.parseDouble(cursor.getString(5)), cursor.getString(6),
                        cursor.getString(8));
                if(ArrayPositionUtil.hasMealRepresentationPosition(context, receipt.getType())) {
                    receipt.setRepresentationPurpose(cursor.getString(7));
                    receipt.setRepresentatives(getTableList(db, receipt.getId(),
                            TABLE_MEAL_REPRESENTATIONS, KEY_REPRESENTATION_RECEIPT_ID,
                            KEY_REPRESENTATION_NAME));
                }
                if(ArrayPositionUtil.hasHotelPosition(context, receipt.getType())) {
                    receipt.setStay(getStay(db, receipt.getId()));
                }
                receipts.add(receipt);
            } while (cursor.moveToNext());
            cursor.close();
        }

        return receipts;
    }

    /**
     * Deletes a given receipt by ID from the receipt table.
     * @param id The ID of the receipt to delete.
     * @return False if no rows were effected, true otherwise.
     */
    public boolean deleteReceipt(int id) {
        SQLiteDatabase db = getWritableDatabase();

        // Deletes the stored image of the receipt
        boolean deleted = CameraUtils.deleteImageFromStorage(getReceipt(id).getImagePath());
        if(!deleted) {
            Log.e(TABLE_RECEIPTS, "Deletion of receipt image failed.");
        } else {
            Log.i(TABLE_RECEIPTS, "Successfully deleted receipt image.");
        }

        int effected = db.delete(TABLE_RECEIPTS, KEY_RECEIPT_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();

        return effected != 0;
    }

    /**
     * Inserts a representation meals representatives into the representation table.
     * @param db The initialized database in the insertReceipt-method.
     * @param receipt The receipt for which to insert representative for.
     * @return False if an error occurs, true otherwise.
     */
    public boolean insertRepresentation(SQLiteDatabase db, Receipt receipt) {
        List<String> representatives = receipt.getRepresentatives();

        long error = 0;

        if(!representatives.isEmpty()) {
            boolean deleted = deleteRepresentation(db, receipt);
            if(!deleted) {
                Log.d(TAG + TABLE_MEAL_REPRESENTATIONS, "No representatives were deleted.");
            }

            for(int i = 0; i < representatives.size(); i++) {
                long err = db.insert(TABLE_MEAL_REPRESENTATIONS, null,
                        getReceiptRepresentativeContent(receipt, i));

                // Ensures an error is reported despite following successful attempts
                if (error != -1) {
                    error = err;
                }
            }
        }

        return error != -1;
    }

    /**
     * Deletes all receipt representatives prior to insertion after update.
     * @param db     The initialized database in the insertRepresentation-method.
     * @param receipt The receipt for which to delete representatives.
     * @return False if no rows were effected, true otherwise.
     */
    private boolean deleteRepresentation(SQLiteDatabase db, Receipt receipt) {
        int effected = db.delete(TABLE_MEAL_REPRESENTATIONS, KEY_REPRESENTATION_RECEIPT_ID + " = ?",
                new String[]{String.valueOf(receipt.getId())});

        return effected != 0;
    }

    /**
     * Returns all receipt representative attributes for handling.
     * @param receipt The receipt to get content from.
     * @param i The index of the current receipt.
     * @return A mapping of content values from key to attribute.
     */
    private ContentValues getReceiptRepresentativeContent(Receipt receipt, int i) {
        ContentValues values = new ContentValues();
        values.put(KEY_REPRESENTATION_RECEIPT_ID, receipt.getId());
        values.put(KEY_REPRESENTATION_NAME, receipt.getRepresentatives().get(i));

        return values;
    }


    /**
     * Inserts a stay into the stay table with a generated ID.
     * @param db The initialized database in the insertReceipt-method.
     * @param stay The stay to be inserted.
     * @return False if an error occurs, true otherwise.
     */
    private boolean insertStay(SQLiteDatabase db, Stay stay) {
        long error = db.insert(TABLE_STAYS, null, getStayContent(stay));
        db.close();

        return error != -1;
    }

    /**
     * Updates a stay in the stay table.
     * @param db The initialized database in the insertReceipt-method.
     * @param stay The stay to be updated.
     * @return False if no rows were effected, true otherwise.
     */
    private boolean updateStay(SQLiteDatabase db, Stay stay) {
        int effected = db.update(TABLE_STAYS, getStayContent(stay), KEY_STAY_RECEIPT_ID + " = ?",
                new String[]{String.valueOf(stay.getFk())});
        db.close();

        return effected != 0;
    }

    /**
     * Returns all stay content attributes for handling.
     * @param stay The stay to get content from.
     * @return A mapping of content values from key to attribute.
     */
    private ContentValues getStayContent(Stay stay) {
        ContentValues values = new ContentValues();
        values.put(KEY_STAY_RECEIPT_ID, stay.getFk());
        values.put(KEY_STAY_NAME, stay.getName());
        values.put(KEY_STAY_ADDRESS, stay.getAddress());
        values.put(KEY_STAY_DATE, stay.getDate());
        values.put(KEY_STAY_DURATION, stay.getDuration());
        values.put(KEY_STAY_TYPE, stay.getType());

        return values;
    }

    /**
     * Returns a stay from a given foreign key ID from the stay table.
     * @param db The initialized database in the getReceipt-methods.
     * @param fk The ID of the connected receipt.
     * @return The stay with the given ID from the stay table.
     */
    private Stay getStay(SQLiteDatabase db, int fk) {
        Cursor cursor = db.query(TABLE_STAYS, new String[]{KEY_STAY_RECEIPT_ID,
                        KEY_STAY_NAME, KEY_STAY_ADDRESS, KEY_STAY_DATE, KEY_STAY_DURATION,
                        KEY_STAY_TYPE}, KEY_STAY_RECEIPT_ID + " = ?",
                new String[]{String.valueOf(fk)}, null, null, null, null);

        Stay stay = null;
        if(cursor != null && cursor.moveToFirst()){
            stay = new Stay(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
                    cursor.getString(2), cursor.getString(3), Integer.parseInt(cursor.getString(4)),
                    cursor.getString(5));
            cursor.close();
        }

        return stay;
    }

    /**
     * Inserts a car travel into the car travel table with a generated ID.
     * @param carTravel The car travel to be inserted.
     * @return False if an error occurs, true otherwise.
     */
    public boolean insertCarTravel(CarTravel carTravel) {
        SQLiteDatabase db = getWritableDatabase();

        long error = db.insert(TABLE_CAR_TRAVELS, null, getCarTravelContent(carTravel));

        boolean inserted = insertCarPassengers(db, carTravel);
        if(!inserted) {
            Log.e(TAG + TABLE_CAR_PASSENGERS, "Error while inserting car passengers.");
        }

        db.close();
        return error != -1;
    }

    /**
     * Updates a car travel in the car travel table.
     * @param carTravel The car travel to be updated.
     * @return False if no rows were effected, true otherwise.
     */
    public boolean updateCarTravel(CarTravel carTravel) {
        SQLiteDatabase db = getWritableDatabase();

        int effected = db.update(TABLE_CAR_TRAVELS, getCarTravelContent(carTravel),
                KEY_CAR_ID + " = ?", new String[]{String.valueOf(carTravel.getId())});

        boolean inserted = insertCarPassengers(db, carTravel);
        if(!inserted) {
            Log.e(TAG + TABLE_CAR_PASSENGERS, "Error while updating car passengers.");
        }

        db.close();
        return effected != 0;
    }

    /**
     * Returns all car travel content attributes for handling.
     * @param carTravel The car travel to get content from.
     * @return A mapping of content values from key to attribute.
     */
    private ContentValues getCarTravelContent(CarTravel carTravel) {
        ContentValues values = new ContentValues();
        values.put(KEY_CAR_ID, carTravel.getId());
        values.put(KEY_CAR_TRAVEL_ID, carTravel.getFk());
        values.put(KEY_CAR_DATE, carTravel.getDate());
        values.put(KEY_CAR_START, carTravel.getStart());
        values.put(KEY_CAR_END, carTravel.getEnd());
        values.put(KEY_CAR_KM, carTravel.getKilometers());

        return values;
    }

    /**
     * Returns a car travel from a given ID from the car travel table.
     * @param id The ID of the car travel to return.
     * @return The car travel with the given ID from the car travel table.
     */
    public CarTravel getCarTravel(int id) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_CAR_TRAVELS, new String[]{KEY_CAR_ID, KEY_CAR_TRAVEL_ID,
                        KEY_CAR_DATE, KEY_CAR_START, KEY_CAR_END, KEY_CAR_KM},
                KEY_CAR_ID + " = ?", new String[]{String.valueOf(id)}, null, null, null, null);

        CarTravel carTravel = null;
        if(cursor != null && cursor.moveToFirst()) {
            carTravel = new CarTravel(Integer.parseInt(cursor.getString(0)),
                    Integer.parseInt(cursor.getString(1)), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4),
                    Double.parseDouble(cursor.getString(5)));
            carTravel.setPassengers(getTableList(db, carTravel.getId(), TABLE_CAR_PASSENGERS,
                    KEY_CAR_PASSENGER_TRAVEL_ID, KEY_CAR_PASSENGER_NAME));
            cursor.close();
        }

        return carTravel;
    }

    /**
     * Returns all car travels from the car travel table with a given ID as foreign key.
     * @param fk The ID which is provided as foreign key reference.
     * @return An array list containing all car travels with given foreign key from the car travel
     * table .
     */
    public List<CarTravel> getAllCarTravels(int fk) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery(SELECT_ALL + TABLE_CAR_TRAVELS + WHERE + KEY_CAR_TRAVEL_ID +
                " = ?", new String[]{String.valueOf(fk)});

        List<CarTravel> carTravels = new ArrayList<>();
        if(cursor != null && cursor.moveToFirst()) {
            do {
                CarTravel carTravel = new CarTravel(Integer.parseInt(cursor.getString(0)),
                        Integer.parseInt(cursor.getString(1)), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4),
                        Double.parseDouble(cursor.getString(5)));
                carTravel.setPassengers(getTableList(db, carTravel.getId(),
                        TABLE_CAR_PASSENGERS, KEY_CAR_PASSENGER_TRAVEL_ID, KEY_CAR_PASSENGER_NAME));
                carTravels.add(carTravel);
            } while (cursor.moveToNext());
            cursor.close();
        }

        return carTravels;
    }

    /**
     * Deletes a given car travel by ID from the car travel table.
     * @param id The ID of the car travel to delete.
     * @return False if no rows were effected, true otherwise.
     */
    public boolean deleteCarTravel(int id) {
        SQLiteDatabase db = getWritableDatabase();

        int effected = db.delete(TABLE_CAR_TRAVELS, KEY_CAR_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();

        return effected != 0;
    }

    /**
     * Inserts a car travels passengers into the car passenger table.
     * @param db The initialized database in the insertCarTravel-method.
     * @param carTravel The car travel for which to insert car passengers.
     * @return False if an error occur, true otherwise.
     */
    private boolean insertCarPassengers(SQLiteDatabase db, CarTravel carTravel) {
        List<String> passengers = carTravel.getPassengers();

        long error = 0;

        if(!passengers.isEmpty()) {
            boolean deleted = deleteCarPassengers(db, carTravel);
            if (!deleted) {
                Log.d(TAG + TABLE_CAR_PASSENGERS, "No car travel passengers were deleted.");
            }

            for (int i = 0; i < passengers.size(); i++) {
                long err = db.insert(TABLE_CAR_PASSENGERS, null, getCarPassengerContent(carTravel, i));

                // Ensures an error is reported despite following successful attempts
                if (error != -1) {
                    error = err;
                }
            }
        }

        return error != -1;
    }

    /**
     * Deletes all car travel passengers prior to insertion after update.
     * @param db     The initialized database in the insertCarPassengers-method.
     * @param carTravel The car travel for which to delete passengers.
     * @return False if no rows were effected, true otherwise.
     */
    private boolean deleteCarPassengers(SQLiteDatabase db, CarTravel carTravel) {
        int effected = db.delete(TABLE_CAR_PASSENGERS, KEY_CAR_PASSENGER_TRAVEL_ID + " = ?",
                new String[]{String.valueOf(carTravel.getId())});

        return effected != 0;
    }

    /**
     * Returns all car travel passenger content attributes for handling.
     * @param carTravel The car travel to get passenger content from.
     * @param i The index of the current car travel.
     * @return A mapping of content values from key to attribute.
     */
    private ContentValues getCarPassengerContent(CarTravel carTravel, int i) {
        ContentValues values = new ContentValues();
        values.put(KEY_CAR_PASSENGER_TRAVEL_ID, carTravel.getId());
        values.put(KEY_CAR_PASSENGER_NAME, carTravel.getPassengers().get(i));

        return values;
    }

    /**
     * Returns a list from a table which holds referenced attributes.
     * @param db The database initialized in public method.
     * @param id The ID of the entity which is referenced.
     * @param table The table from which to get values.
     * @param key The key of the related table of the entity which to compare ID for.
     * @param where A where clause used to specify which column to get values from.
     * @return An array list containing the values of the entity attribute.
     */
    private List<String> getTableList(SQLiteDatabase db, long id, String table, String key,
                                      String where) {
        Cursor cursor = db.rawQuery("SELECT " + where + " FROM " + table + WHERE +
                key + " = ?", new String[]{String.valueOf(id)});

        List<String> list = new ArrayList<>();
        if(cursor != null && cursor.moveToFirst()) {
            do {
                list.add(cursor.getString(cursor.getColumnIndex(where)));
            } while (cursor.moveToNext());
            cursor.close();
        }
        return list;
    }

    /**
     * Returns the active travel, the first travel of the first user.
     * @return The active travel found from first user.
     */
    public Travel getActiveTravel() {
        return getAllTravels(getAllUsers().get(0).getSsn()).get(0);
    }

    /**
     * Checks if the first user has a travel stored.
     * @return True if first user travels was greater than 0, false otherwise.
     */
    public boolean hasActiveTravel() {
        return hasCreatedUser() && getAllTravels(getAllUsers().get(0).getSsn()).size() > 0;
    }

    /**
     * Checks if there has been created a user.
     * @return True if the user table is greater than 0, false otherwise.
     */
    public boolean hasCreatedUser() {
        return getAllUsers().size() > 0;
    }

    /**
     * Returns next available ID (i.e. max(id) + 1) for a travel.
     * @param fk The foreign key of the connected user.
     * @return An int representing the next available ID.
     */
    public int getNextTravelId(long fk) {
        List<Travel> travels = getAllTravels(fk);
        if(!travels.isEmpty()) {
            return travels.get(travels.size() - 1).getId() + 1;
        } else {
           return 1;
        }
    }

    /**
     * Returns next available ID (i.e. max(id) + 1) for a receipt.
     * @param fk The foreign key of the connected travel.
     * @return An int representing the next available ID.
     */
    public int getNextReceiptId(int fk) {
        List<Receipt> receipts = getAllReceipts(fk);
        if(!receipts.isEmpty()) {
            return receipts.get(receipts.size() - 1).getId() + 1;
        } else {
            return 1;
        }
    }

    /**
     * Returns next available ID (i.e. max(id) + 1) for a car travel.
     * @param fk The foreign key of the connected travel.
     * @return An int representing the next available ID.
     */
    public int getNextCarTravelId(int fk) {
        List<CarTravel> carTravels = getAllCarTravels(fk);
        if(!carTravels.isEmpty()) {
            return carTravels.get(carTravels.size() - 1).getId() + 1;
        } else {
            return 1;
        }
    }
}
