package no.uib.jmi053.tefapplication.io;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.util.Log;

import no.uib.jmi053.tefapplication.activities.superclass.TEFActivity;
import no.uib.jmi053.tefapplication.utilities.CameraUtils;
import no.uib.jmi053.tefapplication.utilities.constants.DebugConstants;
import no.uib.jmi053.tefapplication.utilities.constants.IntentConstants;
import no.uib.jmi053.tefapplication.utilities.graphics.CVUtil;

/**
 * Created by JoaT Development on 04.10.2015.
 *
 * Handles photos after being taken from camera activity.
 */
public class PhotoHandler implements Camera.PictureCallback {

    // Log constant
    private final String TAG = DebugConstants.getErrorTag(getClass());

    private Activity activity;
    private Intent inputIntent;

    public PhotoHandler(Activity activity, Intent intent) {
        this.activity = activity;
        this.inputIntent = intent;
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        Log.i(TAG, "Picture taken.");

        if(data != null) {
            Log.i(TAG, "Found picture data.");
            Bitmap image = BitmapFactory.decodeByteArray(data, 0, data.length);
            Log.i(TAG, "Decoded byte array.");

            // Crops and transforms image
            try {
                CVUtil.cropAndTransform(image);
            } catch (Exception e) {
                e.printStackTrace();
            }

            int travelId = inputIntent.getIntExtra(IntentConstants.TRAVEL_ID, -1);
            int receiptId = inputIntent.getIntExtra(IntentConstants.ITEM_ID, -1);

            // Formats the travel name (e.g. travel01)
            String travelName = null;
            if (travelId != -1) {
                travelName = "travel" + String.format("%02d", travelId);
            } else {
                Log.e(TAG, DebugConstants.ERROR_NO_TRAVEL);
            }

            // Formats the receipt name (e.g. receipt01)
            String receiptName = null;
            if (receiptId != -1) {
                receiptName = "receipt" + String.format("%02d", receiptId);
            } else {
                Log.e(TAG, DebugConstants.ERROR_NO_RECEIPT);
            }

            // Concatenates both strings into an image name format (e.g. travel01_receipt01.jpg)
            String name = travelName + "_" + receiptName + ".jpg";

            Log.i(TAG, "Storing image to internal storage.");
            // Save the image and store and return the path to ReceiptActivity
            String path = CameraUtils.saveToInternalStorage(activity,
                    CameraUtils.rotate(image, 90), name);
            Log.i(TAG, "Image stored to: " + path);


            // Sets the image path to the camera activity result data
            Intent resultIntent = new Intent();
            resultIntent.putExtra(IntentConstants.IMAGE_PATH, path);
            activity.setResult(TEFActivity.RESULT_OK, resultIntent);

            activity.finish();
        }
    }
}
