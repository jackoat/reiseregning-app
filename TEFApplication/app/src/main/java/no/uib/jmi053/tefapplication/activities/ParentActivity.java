package no.uib.jmi053.tefapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.inputs.ReceiptActivity;
import no.uib.jmi053.tefapplication.activities.inputs.TravelActivity;
import no.uib.jmi053.tefapplication.activities.inputs.UserActivity;
import no.uib.jmi053.tefapplication.activities.superclass.TEFActivity;
import no.uib.jmi053.tefapplication.io.DatabaseHandler;
import no.uib.jmi053.tefapplication.utilities.constants.IntentConstants;
import no.uib.jmi053.tefapplication.widgets.FontButton;

/**
 * Created by JoaT Development 01.09.15.
 *
 * Top level activity that redirects to different types of travel expenses.
 */
public class ParentActivity extends TEFActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkForUser();
        checkForTravel();
    }

    /**
     * Redirects to create user if none is stored in database.
     */
    private void checkForUser() {
        if(!db.hasCreatedUser()){
            startActivity(new Intent(this, UserActivity.class));
        }
    }

    /**
     * Checks the database for an active travel and labels the travel button accordingly.
     */
    private void checkForTravel() {
        FontButton createTravel = (FontButton) findViewById(R.id.new_travel_button);
        if(db.hasActiveTravel()) {
            createTravel.setText(R.string.travel_load_button);
        } else {
            createTravel.setText(R.string.travel_create_button);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.new_travel_button:
                if(db.hasActiveTravel()) {
                    startActivity(new Intent(this, TravelMenuActivity.class));
                } else {
                    startActivity(new Intent(this, TravelActivity.class));
                }
                break;

            case R.id.new_reimbursement_button:
                Intent intent = new Intent(this, ReceiptActivity.class);
                intent.putExtra(IntentConstants.TRAVEL_ID, DatabaseHandler.REIMBURSEMENT_ID);
                intent.putExtra(IntentConstants.REIMBURSEMENT_FLAG, true);
                startActivity(intent);
                break;
        }
    }
}
