package no.uib.jmi053.tefapplication.activities.inputs;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.superclass.InputActivity;
import no.uib.jmi053.tefapplication.adapters.CursorAdapter;
import no.uib.jmi053.tefapplication.entities.Receipt;
import no.uib.jmi053.tefapplication.entities.Travel;
import no.uib.jmi053.tefapplication.io.DatabaseHandler;
import no.uib.jmi053.tefapplication.io.ExcelHandler;
import no.uib.jmi053.tefapplication.io.MailSender;
import no.uib.jmi053.tefapplication.utilities.CameraUtils;
import no.uib.jmi053.tefapplication.utilities.constants.IntentConstants;
import no.uib.jmi053.tefapplication.utilities.constants.PopupConstants;
import no.uib.jmi053.tefapplication.utilities.parsers.DateParserUtil;
import no.uib.jmi053.tefapplication.utilities.parsers.TimeParserUtil;
import no.uib.jmi053.tefapplication.utilities.popups.DialogUtil;
import no.uib.jmi053.tefapplication.utilities.popups.ToastUtil;
import no.uib.jmi053.tefapplication.widgets.FontEdit;

/**
 * Created by JoaT Development 27.09.15.
 *
 * Activity for sending in a travel expense form.
 */
public class SendActivity extends InputActivity implements View.OnClickListener {

    // Excel handler
    ExcelHandler excelHandler;

    // Input Widgets
    TextView header;
    DatePicker endDate;
    TextView dateText;
    TimePicker endTime;
    TextView timeText;
    FontEdit receiverEmail;
    FontEdit senderEmail;
    FontEdit password;
    Spinner account;

    //Strings
    String startDateString;
    String endDateString;
    String receiverEmailString;
    String senderEmailString;
    String travelRoute;
    String formPath;

    // Account information
    List<String> accountNames;
    HashMap<String, Long> accounts;
    String accountName;

    // Active travel
    Travel travel;

    //Mailsender
    MailSender sender;

    // Image lists
    List<String> imagePathList;

    // Reimbursement flag
    Boolean reimbursement;
    Boolean showToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);

        // Finds the header
        header = (TextView) findViewById(R.id.header);

        // Finds all input widgets (and labels for visibility handling)
        endDate = (DatePicker) findViewById(R.id.send_end_date_input);
        dateText = (TextView) findViewById(R.id.send_end_date_label);
        endTime = (TimePicker) findViewById(R.id.send_end_time_input);
        timeText = (TextView) findViewById(R.id.send_end_time_label);
        receiverEmail = (FontEdit) findViewById(R.id.send_email_input);
        senderEmail = (FontEdit) findViewById(R.id.send_copy_email_input);
        password = (FontEdit) findViewById(R.id.send_password_input);
        account = (Spinner) findViewById(R.id.send_account_input);
        endTime.setIs24HourView(true);

        reimbursement = getIntent().getBooleanExtra(IntentConstants.REIMBURSEMENT_FLAG, false);

        // Sets travel based on flag
        if(reimbursement) {
            travel = db.getTravel(DatabaseHandler.REIMBURSEMENT_ID);
        }  else {
            getActiveTravel();
        }

        // Sets the toast flag to allow toasts to be shown
        showToast = true;

        // Initializes the image lists
        imagePathList = new ArrayList<>();
        for (Receipt r : db.getAllReceipts(travel.getId())) {
            imagePathList.add(r.getImagePath());
        }

        updateInformation();

        inputStringsAtStart = getInputStrings();
    }

    @Override
    protected void onResume(){
        super.onResume();

        updateInformation();
    }

    @Override
    public void onBackPressed() {
        if (reimbursement) {
            // Customized dialog for handling reimbursements
            final Dialog dialog = new Dialog(this);

            // Creates the negative button listener
            View.OnClickListener negativeListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(PopupConstants.DIALOG_INFORMATION_LOSS,
                            "User denied loss of information.");
                    v.setClickable(true);
                    dialog.dismiss();
                }
            };

            // Creates the positive button listener
            View.OnClickListener positiveListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(PopupConstants.DIALOG_INFORMATION_LOSS,
                            "User confirmed loss of information.");
                    v.setClickable(true);
                    db.deleteTravel(DatabaseHandler.REIMBURSEMENT_ID);
                    finish();
                    dialog.dismiss();
                }
            };

            // Customizes the created dialog
            DialogUtil.createAlertDialog(
                    this, dialog, getString(R.string.dialog_information_loss_message),
                    getString(R.string.dialog_information_loss_negative_button),
                    getString(R.string.dialog_information_loss_positive_button),
                    negativeListener, positiveListener)
                    .show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected String getItemName() {
        return null;
    }

    @Override
    protected List<FontEdit> createEditFieldList() {
        return Arrays.asList(receiverEmail, senderEmail, password);
    }

    @Override
    protected List<String> getInputStrings() {
        return Arrays.asList(DateParserUtil.getDateString(endDate),
                TimeParserUtil.getTimeString(endTime),
                receiverEmail.getText().toString(),
                senderEmail.getText().toString(),
                password.getText().toString(),
                account.getSelectedItem().toString());
    }

    /**
     * Fills in information in input fields.
     */
    private void updateInformation(){
        if(reimbursement) {
            // Sets new title for reimbursement
            header.setText(getResources().getString(R.string.send_reimbursement_header));

            // Hides travel-specific fields
            endDate.setVisibility(View.GONE);
            dateText.setVisibility(View.GONE);
            endTime.setVisibility(View.GONE);
            timeText.setVisibility(View.GONE);
        }

        // Sets emails based on the found user
        receiverEmail.setText(db.getAllUsers().get(0).getEmployerEmail());
        senderEmail.setText(db.getAllUsers().get(0).getEmail());

        // Saves all accounts in a hashMap
        accounts = db.getAllUsers().get(0).getAccounts();
        accountNames = new ArrayList<>(accounts.keySet());
        CursorAdapter accountAdapter = new CursorAdapter(this, accountNames);
        account.setAdapter(accountAdapter);
        account = (Spinner)findViewById(R.id.send_account_input);
        accountName = account.getSelectedItem().toString();
    }

    /**
     * Sets the active travel is one is found, finishes the activity otherwise.
     */
    private void getActiveTravel() {
        if(db.hasActiveTravel()) {
            travel = db.getActiveTravel();
        } else {
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_travel_final_button:
                ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                showToast = true;

                if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                        connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                    try {
                        if (!hasUnfilledFields()) {
                            // Parses the date and time from pickers into strings
                            endDateString = "";
                            String endTimeString = "";
                            if (!reimbursement) {
                                endDateString = DateParserUtil.getDateString(endDate);
                                endTimeString = TimeParserUtil.getTimeString(endTime);
                            }


                            // Generates an excel handler with given end date and time and chosen account
                            excelHandler = new ExcelHandler(this, db, travel, endDateString, endTimeString,
                                    accounts.get(accountName));

                            // The path of the generated excel document
                            formPath = excelHandler.modifyExcel();

                            // Sets strings for the SendTask
                            if (!reimbursement) {
                                startDateString = DateParserUtil.formatDate(travel.getStartDate());
                            } else {
                                // Date of first (and only) receipt
                                startDateString = DateParserUtil.formatDate(
                                        db.getAllReceipts(travel.getId()).get(0).getDate());
                            }

                            senderEmailString = senderEmail.getText().toString();
                            receiverEmailString = receiverEmail.getText().toString();
                            travelRoute = db.getTravel(travel.getId()).getRoute();

                            // Creates email sender
                            sender = new MailSender(
                                    senderEmailString, password.getText().toString());

                            // Tries to send mail
                            AsyncSendTask sendTask = new AsyncSendTask();
                            sendTask.execute();
                            if (sendTask.get()) {
                                // Deletes and finishes if successful
                                db.deleteTravel(travel.getId());
                                finish();
                            } else {
                                // Otherwise show error message
                                CameraUtils.deleteImageFromStorage(formPath); // This is an excel document, not an image
                                ToastUtil.createWarningToast(SendActivity.this, getResources().
                                        getString(R.string.send_activity_send_failed_warning)).
                                        show();
                            }
                        } else {
                            if (showToast) {
                                showUnfilledFieldsToast();
                                showToast = false;
                            }
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
                else {
                    if (showToast) {
                        ToastUtil.createWarningToast(this, getResources().getString(R.string.send_activity_no_internet_warning)).show();
                        showToast = false;
                    }
                }
        }
    }

    /**
     * Created by Joat development on 07.10.2015.
     *
     * Object for sending an email in a separate thread
     */
    private class AsyncSendTask extends AsyncTask<Void, Void, Boolean> {
        /**
         * Creates a SendTask
         */
        private AsyncSendTask(){
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Boolean success = false;

            try {
                //Generate email subject, body and form attachment
                if (reimbursement) {
                    sender.setSubject("Reimbursement for " + senderEmailString + " " + startDateString);
                    sender.setBody("This mail has been sent from a Travel Expense Form Application. " +
                            "It contains a single expense for " + senderEmailString + " " + startDateString);
                    sender.addAttachment(formPath, senderEmailString + " " + startDateString + ".xls");
                } else {
                    sender.setSubject("Travel expense form for " + senderEmailString + ": " + startDateString + " - "
                            + endDateString);
                    sender.setBody("This mail has been sent from a Travel Expense Form Application. " +
                            "It contains a travel for " + senderEmailString + " with route " + travelRoute +
                            ": " + startDateString + " - " + endDateString);
                    sender.addAttachment(formPath, senderEmailString + ": " + startDateString + " - " + endDateString +
                            ".xls");
                }
                //Generate image attachments
                for (int i = 0; i < imagePathList.size(); i++) {
                    sender.addAttachment(imagePathList.get(i), findNameFromPath(imagePathList.get(i)));
                }

                //Set to and from addresses
                sender.setFrom(senderEmailString);
                String[] recipients = {receiverEmailString, senderEmailString};
                sender.setTo(recipients);

                //Send the email
                success = sender.send();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return success;
        }

        /**
         * Finds the name of an image file given its path
         *
         * @param path the path string
         * @return the file name
         */
        private String findNameFromPath(String path) {
            String[] split = path.split("images/");
            return split[1];
        }
    }

}
