package no.uib.jmi053.tefapplication.activities.inputs;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.TravelMenuActivity;
import no.uib.jmi053.tefapplication.activities.superclass.InputActivity;
import no.uib.jmi053.tefapplication.entities.Travel;
import no.uib.jmi053.tefapplication.utilities.constants.DebugConstants;
import no.uib.jmi053.tefapplication.utilities.constants.IntentConstants;
import no.uib.jmi053.tefapplication.utilities.constants.PopupConstants;
import no.uib.jmi053.tefapplication.utilities.graphics.GraphicsUtil;
import no.uib.jmi053.tefapplication.utilities.parsers.DateParserUtil;
import no.uib.jmi053.tefapplication.utilities.parsers.TimeParserUtil;
import no.uib.jmi053.tefapplication.utilities.popups.ToastUtil;
import no.uib.jmi053.tefapplication.widgets.FontButton;
import no.uib.jmi053.tefapplication.widgets.FontEdit;
import no.uib.jmi053.tefapplication.widgets.FontView;

/**
 * Created by JoaT Development 16.09.15.
 *
 * Activity for adding or editing information for a travel.
 */
public class TravelActivity extends InputActivity implements View.OnClickListener {

    // Input widgets
    FontEdit route;
    FontEdit purpose;
    DatePicker date;
    TimePicker time;
    ToggleButton customerToggle;
    FontView customerNameLabel;
    FontEdit customerName;
    FontView customerNumberLabel;
    FontEdit customerNumber;
    ToggleButton prepaidAmountToggle;
    FontView prepaidAmountLabel;
    FontEdit prepaidAmount;

    // Event and comment lists
    List<String> events;
    List<String> comments;

    // Flag
    Boolean beingEdited;
    Boolean showToast;

    // The ID of the travel being edited
    int id;

    // The create and save button
    FontButton button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_travel);

        // Finds and sets icon to the create button
        button = (FontButton) findViewById(R.id.new_travel_button);
        GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_create);

        // Finds all input widgets and expandable lists
        route = (FontEdit) findViewById(R.id.travel_route_input);
        purpose = (FontEdit) findViewById(R.id.travel_purpose_input);
        date = (DatePicker) findViewById(R.id.travel_date_input);
        time = (TimePicker) findViewById(R.id.travel_time_input);
        time.setIs24HourView(true);
        customerToggle = (ToggleButton) findViewById(R.id.travel_customer_toggle);
        customerNameLabel = (FontView) findViewById(R.id.travel_customer_name_label);
        customerName = (FontEdit) findViewById(R.id.travel_customer_name_input);
        customerNumberLabel = (FontView) findViewById(R.id.travel_customer_number_label);
        customerNumber = (FontEdit) findViewById(R.id.travel_customer_number_input);
        prepaidAmountToggle = (ToggleButton) findViewById(R.id.travel_prepaid_amount_toggle);
        prepaidAmountLabel = (FontView) findViewById(R.id.travel_prepaid_amount_label);
        prepaidAmount = (FontEdit) findViewById(R.id.travel_prepaid_amount_input);

        // Sets on click listener for the customer toggle
        customerToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(customerToggle.isChecked()) {
                    customerNameLabel.setVisibility(View.VISIBLE);
                    customerName.setVisibility(View.VISIBLE);
                    customerNumberLabel.setVisibility(View.VISIBLE);
                    customerNumber.setVisibility(View.VISIBLE);
                } else {
                    customerNameLabel.setVisibility(View.GONE);
                    customerName.setVisibility(View.GONE);
                    customerNumberLabel.setVisibility(View.GONE);
                    customerNumber.setVisibility(View.GONE);
                }
            }
        });

        // Sets on click listener for the prepaid amount toggle
        prepaidAmountToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(prepaidAmountToggle.isChecked()) {
                    prepaidAmountLabel.setVisibility(View.VISIBLE);
                    prepaidAmount.setVisibility(View.VISIBLE);
                } else {
                    prepaidAmountLabel.setVisibility(View.GONE);
                    prepaidAmount.setVisibility(View.GONE);
                }
            }
        });

        // Finds the ID of the travel
        id = getIntent().getIntExtra(IntentConstants.TRAVEL_ID, -1);

        // Checks if the travel is being edited
        beingEdited = getIntent().getBooleanExtra(IntentConstants.EDIT_FLAG, false);

        // Sets the toast flag to allow toasts to be shown
        showToast = true;

        // If being edited then the event list is set from active travel
        if(beingEdited) {
            events = db.getTravel(id).getEvents();
            comments = db.getTravel(id).getComments();
        }

        updateInformation();

        inputStringsAtStart = getInputStrings();
    }

    @Override
    protected String getItemName() {
        return "travel";
    }

    @Override
    protected List<FontEdit> createEditFieldList() {
        List<FontEdit> editFields = new ArrayList<>();
        editFields.add(route);
        editFields.add(purpose);
        if(customerToggle.isChecked()) {
            editFields.add(customerName);
            editFields.add(customerNumber);
        }
        if(prepaidAmountToggle.isChecked()) {
            editFields.add(prepaidAmount);
        }
        return editFields;
    }

    @Override
    protected List<String> getInputStrings() {
        List<String> tempStrings = Arrays.asList(
                route.getText().toString(),
                purpose.getText().toString(),
                DateParserUtil.getDateString(date),
                TimeParserUtil.getTimeString(time));

        // Create new list to remove fixed size
        List<String> inputStrings = new ArrayList<>();
        inputStrings.addAll(tempStrings);

        if(customerToggle.isChecked()) {
            inputStrings.add(customerName.getText().toString());
            inputStrings.add(customerNumber.getText().toString());
        }
        if(prepaidAmountToggle.isChecked()) {
            inputStrings.add(prepaidAmountToggle.getText().toString());
        }

        return inputStrings;
    }

    /**
     * Ensures that information is up to date on editing.
     */
    private void updateInformation() {
        if(beingEdited) {
            if (id != -1) {
                // Finds the travel with the given ID
                Travel travel = db.getTravel(id);

                // Sets new titles for editing
                FontView header = (FontView) findViewById(R.id.header);
                header.setText(R.string.edit_travel_header);
                setTitle(R.string.title_activity_edit_travel);

                // Sets new information based on found travel
                route.setText(travel.getRoute());
                purpose.setText(travel.getPurpose());
                DateParserUtil.setDate(date, travel.getStartDate());
                TimeParserUtil.setTime(time, travel.getStartTime());
                if(travel.isPaidByCustomer()) {
                    customerName.setText(travel.getCustomerName());
                    customerNumber.setText(String.valueOf(travel.getCustomerId()));
                    customerToggle.setChecked(true);
                    customerToggle.callOnClick();
                }
                if(travel.hasPrepaidAmount()) {
                    prepaidAmount.setText(String.valueOf(travel.getPrepaidAmount()));
                    prepaidAmountToggle.setChecked(true);
                    prepaidAmountToggle.callOnClick();
                }

                // Changes create button text and icon to save
                button.setText(R.string.button_save);
                GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_save);

                // Makes delete button visible
                FontButton delete = (FontButton) findViewById(R.id.delete_travel_button);
                delete.setVisibility(FontButton.VISIBLE);

            } else {
                Log.d(TAG, DebugConstants.ERROR_NO_TRAVEL);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.new_travel_button:
                if(!hasUnfilledFields()) {
                    showToast = false;
                    if(!customerToggle.isChecked() ||
                            !customerName.getText().toString().equals("null")) {
                        if(!customerToggle.isChecked() ||
                                Integer.parseInt(customerNumber.getText().toString()) > 0) {
                            if (!prepaidAmountToggle.isChecked() ||
                                    Double.parseDouble(prepaidAmount.getText().toString()) > 0) {
                                Travel newTravel = getEditedInfo();
                                if (!beingEdited) {
                                    db.insertTravel(newTravel);
                                    startActivity(new Intent(this, TravelMenuActivity.class));
                                } else {
                                    if (id != -1) {
                                        newTravel.setId(id);
                                        db.updateTravel(newTravel);
                                    } else {
                                        Log.d(TAG, DebugConstants.ERROR_NO_TRAVEL);
                                    }
                                }
                                finish();
                            } else {
                                if (showToast){
                                    ToastUtil.createWarningToast(this,
                                            getString(R.string.toast_warning_invalid_prepaid_amount)).
                                            show();
                                    showToast = false;
                                }
                            }
                        }else {
                            if (showToast){
                                ToastUtil.createWarningToast(this, getString(
                                        R.string.toast_warning_invalid_customer_number))
                                        .show();
                                showToast = false;
                            }
                        }
                    } else {
                        if (showToast){
                            ToastUtil.createWarningToast(this, getString(
                                    R.string.toast_warning_invalid_customer_name))
                                    .show();
                            showToast = false;
                        }
                    }
                } else {
                    if (showToast){
                        showUnfilledFieldsToast();
                        showToast = false;
                    }
                }
                break;
            case R.id.delete_travel_button:
                if(id != -1) {
                    final Dialog dialog = new Dialog(this);
                    // Creates the positive button listener
                    View.OnClickListener positiveListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.i(PopupConstants.DIALOG_DELETION,
                                    "User confirmed " + getItemName() + " deletion.");
                            v.setClickable(true);
                            db.deleteTravel(id);
                            finish();
                            dialog.dismiss();
                        }
                    };
                    showDeleteConfirmDialog(dialog, positiveListener);
                } else {
                    Log.d(TAG, DebugConstants.ERROR_NO_TRAVEL);
                }
                break;
        }
    }

    /**
     * Formats and returns information for the created or edited travel.
     * @return The formatted travel based on input fields.
     */
    private Travel getEditedInfo() {
        long fk =  db.getAllUsers().get(0).getSsn();

        int travelId = id;
        if(travelId == -1) {
            travelId = db.getNextTravelId(fk);
        }

        Travel travel = new Travel(travelId, fk, route.getText().toString(),
                purpose.getText().toString(),
                DateParserUtil.getDateString(date), TimeParserUtil.getTimeString(time));
        if(customerToggle.isChecked()) {
            travel.setCustomerName(customerName.getText().toString());
            travel.setCustomerId(Integer.parseInt(customerNumber.getText().toString()));
        }
        if(prepaidAmountToggle.isChecked()) {
            travel.setPrepaidAmount(Double.parseDouble(prepaidAmount.getText().toString()));
        }
        if(beingEdited) {
            travel.setEvents(events);
            travel.setComments(comments);
        }

        return travel;
    }

    /**
     * Creates test values for input fields which are undeclared. Method for testing.
     */
    private void createTestInput() {
        if(route.getText().toString().equals("")) {
            route.setText("Bergen-London-Bergen");
        }
        if(purpose.getText().toString().equals("")) {
            purpose.setText("Conference meeting");
        }
    }
}
