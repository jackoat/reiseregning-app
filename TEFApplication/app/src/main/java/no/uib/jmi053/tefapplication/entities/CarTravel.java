package no.uib.jmi053.tefapplication.entities;

import java.util.List;

/**
 * Created by JoaT Development on 31.08.2015.
 *
 * Holds information about a car travel.
 */
public class CarTravel {

    private int id;
    private int fk;
    private String date;
    private String start;
    private String end;
    private double kilometers;
    private List<String> passengers;
    /**
     * Creates a new CarTravel from an ID, connected to a Travel by a foreign key.
     * @param id The ID generated at database insertion.
     * @param fk The ID of the connected travel.
     * @param date The date of the car travel.
     * @param start The start location of the car travel.
     * @param end The end location of the car travel.
     * @param kilometers The distance driven in km.
     */
    public CarTravel(int id, int fk, String date, String start, String end, double kilometers) {
        this.id = id;
        this.fk = fk;
        this.date = date;
        this.start = start;
        this.end = end;
        this.kilometers = kilometers;
    }

    /**
     * Returns the car travel ID.
     * @return An integer representing the ID automatically generated at input.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets a new ID for a car travel. Called when a travel is given an ID by the external
     * database.
     * @param id The new ID to be set.
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Returns the car travel foreign key.
     * @return An integer representing the ID of the connected travel.
     */
    public int getFk() {
        return fk;
    }

    /**
     * Sets a new foreign key for a car travel. Called when a travel is given an ID by the
     * external database.
     * @param fk The new foreign key to be set.
     */
    public void setFk(int fk) {
        this.fk = fk;
    }

    /**
     * Returns the car travel date.
     * @return A string representing the date of the car travel.
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets a new date for a car travel.
     * @param date The new date to be set of format "dd.mm.yyyy".
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Returns the start location of a car travel.
     * @return A string representing the start GPS-location of the car travel.
     */
    public String getStart() {
        return start;
    }

    /**
     * Sets a new start location for a car travel.
     * @param start The new start location to be set.
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     * Returns the end location of a car travel.
     * @return A string representing the end GPS-location of the car travel.
     */
    public String getEnd() {
        return end;
    }

    /**
     * Sets a new end location for a car travel.
     * @param end The new end location to be set.
     */
    public void setEnd(String end) {
        this.end = end;
    }

    /**
     * Returns the distance in kilometers of the travel.
     * @return A double containing the distance in kilometers of format "xx,x".
     */
    public double getKilometers() {
        return kilometers;
    }

    /**
     * Sets a new distance in kilometers for a car travel.
     * @param kilometers The new distance in kilometers to be set of format "xx,x".
     */
    public void setKilometers(double kilometers) {
        this.kilometers = kilometers;
    }

    /**
     * Returns the names of passengers of the car travel.
     * @return A list representing the name of passengers of the car travel.
     */
    public List<String> getPassengers() {
        return passengers;
    }

    /**
     * Sets a new list of passengers for a car travel.
     * @param passengers The new list of passengers to be set.
     */
    public void setPassengers(List<String> passengers) {
        this.passengers = passengers;
    }

    /**
     * Returns the number of passengers.
     * @return An integer representing the number of car passengers.
     */
    public int getPassengerSize() {
        return passengers.size();
    }

}
