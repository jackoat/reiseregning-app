package no.uib.jmi053.tefapplication.activities.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.inputs.UserActivity;
import no.uib.jmi053.tefapplication.activities.superclass.TEFActivity;

/**
 * Created by JoaT Development 16.09.15.
 *
 * Activity for editing different settings in the application.
 */
public class SettingsActivity extends TEFActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id.personal_info:
                Intent personalIntent = new Intent (this, UserActivity.class);
                startActivity(personalIntent);
                break;
            case R.id.about:
                Intent aboutIntent = new Intent (this, AboutActivity.class);
                startActivity(aboutIntent);
                break;
        }
    }
}
