package no.uib.jmi053.tefapplication.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import no.uib.jmi053.tefapplication.R;

/**
 * Created by JoaT Development on 27.08.2015.
 *
 * Custom Button with a custom font from assets.
 */
public class FontButton extends Button {

    /**
     * Creates a FontButton with a predefined font.
     * @param context The activity to get resources from.
     * @param attrs The attribute set of the connected layout.
     */
    public FontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Typeface.createFromAsset(
                getContext().getAssets(), getResources().getString(R.string.font_roboto_regular)));
    }
}
