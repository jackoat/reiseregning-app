package no.uib.jmi053.tefapplication.activities.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.Arrays;
import java.util.List;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.superclass.InputActivity;
import no.uib.jmi053.tefapplication.utilities.constants.IntentConstants;
import no.uib.jmi053.tefapplication.utilities.constants.PopupConstants;
import no.uib.jmi053.tefapplication.utilities.graphics.GraphicsUtil;
import no.uib.jmi053.tefapplication.utilities.popups.ToastUtil;
import no.uib.jmi053.tefapplication.widgets.FontButton;
import no.uib.jmi053.tefapplication.widgets.FontEdit;
import no.uib.jmi053.tefapplication.widgets.FontView;

/**
 * Created by JoaT Development 24.09.15.
 *
 * Activity for adding or editing information for a bank account.
 */
public class AccountActivity extends InputActivity implements View.OnClickListener {

    // Input widgets
    FontEdit name;
    FontEdit number;

    // Flag
    boolean beingEdited;
    boolean showToast;

    // Holds old name for account being edited
    String accountName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);

        // Finds and sets icon to the create button
        FontButton button = (FontButton) findViewById(R.id.account_save_button);
        GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_create);

        // Finds input widgets
        name = (FontEdit) findViewById(R.id.account_name_input);
        number = (FontEdit) findViewById(R.id.account_number_input);

        // Checks if account is being edited
        beingEdited = getIntent().getBooleanExtra(IntentConstants.EDIT_FLAG, false);

        // Sets the toast flag to allow toasts to be shown
        showToast = true;

        if(beingEdited) {
            // Finds the name of the account
            accountName = getIntent().getStringExtra(IntentConstants.ITEM_NAME);

            // Sets new titles for editing
            FontView header = (FontView) findViewById(R.id.header);
            header.setText(R.string.edit_account_header);
            setTitle(R.string.title_activity_edit_account);

            // Sets name to input field
            name.setText(accountName);

            // Finds the account number
            long accountNumber = getIntent().getLongExtra(IntentConstants.ITEM_NUMBER, -1);

            // Sets the number of the account
            if(accountNumber != -1) {
                number.setText(String.valueOf(accountNumber));
            } else {
                Log.d(TAG, "Account number not found.");
            }

            // Changes create button text and icon to save
            button.setText(R.string.button_save);
            GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_save);

            // Makes delete button visible
            FontButton delete = (FontButton) findViewById(R.id.account_delete_button);
            delete.setVisibility(FontButton.VISIBLE);
        }

        inputStringsAtStart = getInputStrings();
    }

    @Override
    protected String getItemName() {
        return "account";
    }

    @Override
    protected List<FontEdit> createEditFieldList() {
        return Arrays.asList(name, number);
    }

    @Override
    protected List<String> getInputStrings() {
        return Arrays.asList(name.getText().toString(), number.getText().toString());
    }

    @Override
    public void onClick(View v) {
        final Intent result = new Intent();

        if(beingEdited) {
            // The original account number for deleting
            result.putExtra(IntentConstants.ITEM_ID, accountName);
        }

        switch(v.getId()) {
            case R.id.account_save_button:
                if(!hasUnfilledFields()) {
                    showToast = true;
                    if(number.getText().toString().length() == 11) {
                        result.putExtra(IntentConstants.ITEM_NAME, name.getText().toString());
                        result.putExtra(IntentConstants.ITEM_NUMBER,
                                Long.parseLong(number.getText().toString()));
                        if (beingEdited) {
                            setResult(IntentConstants.RESULT_EDIT, result);
                        } else {
                            setResult(IntentConstants.RESULT_CREATE, result);
                        }

                        finish();
                    } else {
                        if (showToast){
                            ToastUtil.createWarningToast(this, getString(
                                    R.string.toast_warning_invalid_account_number_length))
                                    .show();
                            showToast = false;
                        }
                    }
                } else {
                    if (showToast) {
                        showUnfilledFieldsToast();
                        showToast = false;
                    }
                }
                break;
            case R.id.account_delete_button:
                final Dialog dialog = new Dialog(this);

                // Creates the positive button listener
                View.OnClickListener positiveListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(PopupConstants.DIALOG_DELETION,
                                "User confirmed " + getItemName() + " deletion.");
                        v.setClickable(true);
                        setResult(IntentConstants.RESULT_DELETE, result);
                        finish();
                        dialog.dismiss();
                    }
                };
                showDeleteConfirmDialog(dialog, positiveListener);
                break;
        }


    }
}
