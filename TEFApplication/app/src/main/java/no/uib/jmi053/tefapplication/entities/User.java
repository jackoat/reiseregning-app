package no.uib.jmi053.tefapplication.entities;

import java.util.HashMap;

/**
 * Created by JoaT Development on 16/09/2015.
 *
 * Holds information about a user.
 */
public class User {

    private long ssn;
    private String name;
    private String address;
    private String email;
    private String employerEmail;
    private HashMap<String, Long> accounts;
    private String company;
    private int department;

    /**
     * Creates a new User from a SSN.
     * @param ssn The social security of the user.
     * @param name The name of the user.
     * @param address The address of the user.
     * @param email The email of the user.
     * @param company The company the user is employed by.
     * @param department The department which the user works for.
     */
    public User(long ssn, String name, String address, String email, String employerEmail,
                String company, int department) {
        this.ssn = ssn;
        this.name = name;
        this.address = address;
        this.email = email;
        this.employerEmail = employerEmail;
        this.company = company;
        this.department = department;
    }

    public long getSsn() {
        return ssn;
    }

    public void setSsn(long ssn) {
        this.ssn = ssn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployerEmail() {return employerEmail;}

    public void setEmployerEmail(String employerEmail) { this.employerEmail = employerEmail;}

    public HashMap<String, Long> getAccounts() {
        return accounts;
    }

    public void setAccounts(HashMap<String, Long> accounts) {
        this.accounts = accounts;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getDepartment() {
        return department;
    }

    public void setDepartment(int department) {
        this.department = department;
    }
}
