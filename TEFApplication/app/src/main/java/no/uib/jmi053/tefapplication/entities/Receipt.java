package no.uib.jmi053.tefapplication.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JoaT Development on 28.08.2015.
 *
 * Holds information about a receipt
 */
public class Receipt {

    private int id;
    private int fk;
    private String type;
    private String name;
    private String date;
    private double total;
    private String currency;
    private String imagePath;
    private String representationPurpose;
    private List<String> representatives;
    private Stay stay;

    /**
     * Creates a new Receipt from an ID, connected to a Travel by a foreign key.
     * @param id The ID generated at database insertion.
     * @param fk The ID of the connected travel.
     * @param type The type of category of the receipt.
     * @param name The name of the receipt.
     * @param date The date of the receipt.
     * @param total The total of the receipt.
     * @param currency The currency of the receipt.
     * @param imagePath The path of the stored imagePath of the receipt.
     */
    public Receipt(int id, int fk, String type, String name, String date, double total,
                   String currency, String imagePath) {
        this.id = id;
        this.fk = fk;
        this.type = type;
        this.name = name;
        this.date = date;
        this.total = total;
        this.currency = currency;
        this.imagePath = imagePath;
        representationPurpose = "";
        representatives = new ArrayList<>();
    }

    /**
     * Returns the receipt ID.
     * @return An integer representing the ID automatically generated at input.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets a new ID for a receipt. Called when a travel is given an ID by the external database.
     * @param id The new ID to be set.
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Returns the receipt foreign key.
     * @return An integer representing the ID of the connected travel.
     */
    public int getFk() {
        return fk;
    }

    /**
     * Sets a new foreign key for a receipt. Called when a travel is given an ID by the external
     * database.
     * @param fk The new foreign key to be set.
     */
    public void setFk(int fk) {
        this.fk = fk;
    }

    /**
     * Returns the category type of the receipt.
     * @return A string representing the type of category for a receipt.
     */
    public String getType() {
        return type;
    }

    /**
     * Sets a new type of category for a travel.
     * @param type The new type to be set.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Returns the receipt name.
     * @return A string representing the name of the receipt.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets a new name for a receipt.
     * @param name The new name to be set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the purchase date of a receipt.
     * @return A string representation of the travel start date of format "dd.mm.yyyy".
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets a new purchase date for a receipt.
     * @param date The new date to be set of format "dd.mm.yyyy".
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Returns the total of a receipt.
     * @return A double representing the total of format "xxx,xx".
     */
    public double getTotal() {
        return total;
    }

    /**
     * Sets a new total for a receipt.
     * @param total The new total to be set of format "xxx,xx".
     */
    public void setTotal(double total) {
        this.total = total;
    }

    /**
     * Returns the currency of a receipt.
     * @return A string representing the currency of format "NOK for Norwegian kroner".
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets a new currency for a receipt.
     * @param currency The new currency to be set of format "NOK for Norwegian kroner".
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * Returns the imagePath path for a receipt imagePath.
     * @return A string which contains the local filepath of the receipt image path.
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * Returns the representation purpose of a receipt of type MEAL_REPRESENTATION
     * @return A string describing the prupose of representation.
     */
    public String getRepresentationPurpose() {
        return representationPurpose;
    }

    /**
     * Sets anew representation purpose for a receipt of type MEAL_REPRESENTATION.
     * @param representationPurpose The new purpose to be set.
     */
    public void setRepresentationPurpose(String representationPurpose) {
        this.representationPurpose = representationPurpose;
    }

    /**
     * Returns the list of representatives present for a receipt of type MEAL_REPRESENTATION.
     * @return An array list containing all representatives of a meal. Null if not applicable.
     */
    public List<String> getRepresentatives() {
        return representatives;
    }

    /**
     * Sets a new representatives list for a receipt of type MEAL_REPRESENTATION.
     * @param representatives The new list of representatives be set.
     */
    public void setRepresentatives(List<String> representatives) {
        this.representatives = representatives;
    }

    /**
     * Returns the stay of a receipt of type HOTEL.
     * @return A stay containing required information.
     */
    public Stay getStay() {
        return stay;
    }

    /**
     * Sets a new stay for a receipt of type HOTEL.
     * @param stay The new stay to set.
     */
    public void setStay(Stay stay) {
       this.stay = stay;
    }
}