package no.uib.jmi053.tefapplication.activities.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.Collections;
import java.util.List;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.superclass.InputActivity;
import no.uib.jmi053.tefapplication.utilities.constants.IntentConstants;
import no.uib.jmi053.tefapplication.utilities.constants.PopupConstants;
import no.uib.jmi053.tefapplication.utilities.graphics.GraphicsUtil;
import no.uib.jmi053.tefapplication.widgets.FontButton;
import no.uib.jmi053.tefapplication.widgets.FontEdit;
import no.uib.jmi053.tefapplication.widgets.FontView;

/**
 * Created by JoaT Development 24.09.15.
 *
 * Activity for adding or editing information for an event in a travel.
 */
public class EventActivity extends InputActivity implements View.OnClickListener {

    // Input widgets
    FontEdit event;

    // Flag
    boolean beingEdited;
    Boolean showToast;

    // Holds old name for event being edited
    String eventString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);

        // Finds and sets icon to the create button
        FontButton button = (FontButton) findViewById(R.id.event_save_button);
        GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_create);

        // Finds input widget
        event = (FontEdit) findViewById(R.id.event_input);

        // Checks if event is being edited
        beingEdited = getIntent().getBooleanExtra(IntentConstants.EDIT_FLAG, false);

        // Sets the toast flag to allow toasts to be shown
        showToast = true;

        if(beingEdited) {
            // Finds the name of the event
            eventString = getIntent().getStringExtra(IntentConstants.ITEM_NAME);

            // Sets new titles for editing
            FontView header = (FontView) findViewById(R.id.header);
            header.setText(R.string.edit_event_header);
            setTitle(R.string.title_activity_edit_event);

            // Sets name to input field
            event.setText(eventString);

            // Changes create button text and icon to save
            button.setText(R.string.button_save);
            GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_save);

            // Makes delete button visible
            FontButton delete = (FontButton) findViewById(R.id.event_delete_button);
            delete.setVisibility(FontButton.VISIBLE);
        }

        inputStringsAtStart = getInputStrings();
    }

    @Override
    protected String getItemName() {
        return "event";
    }

    @Override
    protected List<FontEdit> createEditFieldList() {
        return Collections.singletonList(event);
    }

    @Override
    protected List<String> getInputStrings() {
        return Collections.singletonList(event.getText().toString());
    }

    @Override
    public void onClick(View v) {
        final Intent result = new Intent();

        if(beingEdited) {
            result.putExtra(IntentConstants.ITEM_ID, eventString);
        }

        switch (v.getId()) {
            case R.id.event_save_button:
                if(!hasUnfilledFields()) {
                    result.putExtra(IntentConstants.ITEM_NAME, event.getText().toString());
                    if (beingEdited) {
                        setResult(IntentConstants.RESULT_EDIT, result);
                    } else {
                        setResult(IntentConstants.RESULT_CREATE, result);
                    }

                    finish();
                } else {
                    if (showToast){
                        showUnfilledFieldsToast();
                        showToast = false;
                    }
                }
                break;
            case R.id.event_delete_button:
                final Dialog dialog = new Dialog(this);

                // Creates the positive button listener
                View.OnClickListener positiveListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(PopupConstants.DIALOG_DELETION,
                                "User confirmed " + getItemName() + " deletion.");
                        v.setClickable(true);
                        setResult(IntentConstants.RESULT_DELETE, result);
                        finish();
                        dialog.dismiss();
                    }
                };
                showDeleteConfirmDialog(dialog, positiveListener);
                break;
        }
    }
}
