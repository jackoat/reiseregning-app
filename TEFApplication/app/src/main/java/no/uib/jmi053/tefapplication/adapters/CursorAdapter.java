package no.uib.jmi053.tefapplication.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;

import java.util.List;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.widgets.FontView;


/**
 * Created by JoaT Development on 09/09/2015.
 *
 * A custom adapter for displaying information for a Spinner.
 */
public class CursorAdapter extends BaseAdapter implements SpinnerAdapter {
    private Activity activity;
    private List<String> items;

    public CursorAdapter(Activity activity, List<String> items) {
        this.activity = activity;
        this.items = items;
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View spinView;
        if( convertView == null ){
            LayoutInflater inflater = activity.getLayoutInflater();
            spinView = inflater.inflate(R.layout.spinner_item, null);
        } else {
            spinView = convertView;
        }

        FontView item = (FontView) spinView.findViewById(R.id.spinner_item_label);
        item.setText(String.valueOf(items.get(position)));
        return spinView;
    }

}
