package no.uib.jmi053.tefapplication.activities.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.Collections;
import java.util.List;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.activities.superclass.InputActivity;
import no.uib.jmi053.tefapplication.utilities.constants.IntentConstants;
import no.uib.jmi053.tefapplication.utilities.constants.PopupConstants;
import no.uib.jmi053.tefapplication.utilities.graphics.GraphicsUtil;
import no.uib.jmi053.tefapplication.widgets.FontButton;
import no.uib.jmi053.tefapplication.widgets.FontEdit;
import no.uib.jmi053.tefapplication.widgets.FontView;

/**
 * Created by JoaT Development 24.09.15.
 *
 * Activity for adding or editing information for a person (passenger or representative).
 */
public class PersonActivity extends InputActivity implements View.OnClickListener {

    // Input widgets
    FontEdit person;

    // Flag
    boolean beingEdited;
    Boolean showToast;

    // Holds old name for person being edited
    String personString;

    // The name of the parent
    String parentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_person);

        // Parent activity name
        parentName = getIntent().getStringExtra(IntentConstants.PARENT_NAME);

        // Finds header
        FontView header = (FontView) findViewById(R.id.header);

        // Sets title and header based on parent
        if(parentName.equals(IntentConstants.PARENT_RECEIPT)) {
            header.setText(R.string.new_representative_header);
            setTitle(R.string.title_activity_new_representative);
        }

        if(parentName.equals(IntentConstants.PARENT_CAR_TRAVEL)) {
            header.setText(R.string.new_passenger_header);
            setTitle(R.string.title_activity_new_passenger);
        }

        // Finds and sets icon to the create button
        FontButton button = (FontButton) findViewById(R.id.person_save_button);
        GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_create);

        // Finds input widget
        person = (FontEdit) findViewById(R.id.person_input);

        // Checks if person is being edited
        beingEdited = getIntent().getBooleanExtra(IntentConstants.EDIT_FLAG, false);

        // Sets the toast flag to allow toasts to be shown
        showToast = true;

        if(beingEdited) {
            // Finds the name of the person
            personString = getIntent().getStringExtra(IntentConstants.ITEM_NAME);

            // Sets new titles for editing based on parent
            if(parentName.equals(IntentConstants.PARENT_RECEIPT)) {
                header.setText(R.string.edit_representative_header);
                setTitle(R.string.title_activity_edit_representative);
            }
            if(parentName.equals(IntentConstants.PARENT_CAR_TRAVEL)) {
                header.setText(R.string.edit_passenger_header);
                setTitle(R.string.title_activity_edit_passenger);
            }

            // Sets name to input field
            person.setText(personString);

            // Changes create button text and icon to save
            button.setText(R.string.button_save);
            GraphicsUtil.newDrawableRight(this, button, R.drawable.ic_save);

            // Makes delete button visible
            FontButton delete = (FontButton) findViewById(R.id.person_delete_button);
            delete.setVisibility(FontButton.VISIBLE);
        }

        inputStringsAtStart = getInputStrings();
    }

    @Override
    protected String getItemName() {
        if(parentName.equals(IntentConstants.PARENT_RECEIPT)) {
            return "representative";
        }
        if(parentName.equals(IntentConstants.PARENT_CAR_TRAVEL)) {
            return "passenger";
        }
        Log.e("Error", "Undefined parent.");
        return "undefined";
    }

    @Override
    protected List<FontEdit> createEditFieldList() {
        return Collections.singletonList(person);
    }

    @Override
    protected List<String> getInputStrings() {
        return Collections.singletonList(person.getText().toString());
    }

    @Override
    public void onClick(View v) {
        final Intent result = new Intent();

        if(beingEdited) {
            result.putExtra(IntentConstants.ITEM_ID, personString);
        }

        switch (v.getId()) {
            case R.id.person_save_button:
                if (!hasUnfilledFields()) {
                    result.putExtra(IntentConstants.ITEM_NAME, person.getText().toString());
                    if (beingEdited) {
                        setResult(IntentConstants.RESULT_EDIT, result);
                    } else {
                        setResult(IntentConstants.RESULT_CREATE, result);
                    }

                    finish();
                } else {
                    if (showToast){
                        showUnfilledFieldsToast();
                        showToast = false;
                    }
                }
                break;
            case R.id.person_delete_button:
                final Dialog dialog = new Dialog(this);

                // Creates the positive button listener
                View.OnClickListener positiveListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(PopupConstants.DIALOG_DELETION,
                                "User confirmed " + getItemName() + " deletion.");
                        v.setClickable(true);
                        setResult(IntentConstants.RESULT_DELETE, result);
                        finish();
                        dialog.dismiss();
                    }
                };
                showDeleteConfirmDialog(dialog, positiveListener);
        }
    }
}
