package no.uib.jmi053.tefapplication.io.currency;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

/**
 * AsyncTask for finding a currency
 *
 * Created by JoaT Development on 10/16/2015.
 */
public class JsonCurrencyAsyncTask extends AsyncTask<Void,Void,Double>{
    private String currency;
    public double currencyDouble;

    public JsonCurrencyAsyncTask(String currency){
        this.currency = currency;
    }

    /**
     * Finds the ecxhange rate from DNB web resource given the currency code
     *
     * @param params .
     * @return the exchange rate
     */
    @Override
    protected Double doInBackground(Void... params) {
        try {
            String baseUrl = "https://www.dnb.no/portalfront/datafiles/miscellaneous/csv/kursliste.csv";
            URL dnbUrl = new URL(baseUrl);
            InputStream is = dnbUrl.openStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String jsonText = readAll(rd);

            int currencyIndex = jsonText.indexOf(currency + ",,");
            String currencySubString = jsonText.substring((currencyIndex + 5), (currencyIndex + 10));
            String currencyOneOrHundred = jsonText.substring((currencyIndex - 2), (currencyIndex - 1));
            double outputDouble = Double.parseDouble(currencySubString);
            if (currencyOneOrHundred.equals("0")){currencyDouble = outputDouble/100;}
            else{currencyDouble = outputDouble;}
        } catch (Exception ex) {
            ex.printStackTrace();
        } return currencyDouble;
    }

    /**
     * Gets all currency strings
     *
     * @param rd the BufferedReader
     * @return the string of currencies
     * @throws IOException
     */
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
