package no.uib.jmi053.tefapplication.utilities;

import java.util.Map;

/**
 * Created by JoaT Development on 17.09.2015.
 *
 * Contains extended Map functionality.
 */
public abstract class MapUtil {

    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (value.hashCode() == entry.getValue().hashCode()) {
                return entry.getKey();
            }
        }
        return null;
    }
}
