package no.uib.jmi053.tefapplication.widgets;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.TimePicker;

import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.utilities.graphics.GraphicsUtil;

/**
 * Created by JoaT Development on 09.09.2015.
 *
 * Custom TimePicker with custom formatting.
 */
public class FormattedTimePicker extends TimePicker {

    /**
     * Creates a FormattedTimePicker with a predefined string colour.
     * @param context The activity to get resources from.
     * @param attrs The attribute set of the connected layout.
     */
    public FormattedTimePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        GraphicsUtil.format(this, ContextCompat.getColor(context, R.color.white));
    }
}
