package no.uib.jmi053.tefapplication.utilities.constants;

/**
 * Created by JoaT Development on 04.10.2015.
 *
 * Contains constants and utilities for debugging.
 */
public abstract class DebugConstants {

    // Error log constants
    public static final String ERROR_NO_TRAVEL = "Travel ID not found.";
    public static final String ERROR_NO_RECEIPT = "Receipt ID not found.";
    public static final String ERROR_NO_CAR_TRAVEL = "Car travel ID not found.";

    /**
     * Returns the name of the activity as error tags.
     * @param klass The class to get name from.
     * @return The name of the activity.
     */
    public static String getErrorTag(Class klass) {
        return klass.getName();
    }
}
