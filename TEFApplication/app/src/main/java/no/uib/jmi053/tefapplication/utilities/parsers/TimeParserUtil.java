package no.uib.jmi053.tefapplication.utilities.parsers;

import android.widget.TimePicker;

/**
 * Created by JoaT Development on 04/09/2015.
 *
 * Contains functionality for parsing Times from TimePickers.
 */
public abstract class TimeParserUtil {

    /**
     * Parse the hour from a string of format "hh:mm".
     * @param time The time string to parse from.
     * @return An integer representing the parsed hour.
     */
    private static int parseHour(String time){
        String[] split = time.split("\\:");
        return Integer.parseInt(split[0]);
    }

    /**
     * Parse the minutes from a string of format "hh:mm".
     * @param time The time string to parse from.
     * @return An integer representing the parsed minutes.
     */
    private static int parseMinute(String time){
        String[] split = time.split("\\:");
        return Integer.parseInt(split[1]);
    }

    /**
     * Creates a formatted time string from a time picker.
     * @param time The time picker to get time from.
     * @return A string representing the given time pickers time of format "hh:mm".
     */
    public static String getTimeString(TimePicker time) {
        time.clearFocus();
        String timeString = String.format("%02d", time.getCurrentHour());
        timeString += ":" + String.format("%02d", time.getCurrentMinute());
        return timeString;
    }

    /**
     * Sets a given time to a time picker.
     * @param time The time picker to update time for.
     * @param timeString The time string to parse information from.
     */
    public static void setTime(TimePicker time, String timeString) {
        time.setCurrentHour(TimeParserUtil.parseHour(timeString));
        time.setCurrentMinute(TimeParserUtil.parseMinute(timeString));
    }

}
