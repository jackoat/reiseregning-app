package no.uib.jmi053.tefapplication.io;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import jxl.CellType;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFeatures;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import no.uib.jmi053.tefapplication.R;
import no.uib.jmi053.tefapplication.entities.Receipt;
import no.uib.jmi053.tefapplication.entities.Travel;
import no.uib.jmi053.tefapplication.entities.User;
import no.uib.jmi053.tefapplication.io.currency.JsonCurrencyAsyncTask;
import no.uib.jmi053.tefapplication.utilities.ArrayPositionUtil;
import no.uib.jmi053.tefapplication.utilities.parsers.DateParserUtil;

/**
 * Created by JoaT Development on 9/30/2015.
 *
 * Creates and modifies all excel documents.
 */
public class ExcelHandler {

    // Gets resources and database access from activity of initialization
    private Context context;
    private DatabaseHandler db;
    private Travel travel;
    private String returnDate;
    private String returnTime;
    private long accountNumber;
    private HashMap<String, Double> exchangeRates = new HashMap<>();

    // Excel name constant
    private static final String EXCEL_FILE_NAME = "output.xls";

    /**
     * Creates an ExcelHandler which produces an excel document containing all information required
     * to send a travel expense form.
     *
     * @param context    The activity to get resources from.
     * @param db         The initialized database from the SendActivity.
     * @param travel     The travel to fill into the form
     * @param returnDate The return date of the travel which is stored locally in SendActivity.
     * @param returnTime The return time of the travel which is stored locally in SendActivity.
     */
    public ExcelHandler(Context context, DatabaseHandler db, Travel travel, String returnDate,
                        String returnTime, long accountNumber) {
        this.context = context;
        this.db = db;
        this.travel = travel;
        this.returnDate = returnDate;
        this.returnTime = returnTime;
        this.accountNumber = accountNumber;
    }

    // Columns and rows have index - 1 (related to excel-sheet indexes)

    /**
     * Opens the travel expense form template document and modifies it with stored information.
     */
    public String modifyExcel() {
        InputStream inputStream = context.getResources().openRawResource(R.raw.reisetemplate);
        Workbook w;
        File root = Environment.getExternalStorageDirectory();

        for (Receipt receipt:db.getAllReceipts(travel.getId())) {
            String currency = receipt.getCurrency();
            if (!currency.equals("NOK") && !exchangeRates.containsKey(currency)){
                try{
                    JsonCurrencyAsyncTask currencyTask = new JsonCurrencyAsyncTask(currency){};
                    currencyTask.execute();
                    double currencyNumber = currencyTask.get();
                    exchangeRates.put(currency, currencyNumber);
                }catch(Exception exc){exc.printStackTrace();}
            }
        }

        try {
            w = Workbook.getWorkbook(inputStream);
            WritableWorkbook copy = Workbook.createWorkbook(new File(root + "/Output.xls"), w);
            WritableSheet reiseregning = copy.getSheet(0);
            copy.setColourRGB(Colour.GOLD, 255, 255, 204);

            // Route label in the case of reimbursement
            WritableCell routeLabel = reiseregning.getWritableCell(5, 6);
            WritableCell purposeLabel = reiseregning.getWritableCell (1, 8);
            if (travel.getPurpose().equals("Single Reimbursement")){
            writeStringToXls(purposeLabel, "");
                writeStringToXls(routeLabel,
                        "Reimbursement name:");
            }

            // Cells from 1.INFORMATION
            WritableCell company = reiseregning.getWritableCell(5, 1);
            WritableCell name = reiseregning.getWritableCell(1, 5);
            WritableCell address = reiseregning.getWritableCell(5, 5);
            WritableCell account = reiseregning.getWritableCell(12, 5);
            WritableCell ssn = reiseregning.getWritableCell(1, 7);
            WritableCell route = reiseregning.getWritableCell(5, 7);
            WritableCell team_code = reiseregning.getWritableCell(9, 7);
            WritableCell start_date_time = reiseregning.getWritableCell(12, 7);
            WritableCell purpose = reiseregning.getWritableCell(1, 9);

            WritableCell customer_name = reiseregning.getWritableCell(5, 9);
            WritableCell customer_number = reiseregning.getWritableCell(9, 9);
            WritableCell return_date_time = reiseregning.getWritableCell(12, 9);

            // May be blank, may be more than one
            WritableCell events = reiseregning.getWritableCell(1, 11);

            // Cells from 2. NON PREPAID ACCOMMODATION
            // May be blank, may be more than one.
            WritableCell stay_id = reiseregning.getWritableCell(1, 15);
            WritableCell stay_type = reiseregning.getWritableCell(2, 15);
            WritableCell stay_name = reiseregning.getWritableCell(5, 15);
            WritableCell stay_address = reiseregning.getWritableCell(8, 15);
            WritableCell stay_nights = reiseregning.getWritableCell(11, 15);
            WritableCell stay_date = reiseregning.getWritableCell(12, 15);
            WritableCell stay_price = reiseregning.getWritableCell(14, 15);
            WritableCell stays_sum_nok = reiseregning.getWritableCell(14,16);

            //Cells from 3. EXPENSES
            //May be blank, may be more than one
            WritableCell expense_id = reiseregning.getWritableCell(1, 20);
            WritableCell expense_type = reiseregning.getWritableCell(2, 20);
            WritableCell expense_account = reiseregning.getWritableCell(5, 20);
            WritableCell expense_date = reiseregning.getWritableCell(7, 20);
            WritableCell expence_amount = reiseregning.getWritableCell(9, 20);
            WritableCell expense_currency = reiseregning.getWritableCell(12, 20);
            WritableCell expense_amount_nok = reiseregning.getWritableCell(14, 20);
            WritableCell expenses_sum_nok = reiseregning.getWritableCell(14, 21);

            //Cells from 4. MEAL REPRESENTATION
            WritableCell representation_id = reiseregning.getWritableCell(1, 25);
            WritableCell representation_purpose = reiseregning.getWritableCell(2, 25);
            WritableCell representation_involved_people = reiseregning.getWritableCell(5, 25);
            WritableCell representation_amount = reiseregning.getWritableCell(9, 25);
            WritableCell representation_currency = reiseregning.getWritableCell(12, 25);
            WritableCell representation_amount_nok = reiseregning.getWritableCell(14, 25);

            //Cell from 5. TOTAL EXPENSE COSTS
            WritableCell total_expenses_costs = reiseregning.getWritableCell(14, 27);

            //Cell from 6. RECEIVED IN ADVANCE BY EMPLOYEE
            WritableCell received_in_advance = reiseregning.getWritableCell(14, 29);

            //Cell from 7. AMOUNT TO BE CHARGED
            WritableCell total_reimbursement = reiseregning.getWritableCell(14, 31);

            // Cell from 8. COMMENTS
            WritableCell comments = reiseregning.getWritableCell(1, 34);

            // Cells from 9. SIGNATURES
            WritableCell employee_date = reiseregning.getWritableCell(1, 38);
            WritableCell employee_sign = reiseregning.getWritableCell(3, 38);

            // Finds the active user and active travel for information retrieval
            User user = db.getAllUsers().get(0);

            // Writes strings to 1. INFORMATION from the database to the excel form
            writeStringToXls(ssn, String.format("%011d", user.getSsn())); // Enforces 11 digits
            writeStringToXls(name, user.getName());
            writeStringToXls(company, user.getCompany());
            writeStringToXls(address, user.getAddress());
            if (!travel.getPurpose().equals("Single Reimbursement")) {
                writeStringToXls(route, travel.getRoute());
            }
            else writeStringToXls(route,
                    db.getAllReceipts(DatabaseHandler.REIMBURSEMENT_ID).get(0).getName());
            writeStringToXls(team_code, String.valueOf(user.getDepartment()));
            writeStringToXls(start_date_time, DateParserUtil.formatDate(travel.getStartDate()) +
                    " " + travel.getStartTime());
            if (travel.getPurpose().equals("Single Reimbursement")){writeStringToXls(purpose, "");}
            else{writeStringToXls(purpose, travel.getPurpose());
            }
            if(travel.getCustomerName().equals("null")){
                writeStringToXls(customer_name, "");
            } else{writeStringToXls(customer_name, travel.getCustomerName());}
            if (String.valueOf(travel.getCustomerId()).equals("0")){
                writeStringToXls(customer_number, "");
            } else {writeStringToXls(customer_number, String.valueOf(travel.getCustomerId()));}
            writeStringToXls(return_date_time, DateParserUtil.formatDate(returnDate) +
                    " " + returnTime);

            writeStringToXls(team_code, String.valueOf(user.getDepartment()));
            writeStringToXls(account, String.valueOf(accountNumber));
            writeStringToXls(employee_sign, user.getName());

            // Sets the current date and time for signature
            @SuppressLint("SimpleDateFormat") SimpleDateFormat df =
                    new SimpleDateFormat("dd.MM.yyyy HH:mm");
            Calendar cal = Calendar.getInstance();
            String date = df.format(cal.getTime());
            writeStringToXls(employee_date, date);

            writeStringToXls(received_in_advance, formatAmount(travel.getPrepaidAmount()));

            // Alle dynamiske + konkatinerte felter
            addEvents(events);
           int commentSize = addComments(comments);
            try {
                if (commentSize > 80) {
                    reiseregning.setRowView(34, 30 * 24);
                }
            } catch (Exception ex){ex.printStackTrace();}
            double staysTotal = addStays(stay_id, stay_type, stay_name, stay_address, stay_nights, stay_date,
                    stay_price, stays_sum_nok,
                    reiseregning);
            double expensesTotal = addExpenses(expense_id, expense_type, expense_account,
                    expense_date, expence_amount,
                    expense_currency, expense_amount_nok, expenses_sum_nok, reiseregning);
            double representationsTotal = addRepresentations(representation_id,
                    representation_purpose, representation_involved_people, representation_amount,
                    representation_currency, representation_amount_nok, reiseregning);
            double totalReimbursement = staysTotal+expensesTotal+representationsTotal;
            double toBeCharged = totalReimbursement - travel.getPrepaidAmount();

            writeStringToXls(total_expenses_costs, formatAmount(totalReimbursement));
            writeStringToXls(total_reimbursement, formatAmount(toBeCharged));

            try {
                reiseregning.getSettings().setProtected(true);
                reiseregning.getSettings().setPassword("joat");
                reiseregning.getSettings().setZoomFactor(65);
                copy.write();
                copy.close();
                inputStream.close();
            } catch (WriteException we) {
                we.printStackTrace();
            }
        } catch (BiffException | IOException b) {
            b.printStackTrace();
        }
        return root.getAbsolutePath() + "/" + EXCEL_FILE_NAME;
    }

    /**
     * Writes static labels to all specified cells.
     *
     * @param cell The cell to check for type label.
     * @param str  The string to put as label.
     */
    public void writeStringToXls(WritableCell cell, String str) {
        if (cell.getType() == CellType.LABEL) {
            Label cell_label = (Label) cell;
            cell_label.setString(str);
        }
    }

    public void addEvents(WritableCell events) {
        String event_strings = "";
        String subzero = "";
        List<String> events_list = new ArrayList<>(travel.getEvents());
        for (int i = 0; i<events_list.size(); i++) {
            event_strings = event_strings + events_list.get(i) + ", ";
            subzero = event_strings.substring(0, event_strings.length()-2);
        }
        writeStringToXls(events, subzero);
    }
    public int addComments(WritableCell comments) {
        String comment_strings = "";
        String subzero = "";
        List<String> comments_list = new ArrayList<>(travel.getComments());
        for (int i = 0; i < comments_list.size(); i++) {
            int commentLength = comments_list.get(i).length();
            String comment_final = comments_list.get(i);
            String firstLine = null;
            String secondLine = null;
            if (commentLength>100){
                String com_sub = comment_final.substring(80, comment_final.length());
                String com_initial = comment_final.substring(0,100);
                String[] spaceIndex = com_sub.split("\\s");
                System.out.println(spaceIndex[0]);
                System.out.println(spaceIndex[1]);
                firstLine = spaceIndex[0];
                secondLine = spaceIndex[1];
                comment_final = com_initial + firstLine + "\n" + secondLine;
            }
            comment_strings = comment_strings + comment_final + ". ";
            subzero = comment_strings.substring(0, comment_strings.length() - 2);
        }
        WritableCellFormat comments_format = new WritableCellFormat();
        try {

            comments_format.setWrap(true);
            comments_format.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
            comments_format.setBorder(Border.LEFT, BorderLineStyle.THIN);
            comments_format.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            comments_format.setBorder(Border.TOP, BorderLineStyle.THIN);
        }catch (WriteException wr){wr.printStackTrace();}
        comments.setCellFormat(comments_format);
        writeStringToXls(comments, subzero);
        return subzero.length();
    }
    public double addStays(WritableCell stayID, WritableCell stayType, WritableCell stayName,
                           WritableCell stayAddress, WritableCell stayNights, WritableCell stayDate, WritableCell stayPrice,
                           WritableCell stays_sum_in_nok, WritableSheet sheet0) {

        double totalSum = 0;

        //Inserting new row
        WritableCell stay_id_copy;
        WritableCell stay_type_copy;
        WritableCell stay_name_copy;
        WritableCell stay_address_copy;
        WritableCell stay_date_copy;
        WritableCell stay_price_copy;
        WritableCell stay_nights_copy;

        try {

            //Creates fix for borderproblem
            WritableCellFormat border_fix = new WritableCellFormat();
            border_fix.setBackground(Colour.GOLD);
            border_fix.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);

            //Creates format for new normal cells.
            WritableCellFormat insert_format = new WritableCellFormat(stayType.getCellFormat());
            insert_format.setBorder(Border.ALL, BorderLineStyle.THIN);

            //Creates format for new left-most cells with bold border.
            WritableCellFormat left_format = new WritableCellFormat(stayType.getCellFormat());
            left_format.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
            left_format.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            left_format.setBorder(Border.BOTTOM, BorderLineStyle.THIN);

            //Creates format for new right-most cells with bold border.
            WritableCellFormat right_format = new WritableCellFormat(stayType.getCellFormat());
            right_format.setBorder(Border.LEFT, BorderLineStyle.THIN);
            right_format.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
            right_format.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
            right_format.setBackground(Colour.GOLD);
            right_format.setAlignment(Alignment.RIGHT);

            //Creates format for last column in list with bold underline.
            WritableCellFormat bottom_format = new WritableCellFormat(stayType.getCellFormat());
            bottom_format.setBorder(Border.LEFT, BorderLineStyle.THIN);
            bottom_format.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            bottom_format.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);

            //Creates format for last, leftmost cell in list with bold left and underline.
            WritableCellFormat bottom_format_left = new WritableCellFormat(stayType.getCellFormat());
            bottom_format_left.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
            bottom_format_left.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            bottom_format_left.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);

            //Creates format for last, rightmost cell in list with bold right and underline.
            WritableCellFormat bottom_format_right = new WritableCellFormat(stayType.getCellFormat());
            bottom_format_right.setBorder(Border.LEFT, BorderLineStyle.THIN);
            bottom_format_right.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
            bottom_format_right.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
            bottom_format_right.setBackground(Colour.GOLD);
            bottom_format_right.setAlignment(Alignment.RIGHT);

            List<Receipt> hotel_receipts = new ArrayList<>();

            for (Receipt receipt : db.getAllReceipts(travel.getId())) {
                if (ArrayPositionUtil.hasHotelPosition(context, receipt.getType())) {
                    hotel_receipts.add(receipt);
                }
            }
            if (hotel_receipts.size() == 1) {
                writeStringToXls(stayID, String.valueOf(hotel_receipts.get(0).getId()));
                writeStringToXls(stayType, hotel_receipts.get(0).getStay().getType());
                writeStringToXls(stayName, hotel_receipts.get(0).getStay().getName());
                writeStringToXls(stayAddress, hotel_receipts.get(0).getStay().getAddress());
                writeStringToXls(stayNights, String.valueOf(hotel_receipts.get(0).getStay().getDuration()));
                writeStringToXls(stayDate, DateParserUtil.formatDate(
                        hotel_receipts.get(0).getStay().getDate()));
                // fikser total_sum exchange rates
                String hotelCurrency = hotel_receipts.get(0).getCurrency();
                double total_nok = 0;
               if(hotelCurrency.equals("NOK")){
                   total_nok = hotel_receipts.get(0).getTotal();
                   writeStringToXls(stayPrice, formatAmount(total_nok));
               }else if (exchangeRates.get(hotelCurrency)==0){
                   writeStringToXls(stayPrice, hotelCurrency + " utilgjengelig");
               } else{
                   double rate = exchangeRates.get(hotelCurrency);
                   total_nok = hotel_receipts.get(0).getTotal()*rate;
                    writeStringToXls(stayPrice, formatAmount(total_nok));
                }
                totalSum = totalSum+total_nok;
                writeStringToXls(stays_sum_in_nok, formatAmount(totalSum));
            } else {
                for (int i = 0; i < hotel_receipts.size(); i++) {

                    stay_id_copy = stayID.copyTo(1, 15 + i);
                    stay_type_copy = stayType.copyTo(2, 15 + i);
                    stay_name_copy = stayName.copyTo(5, 15 + i);
                    stay_address_copy = stayAddress.copyTo(8, 15 + i);
                    stay_nights_copy = stayNights.copyTo(11, 15 + i);
                    stay_date_copy = stayDate.copyTo(12, 15 + i);
                    stay_price_copy = stayPrice.copyTo(14, 15 + i);

                    if (i > 0) {
                        sheet0.insertRow(15 + i);
                    }

                    //Copies background cell into new-row white side cells
                    WritableCell background_copy_left = sheet0.getWritableCell(0, 10);
                    WritableCell left_side = background_copy_left.copyTo(0, 15 + i);
                    WritableCell background_copy_right = sheet0.getWritableCell(16, 15);
                    WritableCell right_side = background_copy_right.copyTo(16,15 + i);
                    right_side.setCellFormat(border_fix);
                    sheet0.addCell(left_side);
                    sheet0.addCell(right_side);

                    //adds the copy cells to new stay row
                    sheet0.addCell(stay_id_copy);
                    sheet0.addCell(stay_type_copy);
                    sheet0.addCell(stay_name_copy);
                    sheet0.addCell(stay_address_copy);
                    sheet0.addCell(stay_nights_copy);
                    sheet0.addCell(stay_date_copy);
                    sheet0.addCell(stay_price_copy);

                    // merges cells (in.col, in.row, des.col, des.row)

                    sheet0.mergeCells(2, 15 + i, 4, 15 + i);
                    sheet0.mergeCells(5, 15 + i, 7, 15 + i);
                    sheet0.mergeCells(8, 15 + i, 10, 15 + i);
                    sheet0.mergeCells(12, 15 + i, 13, 15 + i);
                    sheet0.mergeCells(14, 15 + i, 15, 15 + i);

                    //Sets format for copied cells

                    if(i < hotel_receipts.size() - 1) {
                        stay_id_copy.setCellFormat(left_format);
                        stay_type_copy.setCellFormat(insert_format);
                        stay_name_copy.setCellFormat(insert_format);
                        stay_address_copy.setCellFormat(insert_format);
                        stay_nights_copy.setCellFormat(insert_format);
                        stay_date_copy.setCellFormat(insert_format);
                        stay_price_copy.setCellFormat(right_format);
                    } else if(i == hotel_receipts.size()-1){
                        stay_id_copy.setCellFormat(bottom_format_left);
                        stay_type_copy.setCellFormat(bottom_format);
                        stay_name_copy.setCellFormat(bottom_format);
                        stay_address_copy.setCellFormat(bottom_format);
                        stay_nights_copy.setCellFormat(bottom_format);
                        stay_date_copy.setCellFormat(bottom_format);
                        stay_price_copy.setCellFormat(bottom_format_right);
                    }

                    //Sets row height as 24 points
                    sheet0.setRowView(15 + i, 20 * 24);

                    writeStringToXls(stay_id_copy, String.valueOf(hotel_receipts.get(i).getId()));
                    writeStringToXls(stay_type_copy, hotel_receipts.get(i).getStay().getType());
                    writeStringToXls(stay_address_copy, hotel_receipts.get(i).getStay().getAddress());
                    writeStringToXls(stay_name_copy, hotel_receipts.get(i).getStay().getName());
                    writeStringToXls(stay_nights_copy, String.valueOf(hotel_receipts.get(i).getStay().getDuration()));
                    writeStringToXls(stay_date_copy, DateParserUtil.formatDate(
                            hotel_receipts.get(i).getStay().getDate()));

                    // fikser total_sum exchange rates
                    String hotelCurrency = hotel_receipts.get(i).getCurrency();
                    double total_nok = 0;
                    if(hotelCurrency.equals("NOK")){
                        total_nok = hotel_receipts.get(i).getTotal();
                        writeStringToXls(stay_price_copy, formatAmount(total_nok));
                    } else if (exchangeRates.get(hotelCurrency)==0) {
                        writeStringToXls(stay_price_copy, hotelCurrency + " utilgjengelig");
                    }else{
                        double rate = exchangeRates.get(hotelCurrency);
                        total_nok = hotel_receipts.get(i).getTotal()*rate;
                        writeStringToXls(stay_price_copy, formatAmount(total_nok));
                    }
                    totalSum = totalSum+total_nok;
                }
                writeStringToXls(stays_sum_in_nok, formatAmount(totalSum));
                WritableCell right_side = sheet0.getWritableCell(16, 15);
                right_side.setCellFormat(border_fix);
            }

        } catch (WriteException we) {
            we.printStackTrace();
        } return totalSum;
    }

    public double addExpenses(WritableCell expenseID, WritableCell expenseType,
                              WritableCell expenseAccount, WritableCell expenseDate,
                              WritableCell expenseAmount, WritableCell expenseCurrency,
                              WritableCell expenseInNOK, WritableCell expenseSum,
                              WritableSheet sheet0) {
        double totalSum = 0;
        //Inserting new row
        WritableCell expense_id_copy;
        WritableCell expense_type_copy;
        WritableCell expense_account_copy;
        WritableCell expense_date_copy;
        WritableCell expense_amount_copy;
        WritableCell expense_currency_copy;
        WritableCell expense_in_nok_copy;

        try {

            //Creates fix for borderproblem
            WritableCellFormat border_fix = new WritableCellFormat();
            border_fix.setBackground(Colour.GOLD);
            border_fix.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);

            //Creates format for new normal cells.
            WritableCellFormat insert_format = new WritableCellFormat(expenseType.getCellFormat());
            insert_format.setBorder(Border.ALL, BorderLineStyle.THIN);

            //Creates format for new left-most cells with bold border.
            WritableCellFormat left_format = new WritableCellFormat(expenseType.getCellFormat());
            left_format.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
            left_format.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            left_format.setBorder(Border.BOTTOM, BorderLineStyle.THIN);

            //Creates format for new right-most cells with bold border.
            WritableCellFormat right_format = new WritableCellFormat(expenseType.getCellFormat());
            right_format.setBorder(Border.LEFT, BorderLineStyle.THIN);
            right_format.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
            right_format.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
            right_format.setAlignment(Alignment.RIGHT);
            right_format.setBackground(Colour.GOLD);

            //Creates format for last column in list with bold underline.
            WritableCellFormat bottom_format = new WritableCellFormat(expenseType.getCellFormat());
            bottom_format.setBorder(Border.LEFT, BorderLineStyle.THIN);
            bottom_format.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            bottom_format.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);

            //Creates format for last, leftmost cell in list with bold left and underline.
            WritableCellFormat bottom_format_left = new WritableCellFormat(expenseType.getCellFormat());
            bottom_format_left.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
            bottom_format_left.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            bottom_format_left.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);

            //Creates format for last, rightmost cell in list with bold right and underline.
            WritableCellFormat bottom_format_right = new WritableCellFormat(expenseType.getCellFormat());
            bottom_format_right.setBorder(Border.LEFT, BorderLineStyle.THIN);
            bottom_format_right.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
            bottom_format_right.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
            bottom_format_right.setAlignment(Alignment.RIGHT);
            bottom_format_right.setBackground(Colour.GOLD);

            List<Receipt> expense_receipts = new ArrayList<>();
            List<Receipt> hotel_receipts = new ArrayList<>();

            for (Receipt receipt : db.getAllReceipts(travel.getId())) {
                if (ArrayPositionUtil.hasHotelPosition(context, receipt.getType())) {
                    hotel_receipts.add(receipt);
                }
                if(!ArrayPositionUtil.hasMealRepresentationPosition(context,receipt.getType()) &&
                        !ArrayPositionUtil.hasHotelPosition(context, receipt.getType())){
                    expense_receipts.add(receipt);
                }
            }

            int counter;
            if(hotel_receipts.size() > 1) {
                counter = 19 + hotel_receipts.size();
            } else {counter = 20;}

            if (expense_receipts.size() == 1) {
                String acc1 = getExpenseAccount(expense_receipts.get(0));
                writeStringToXls(expenseID, String.valueOf(expense_receipts.get(0).getId()));
                writeStringToXls(expenseType, expense_receipts.get(0).getType());
                writeStringToXls(expenseAccount, acc1);
                writeStringToXls(expenseDate, DateParserUtil.formatDate(
                        expense_receipts.get(0).getDate()));
                writeStringToXls(expenseAmount, formatAmount(expense_receipts.get(0).getTotal()));
                writeStringToXls(expenseCurrency, expense_receipts.get(0).getCurrency());

                // fikser total_sum exchange rates
                String expCur = expense_receipts.get(0).getCurrency();
                double total_nok = 0;
                if(expCur.equals("NOK")){
                    total_nok = expense_receipts.get(0).getTotal();
                    writeStringToXls(expenseInNOK, formatAmount(total_nok));
                }else if (exchangeRates.get(expCur)==0) {
                    writeStringToXls(expenseInNOK, expCur + " utilgjengelig");
                } else{
                    double rate = exchangeRates.get(expCur);
                    total_nok = expense_receipts.get(0).getTotal()*rate;
                    writeStringToXls(expenseInNOK, formatAmount(total_nok));
                }
                totalSum = totalSum+total_nok;
            } else {
                for (int i = 0; i < expense_receipts.size(); i++) {
                    expense_id_copy = expenseID.copyTo(1, counter + i);
                    expense_type_copy = expenseType.copyTo(2, counter + i);
                    expense_account_copy = expenseAccount.copyTo(5, counter + i);
                    expense_date_copy = expenseDate.copyTo(7, counter + i);
                    expense_amount_copy = expenseAmount.copyTo(9, counter + i);
                    expense_currency_copy = expenseCurrency.copyTo(12, counter + i);
                    expense_in_nok_copy = expenseInNOK.copyTo(14, counter + i);


                    if (i > 0) {
                        sheet0.insertRow(counter + i);
                    }

                    //Copies background cell into new-row white side cells
                    WritableCell background_copy_left = sheet0.getWritableCell(0, 10);
                    WritableCell left_side = background_copy_left.copyTo(0, counter + i);
                    WritableCell background_copy_right = sheet0.getWritableCell(16, 15);
                    WritableCell right_side = background_copy_right.copyTo(16, counter + i);
                    right_side.setCellFormat(border_fix);
                    sheet0.addCell(left_side);
                    sheet0.addCell(right_side);

                    //adds the copy cells to new stay row
                    sheet0.addCell(expense_id_copy);
                    sheet0.addCell(expense_type_copy);
                    sheet0.addCell(expense_account_copy);
                    sheet0.addCell(expense_date_copy);
                    sheet0.addCell(expense_amount_copy);
                    sheet0.addCell(expense_currency_copy);
                    sheet0.addCell(expense_in_nok_copy);

                    // merges cells (in.col, in.row, des.col, des.row)
                    sheet0.mergeCells(2, counter + i, 4, counter + i);
                    sheet0.mergeCells(5, counter + i, 6, counter + i);
                    sheet0.mergeCells(7, counter + i, 8, counter + i);
                    sheet0.mergeCells(9, counter + i, 11, counter + i);
                    sheet0.mergeCells(12, counter + i, 13, counter + i);
                    sheet0.mergeCells(14, counter + i, 15, counter + i);

                    //Sets format for copied cells

                    if(i < expense_receipts.size()-1) {
                        expense_id_copy.setCellFormat(left_format);
                        expense_type_copy.setCellFormat(insert_format);
                        expense_account_copy.setCellFormat(insert_format);
                        expense_date_copy.setCellFormat(insert_format);
                        expense_amount_copy.setCellFormat(insert_format);
                        expense_currency_copy.setCellFormat(insert_format);
                        expense_in_nok_copy.setCellFormat(right_format);
                    } else if(i==expense_receipts.size()-1){
                        expense_id_copy.setCellFormat(bottom_format_left);
                        expense_type_copy.setCellFormat(bottom_format);
                        expense_account_copy.setCellFormat(bottom_format);
                        expense_date_copy.setCellFormat(bottom_format);
                        expense_amount_copy.setCellFormat(bottom_format);
                        expense_currency_copy.setCellFormat(bottom_format);
                        expense_in_nok_copy.setCellFormat(bottom_format_right);
                    }

                    //sets row height as 24 points
                    sheet0.setRowView(counter + i, 20 * 24);

                    String acc1 = getExpenseAccount(expense_receipts.get(i));
                    writeStringToXls(expense_id_copy, String.valueOf(
                            expense_receipts.get(i).getId()));
                    writeStringToXls(expense_type_copy, expense_receipts.get(i).getType());
                    writeStringToXls(expense_account_copy, acc1);
                    writeStringToXls(expense_date_copy, DateParserUtil.formatDate(
                            expense_receipts.get(i).getDate()));
                    writeStringToXls(expense_amount_copy, formatAmount(
                            expense_receipts.get(i).getTotal()));
                    writeStringToXls(expense_currency_copy, expense_receipts.get(i).getCurrency());

                    // fikser total_sum exchange rates
                    String expCur = expense_receipts.get(i).getCurrency();
                    double total_nok = 0;
                    if(expCur.equals("NOK")){
                        total_nok = expense_receipts.get(i).getTotal();
                        writeStringToXls(expense_in_nok_copy, formatAmount(total_nok));
                    } else if (exchangeRates.get(expCur)==0) {
                        writeStringToXls(expense_in_nok_copy, expCur + " utilgjengelig");
                    }else{
                        double rate = exchangeRates.get(expCur);
                        total_nok = expense_receipts.get(i).getTotal()*rate;
                        writeStringToXls(expense_in_nok_copy, formatAmount(total_nok));
                    }
                    totalSum = totalSum+total_nok;
                }
            }
            writeStringToXls(expenseSum, formatAmount(totalSum));

        } catch (WriteException we) {
            we.printStackTrace();
        }
        return totalSum;
    }

    public double addRepresentations(WritableCell representationId, WritableCell representationPurpose,
                                     WritableCell representationPersons, WritableCell representationAmount,
                                     WritableCell representationCurrency, WritableCell representationAmountInNok, WritableSheet sheet0) {

        double totalSum = 0;

        WritableCell representation_id_copy;
        WritableCell representation_purpose_copy;
        WritableCell representation_persons_copy;
        WritableCell representation_amount_copy;
        WritableCell representation_currency_copy;
        WritableCell representation_amount_in_nok_copy;

        try {

            WritableCellFormat border_fix = new WritableCellFormat();
            border_fix.setBackground(Colour.GOLD);
            border_fix.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);

            //Creates format for new normal cells.
            WritableCellFormat insert_format = new WritableCellFormat(representationId.getCellFormat());
            insert_format.setBorder(Border.ALL, BorderLineStyle.THIN);

            //Creates format for new left-most cells with bold border.
            WritableCellFormat left_format = new WritableCellFormat(representationId.getCellFormat());
            left_format.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
            left_format.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            left_format.setBorder(Border.BOTTOM, BorderLineStyle.THIN);

            //Creates format for new right-most cells with bold border.
            WritableCellFormat right_format = new WritableCellFormat(representationId.getCellFormat());
            right_format.setBorder(Border.LEFT, BorderLineStyle.THIN);
            right_format.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
            right_format.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
            right_format.setAlignment(Alignment.RIGHT);
            right_format.setBackground(Colour.GOLD);

            //Creates format for last column in list with bold underline.
            WritableCellFormat bottom_format = new WritableCellFormat(representationId.getCellFormat());
            bottom_format.setBorder(Border.LEFT, BorderLineStyle.THIN);
            bottom_format.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            bottom_format.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);

            //Creates format for last, leftmost cell in list with bold left and underline.
            WritableCellFormat bottom_format_left = new WritableCellFormat(representationId.getCellFormat());
            bottom_format_left.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
            bottom_format_left.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            bottom_format_left.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);

            //Creates format for last, rightmost cell in list with bold right and underline.
            WritableCellFormat bottom_format_right = new WritableCellFormat(representationId.getCellFormat());
            bottom_format_right.setBorder(Border.LEFT, BorderLineStyle.THIN);
            bottom_format_right.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
            bottom_format_right.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
            bottom_format_right.setAlignment(Alignment.RIGHT);
            bottom_format_right.setBackground(Colour.GOLD);

            List<Receipt> hotel_receipts = new ArrayList<>();
            List<Receipt> expense_receipts = new ArrayList<>();
            List<Receipt> meal_rep_receipts = new ArrayList<>();

            //Populates lists.
            for (Receipt receipt : db.getAllReceipts(travel.getId())) {
                if (ArrayPositionUtil.hasMealRepresentationPosition(context, receipt.getType())) {
                    meal_rep_receipts.add(receipt);
                }
                else if (ArrayPositionUtil.hasHotelPosition(context, receipt.getType())) {
                    hotel_receipts.add(receipt);
                } else {
                    expense_receipts.add(receipt);
                }
            }


            int base_row = 25;
            int hotel_counter = 0;
            int expense_counter = 0;

            if (hotel_receipts.size() > 1){
                hotel_counter = hotel_receipts.size()-1;
            }

            if(expense_receipts.size() > 1){
                expense_counter = expense_receipts.size()-1;
            }

            int new_base_row = base_row + hotel_counter + expense_counter;


            if (meal_rep_receipts.size() == 1) {
                writeStringToXls(representationId,
                        String.valueOf(meal_rep_receipts.get(0).getId()));
                writeStringToXls(representationPurpose,
                        meal_rep_receipts.get(0).getRepresentationPurpose());
                writeStringToXls(representationPersons,
                        getRepresentatives(
                                TextUtils.join(", ",
                                        meal_rep_receipts.get(0).getRepresentatives())));
                writeStringToXls(representationAmount,
                        formatAmount(meal_rep_receipts.get(0).getTotal()));
                writeStringToXls(representationCurrency, meal_rep_receipts.get(0).getCurrency());

                // fikser total_sum exchange rates
                String mealCur = meal_rep_receipts.get(0).getCurrency();
                double total_nok = 0;
                if(mealCur.equals("NOK")){
                    total_nok = meal_rep_receipts.get(0).getTotal();
                    writeStringToXls(representationAmountInNok, formatAmount(total_nok));
                }else if (exchangeRates.get(mealCur)==0) {
                    writeStringToXls(representationAmount, mealCur + " utilgjengelig");
                } else{
                    double rate = exchangeRates.get(mealCur);
                    total_nok = meal_rep_receipts.get(0).getTotal()*rate;
                    writeStringToXls(representationAmountInNok, formatAmount(total_nok));
                }
                totalSum = totalSum+total_nok;


            } else {
                for (int k = 0; k < meal_rep_receipts.size(); k++) {
                    representation_id_copy = representationId.copyTo(1, new_base_row + k);
                    representation_purpose_copy = representationPurpose.copyTo(2, new_base_row + k);
                    representation_persons_copy = representationPersons.copyTo(5, new_base_row +k);
                    representation_amount_copy = representationAmount.copyTo(9, new_base_row + k);
                    representation_currency_copy = representationCurrency.copyTo(12, new_base_row+k);
                    representation_amount_in_nok_copy = representationAmountInNok.copyTo(14, new_base_row+k);

                    if(k > 0){
                        sheet0.insertRow(new_base_row + k);
                    }

                    //Copies background cell into new-row white side cells
                    WritableCell background_copy_left = sheet0.getWritableCell(0, 10);
                    WritableCell left_side = background_copy_left.copyTo(0, new_base_row + k);
                    WritableCell background_copy_right = sheet0.getWritableCell(16, 15);
                    WritableCell right_side = background_copy_right.copyTo(16, new_base_row + k);
                    right_side.setCellFormat(border_fix);
                    sheet0.addCell(left_side);
                    sheet0.addCell(right_side);

                    //adds the copy cells to new stay row
                    sheet0.addCell(representation_id_copy);
                    sheet0.addCell(representation_purpose_copy);
                    sheet0.addCell(representation_persons_copy);
                    sheet0.addCell(representation_amount_copy);
                    sheet0.addCell(representation_currency_copy);
                    sheet0.addCell(representation_amount_in_nok_copy);

                    //merges cells
                    sheet0.mergeCells(2, new_base_row + k, 4, new_base_row+ k);
                    sheet0.mergeCells(5, new_base_row + k, 8, new_base_row+ k);
                    sheet0.mergeCells(9, new_base_row + k, 11, new_base_row+ k);
                    sheet0.mergeCells(12, new_base_row + k, 13, new_base_row+ k);
                    sheet0.mergeCells(14, new_base_row + k, 15, new_base_row+ k);

                    //Sets format for copied cells
                    if( k < meal_rep_receipts.size()-1) {
                        representation_id_copy.setCellFormat(left_format);
                        representation_purpose_copy.setCellFormat(insert_format);
                        representation_persons_copy.setCellFormat(insert_format);
                        representation_amount_copy.setCellFormat(insert_format);
                        representation_currency_copy.setCellFormat(insert_format);
                        representation_amount_in_nok_copy.setCellFormat(right_format);

                    }
                    else if(k == meal_rep_receipts.size()-1){
                        representation_id_copy.setCellFormat(bottom_format_left);
                        representation_purpose_copy.setCellFormat(bottom_format);
                        representation_persons_copy.setCellFormat(bottom_format);
                        representation_amount_copy.setCellFormat(bottom_format);
                        representation_currency_copy.setCellFormat(bottom_format);
                        representation_amount_in_nok_copy.setCellFormat(bottom_format_right);
                    }

                    sheet0.setRowView(new_base_row + k, 20 * 24);

                    writeStringToXls(representation_id_copy, String.valueOf(meal_rep_receipts.get(k).getId()));
                    writeStringToXls(representation_purpose_copy, meal_rep_receipts.get(k).getRepresentationPurpose());

                    // dersom det er for mange navn, lager den ny linje
                    String repList = TextUtils.join(", ", meal_rep_receipts.get(k).getRepresentatives());
                    String representation_people = getRepresentatives(repList);
                    writeStringToXls(representation_persons_copy, repList);

                    writeStringToXls(representation_amount_copy, formatAmount(meal_rep_receipts.get(k).getTotal()));
                    writeStringToXls(representation_currency_copy, meal_rep_receipts.get(k).getCurrency());

                    // fikser total_sum exchange rates
                    String mealCur = meal_rep_receipts.get(k).getCurrency();
                    double total_nok = 0;
                    if(mealCur.equals("NOK")){
                        total_nok = meal_rep_receipts.get(k).getTotal();
                        writeStringToXls(representation_amount_in_nok_copy,
                                formatAmount(total_nok));
                    } else if (exchangeRates.get(mealCur)==0) {
                        writeStringToXls(representation_amount_in_nok_copy,
                                mealCur + " utilgjengelig");
                    }else{
                        double rate = exchangeRates.get(mealCur);
                        total_nok = meal_rep_receipts.get(k).getTotal()*rate;
                        writeStringToXls(representation_amount_in_nok_copy,
                                formatAmount(total_nok));
                    }
                    totalSum = totalSum+total_nok;
                }
            }
        }
        catch (WriteException we) {
            we.printStackTrace();
        }
        return totalSum;
    }

    private String getExpenseAccount(Receipt receipt){
        String[] expenseTypes =
                context.getResources().getStringArray(R.array.receipt_type_categories);
        String expense_account = null;
        if (receipt.getType().equals(expenseTypes[0])) {
            expense_account = "7113";
        }
        if (receipt.getType().equals(expenseTypes[1])) {
            expense_account = "7110";
        }
        if (receipt.getType().equals(expenseTypes[2])) {
            expense_account = "7114";
        }
        if (receipt.getType().equals(expenseTypes[3])) {
            expense_account = "7117";
        }
        if (receipt.getType().equals(expenseTypes[4])) {
            expense_account = "7120";
        }
        if (receipt.getType().equals(expenseTypes[5])) {
            expense_account = "7010";
        }
        if (receipt.getType().equals(expenseTypes[6])) {
            expense_account = "7112";
        }
        if (receipt.getType().equals(expenseTypes[7])) {
            expense_account = "7370/7371";
        }
        if (receipt.getType().equals(expenseTypes[8])) {
            expense_account = "7113";
        }
        if (receipt.getType().equals(expenseTypes[9])) {
            expense_account = "5902";
        }
        if (receipt.getType().equals(expenseTypes[10])) {
            expense_account = "";
        }
        return expense_account;
    }

    private  String getRepresentatives(String repList){
        String repFinal = repList;
        if (repList.length() > 35){
            System.out.println("repList over 35: " + repList.length());
            String repSub = repList.substring(25, repList.length());
            System.out.println("substring etter 30: " + repSub);
            String repFirst = repList.substring(0, 25);
            String[] subStrings = repSub.split(",");
            String rep1 = subStrings[0];
            String firstLine = repFirst + rep1;
            String secondLine = "";
            for (int j = 1; j < subStrings.length; j++){
                secondLine = secondLine + subStrings[j];
            }
            repFinal = firstLine + "\n" + secondLine;
        }
        return repFinal;
    }

    private String formatAmount(double amount) {
        return String.format("%.2f", amount);
    }
}
